package com.futool.excel;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.alibaba.excel.write.style.column.LongestMatchColumnWidthStyleStrategy;
import com.futool.JsonUtil;
import com.futool.UtilValidate;
import com.futool.io.FileUtil;
import lombok.extern.slf4j.Slf4j;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Excel读写工具类
 * @author liuc
 * @version V1.0
 * @date 2021/11/6 17:11
 * @since JDK1.8
 */
@Slf4j
public class ExcelUtil {

    public static void writeExcel(String filePath,String title,String sheetName,List<List<Object>> dataList) throws Exception {
        File file = new File(filePath);
        // 创建writerBuilder对象，自动调整列宽
        ExcelWriter excelWriter = null;
        try {
            excelWriter = EasyExcel.write(new FileOutputStream(file))
                    .registerWriteHandler(new LongestMatchColumnWidthStyleStrategy())
                    .autoCloseStream(Boolean.TRUE)
                    .build();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        //  List<List<?>>类型, 二维数据, 表示第几行第几列, 设置表头内容
        List<List<String>> titleList = new ArrayList<List<String>>();
        String []titles = title.split(",");
        titleList.add(Arrays.asList(titles));

//        List<List<String>> titleList = new ArrayList<>();
//        titleList.add(Arrays.asList("名称", "key", "value"));
        if(titles.length != dataList.get(0).size()){
            throw new ExcelException("导出的每行数据的列数与表头列数不一致,请检查");
        }
        int sheetNum = 0;
        WriteSheet writeSheet = EasyExcel.writerSheet(sheetNum,sheetName).build();
        excelWriter.write(titleList, writeSheet);
        // 写入文件
        excelWriter.write(dataList, writeSheet);
        // 关闭文件流
        excelWriter.finish();
    }

    /**
     * 根据文件路径读取excel文件并返回为List<Map>格式
     * @param filePath
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     * @author liuc
     * @date 2021/11/8 20:10
     * @throws
     */
    public static List<Map<String,Object>> readExcel(String filePath){
        boolean flag = FileUtil.isExists(filePath);
        if (!flag) {
            log.error("找不到文件或文件路径错误,文件：{}", filePath);
            throw new ExcelException("找不到文件或文件路径错误,文件：{}",filePath);
        }
        if ((!filePath.toLowerCase().endsWith(ExcelTypeEnum.XLS.getValue())
                && !filePath.toLowerCase().endsWith(ExcelTypeEnum.XLSX.getValue()))) {
            throw new ExcelException("文件格式错误！");
        }
        // 接收结果集，为一个List列表，每个元素为一个map对象，key-value对为excel中每个列对应的值
        List<Map<String,Object>> resultList = new ArrayList<Map<String,Object>>();
        EasyExcel.read(filePath, new AnalysisEventListener<Map<String, Object>>() {
            @Override
            public void invoke(Map<String, Object> map, AnalysisContext analysisContext) {
                log.info("解析到一条数据:{}", JsonUtil.toJson(map));
                resultList.add(map);
            }
            @Override
            public void doAfterAllAnalysed(AnalysisContext analysisContext) {
                //非必要语句，查看导入的数据
                log.info("导入的数据条数为: " + resultList.size());
                log.info("所有数据解析完成！");
            }
        }).sheet().doRead();
        return resultList;
    }

    /**
     * 读大于1000行数据, 带样式
     * @param filePath 文件觉得路径
     * @return
     */
    public static List<Object> readExcel(String filePath,String sheetName){
        List<Object> resultList = new ArrayList<Object>();
        InputStream fileStream = null;
        if (UtilValidate.isEmpty(sheetName)) {
            sheetName = "Sheet1";
        }
        try {
            fileStream = new FileInputStream(filePath);
            ExcelListener excelListener = new ExcelListener();
            EasyExcel.read(fileStream, excelListener).sheet(sheetName).doRead();
            resultList =  excelListener.getDataList();
        } catch (FileNotFoundException e) {
            log.error("找不到文件或文件路径错误, 文件：{}", filePath);
        }finally {
            try {
                if(fileStream != null){
                    fileStream.close();
                }
            } catch (IOException e) {
                log.error("excel文件读取失败, 失败原因：{}", e);
            }
        }
        return resultList;
    }
}
