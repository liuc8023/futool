package com.futool.excel;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.excel.exception.ExcelDataConvertException;
import com.futool.JsonUtil;
import lombok.extern.slf4j.Slf4j;
import java.util.ArrayList;
import java.util.List;

/**
 * 如果没有特殊说明，下面的案例将默认使用这个监听器
 * @author liuc
 * @version V1.0
 * @date 2021/11/6 17:10
 * @since JDK1.8
 */
@Slf4j
public class ExcelListener extends AnalysisEventListener {
    /**
     * 每隔5条存储数据库，实际使用中可以3000条，然后清理list ，方便内存回收
     */
    private static final int BATCH_COUNT = 300;

    /**
     * 可以通过实例获取该值
     * 如果使用了spring,请使用这个构造方法。每次创建Listener的时候需要把spring管理的类传进来
     */
    private List<Object> dataList = new ArrayList<>();

    /**
     * 这个每一条数据解析都会来调用
     * @param object
     * @param context
     * @return void
     * @author liuc
     * @date 2021/11/6 17:18
     * @throws
     */
    @Override
    public void invoke(Object object, AnalysisContext context) {
        log.info("解析到一条数据:{}", JsonUtil.toJson(object));
        //数据存储到list，供批量处理，或后续自己业务逻辑处理。
        dataList.add(object);
        //如数据过大，可以进行定量分批处理
        if(dataList.size()>= BATCH_COUNT){
            handleBusinessLogic();
            dataList.clear();
        }

    }

    /**
     * 所有数据解析完成了 都会来调用
     * @param context
     * @return void
     * @author liuc
     * @date 2021/11/6 17:19
     * @throws
     */
    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        //非必要语句，查看导入的数据
        log.info("导入的数据条数为: " + dataList.size());
        log.info("所有数据解析完成！");
    }

    /**
     * 根据业务自行实现该方法，例如将解析好的dataList存储到数据库中
     * @param
     * @return void
     * @author liuc
     * @date 2021/11/6 17:21
     * @throws
     */
    private void handleBusinessLogic() {

    }

    /**
     * 监听器实现这个方法就可以在读取数据的时候获取到异常信息
     * @param exception
     * @param context
     * @return void
     * @author liuc
     * @date 2021/11/6 21:31
     * @throws
     */
    @Override
    public void onException(Exception exception, AnalysisContext context) {
        log.error("解析失败，但是继续解析下一行:{}", exception.getMessage());
        // 如果是某一个单元格的转换异常 能获取到具体行号
        // 如果要获取头的信息 配合invokeHeadMap使用
        if (exception instanceof ExcelDataConvertException) {
            ExcelDataConvertException excelDataConvertException = (ExcelDataConvertException)exception;
            log.error("第{}行，第{}列解析异常", excelDataConvertException.getRowIndex(),
                    excelDataConvertException.getColumnIndex());
        }
    }

    public List<Object> getDataList() {
        return dataList;
    }

    public void setDataList(List<Object> dataList) {
        this.dataList = dataList;
    }
}
