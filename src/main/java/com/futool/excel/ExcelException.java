package com.futool.excel;

/**
 * @author liuc
 * @version V1.0
 * @date 2021/11/8 19:53
 * @since JDK1.8
 */
public class ExcelException extends RuntimeException{
    private String code;
    public ExcelException(){}

    public ExcelException(Object message) {
        super(message.toString());
    }

    public ExcelException(String code, Object message) {
        super(message.toString());
        this.code = code;
    }

    public String getCode() {
        return this.code;
    }
}
