package com.futool;

import com.jcraft.jsch.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import java.io.*;
import java.util.Objects;
import java.util.Properties;
import java.util.Vector;

/**
 * SFTP 文件上传工具类
 *
 * @author liuc
 * @version V1.0
 * @date 2021/11/23 21:56
 * @since JDK1.8
 */
@Slf4j
public class SftpUtil {
    private ChannelSftp channel;

    private Session session;
    /**
     * sftp用户名
     */
    private String userName;
    /**
     * sftp密码
     */
    private String password;
    /**
     * sftp主机ip
     */
    private String ftpHost;
    /**
     * sftp主机端口
     */
    private int ftpPort;
    /**
     * 超时时间
     */
    private int timeout;
    /**
     * 密钥文件路径
     */
    private String privateKey;
    /**
     * 密钥的密码
     */
    private String passphrase;

    /**
     * @param ftpHost  IP地址
     * @param ftpPort  端口
     * @param userName 用户名
     * @param password 密码
     * @throws Exception
     */
    public SftpUtil(String ftpHost, int ftpPort, String userName, String password, int timeout) throws Exception {
        this.ftpHost = ftpHost;
        this.ftpPort = ftpPort;
        this.userName = userName;
        this.password = password;
        this.timeout = timeout;
        this.connectServer();
    }

    /**
     * @param userName   用户名
     * @param privateKey 私钥
     * @param passphrase 私钥密码
     * @param ftpHost    IP地址
     * @param ftpPort    端口
     */
    public SftpUtil(String userName, String privateKey, String passphrase, String ftpHost, int ftpPort) {
        this.userName = userName;
        this.ftpHost = ftpHost;
        this.ftpPort = ftpPort;
        this.privateKey = privateKey;
        this.passphrase = passphrase;
        this.channel = new ChannelSftp();
        this.loginForRsa();
    }

    /**
     * 使用RSA密钥的方式连接sftp服务器
     * @param
     * @return void
     * @author liuc
     * @date 2021/11/24 15:14
     * @throws
     */
    public synchronized void loginForRsa() {
        Channel channel = null;
        log.info("Connected to sftp start" + ftpHost + ":" + ftpPort);
        JSch jsch = new JSch();
        // 设置密钥和密码
        try {
            if (this.isNotEmpty(privateKey)) {
                if (this.isNotEmpty(passphrase)) {
                    jsch.addIdentity(privateKey, passphrase);
                } else {
                    jsch.addIdentity(privateKey);
                }
            } else {
                throw new Exception("privateKey is null or empty!");
            }
            log.info("-----privateKey is success-----");
            log.info("-----sftp连接正在创建session-----");
            if (ftpPort <= 0) {
                session = jsch.getSession(userName, ftpHost);
            } else {
                session = jsch.getSession(userName, ftpHost, ftpPort);
            }
            log.info("-----userName and ftpHost is success-----");
            Properties config = new Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            // 设置超时时间
            session.setTimeout(timeout);
            session.connect();
            log.info("-----sftp创建session成功-----");

            log.info("-----正在打开sftp连接-----");
            channel = session.openChannel("sftp");
            log.info("-----sftp连接打开成功-----");

            log.info("sftp连接中...");
            channel.connect();
            log.info("sftp连接成功");

            this.channel = (ChannelSftp) channel;
            log.info("Connected to sftp " + ftpHost + " sucess !!!");
        } catch (Exception e) {
            log.info("异常信息：" + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * 连接sftp服务器
     * @param
     * @return void
     * @author liuc
     * @date 2021/11/24 15:14
     * @throws JSchException
     */
    public synchronized void connectServer() throws JSchException {
        if (Objects.nonNull(this.channel)) {
            this.disconnect();
        }
        JSch jsch = new JSch();
        log.info("sftp正在创建session...");
        // 根据用户名，主机ip，端口获取一个Session对象
        session = jsch.getSession(this.userName, this.ftpHost, this.ftpPort);
        // 设置密码
        session.setPassword(this.password);
        log.info("sftp创建session成功");
        // 为Session对象设置properties
        Properties config = new Properties();
        config.put("StrictHostKeyChecking", "no");
        session.setConfig(config);
        log.info("设置超时时间为："+timeout+"s");
        // 设置超时时间
        session.setTimeout(timeout);
        // 通过Session建立链接
        session.connect();
        log.info("正在打开sftp连接");
        // 打开SFTP通道
        this.channel = (ChannelSftp) session.openChannel("sftp");
        // 建立SFTP通道的连接
        this.channel.connect();
        log.info("打开sftp连接成功");
    }

    /**
     * 从sftp上下载文件到本地
     *
     * @param remotePath 远程服务器文件路径：/test/
     * @param remoteFile sftp服务器文件名：test.txt
     * @param localFile  下载到本地的文件路径和名称：D:/test/test.text
     * @param closeFlag  true表示关闭连接，false表示 不关闭连接
     * @return flag 下载是否成功， true成功，false下载失败
     * @author liuc
     * @date 2021/11/24 15:14
     * @throws Exception
     */
    public synchronized boolean downloadFile(String remotePath, String remoteFile, String localFile, boolean closeFlag) throws Exception {
        long startTime=System.currentTimeMillis();
        this.isOpenConn();
        boolean flag = false;
        try {
            if (!remotePath.endsWith("/")) {
                remotePath = StringUtils.join(remotePath,"/");
            }
            this.channel.cd(remotePath);
            InputStream input = this.channel.get(remoteFile);
            log.info("获取远程目录：" + remotePath + "下文件" + remoteFile);
            // 判断本地目录是否存在，若不存在就创建文件夹
            if (this.isNotEmpty(localFile)) {
                File checkFileTemp = new File(localFile);
                if (!checkFileTemp.getParentFile().exists()) {
                    // 创建文件夹
                    checkFileTemp.getParentFile().mkdirs();
                }
            }

            FileOutputStream out = new FileOutputStream(new File(localFile));
            log.info("下载到本地文件： " + localFile);
            byte[] bt = new byte[1024];
            int length = -1;
            while ((length = input.read(bt)) != -1) {
                out.write(bt, 0, length);
            }
            log.info("下载远程文件:" + remotePath + remoteFile + "成功");

            input.close();
            out.close();
            if (closeFlag) {
                this.disconnect();
            }
            flag = true;
        } catch (SftpException e) {
            log.error("文件下载失败！" + e.getMessage());
            throw new RuntimeException("文件下载失败！");
        } catch (FileNotFoundException e) {
            log.error("下载文件到本地的路径有误！" + e.getMessage());
            throw new RuntimeException("下载文件到本地的路径有误！");
        } catch (IOException e) {
            log.error("文件写入有误！" + e.getMessage());
            throw new RuntimeException("文件写入有误！");
        }
        long endTime=System.currentTimeMillis();
        log.info("下载所需时间："+(endTime-startTime)+"ms");
        return flag;
    }

    /**
     * 从sftp上下载文件到本地
     *
     * @param remotePath     远程服务器文件路径：/test/
     * @param remoteFileName sftp服务器文件名：test.txt
     * @param localFilePath  下载到本地的文件路径：D:/test/
     * @param localFileName  下载到本地的文件名称：test.text
     * @param closeFlag      true表示关闭连接，false表示 不关闭连接
     * @return flag 下载是否成功， true成功，false下载失败
     * @author liuc
     * @date 2021/11/24 15:14
     * @throws Exception
     */
    public synchronized boolean downloadFile(String remotePath, String remoteFileName, String localFilePath, String localFileName, boolean closeFlag) throws Exception {
        this.isOpenConn();
        boolean flag = false;
        try {
            this.channel.cd(remotePath);
            InputStream input = this.channel.get(remoteFileName);
            log.info("获取远程目录：" + remotePath + "下文件" + remoteFileName);
            String localRemoteFile = localFilePath + remoteFileName;
            File checkFileTemp = null;
            // 判断本地目录是否存在，若不存在就创建文件夹
            if (this.isNotEmpty(localRemoteFile)) {
                checkFileTemp = new File(localRemoteFile);
                if (!checkFileTemp.getParentFile().exists()) {
                    // 创建文件夹
                    checkFileTemp.getParentFile().mkdirs();
                }
            }

            FileOutputStream out = new FileOutputStream(new File(localRemoteFile));
            log.info("下载到本地文件： " + localRemoteFile);
            byte[] bt = new byte[1024];
            int length = -1;
            while ((length = input.read(bt)) != -1) {
                out.write(bt, 0, length);
            }
            log.info("下载远程文件" + remotePath + remoteFileName + "成功");

            input.close();
            out.close();
            if (closeFlag) {
                this.disconnect();
            }
            flag = true;
            File upSupFile = new File(localFilePath + localFileName);
            checkFileTemp.renameTo(upSupFile);
            log.info("重命名本地文件：" + localRemoteFile + "，改为：" + localFilePath + localFileName);
        } catch (SftpException e) {
            log.error("文件下载失败！" + e.getMessage());
            throw new RuntimeException("文件下载失败！");
        } catch (FileNotFoundException e) {
            log.error("下载文件到本地的路径有误！" + e.getMessage());
            throw new RuntimeException("下载文件到本地的路径有误！");
        } catch (IOException e) {
            log.error("文件写入有误！" + e.getMessage());
            throw new RuntimeException("文件写入有误！");
        }
        return flag;
    }

    /**
     * 上传文件到sftp服务器
     *
     * @param remotePath sftp服务器路径：/test/
     * @param fileName   sftp服务器文件名：test.txt
     * @param localFile  本地文件路径和名称字符串：D:/test/test.txt
     * @param closeFlag  true表示关闭连接，false表示 不关闭连接
     * @return flag true上传成功， false上传失败
     * @author liuc
     * @date 2021/11/24 15:14
     * @throws Exception
     */
    public synchronized boolean uploadFile(String remotePath, String fileName, String localFile, boolean closeFlag) throws Exception {
        isOpenConn();
        boolean flag = false;
        InputStream input = null;
        try {
            input = new FileInputStream(localFile);

            log.info("获取本地文件");
            // 判断是否需要在远程目录上创建对应的目录
            String[] dirs = remotePath.split("\\/");
            if (Objects.isNull(dirs) || dirs.length < 1) {
                dirs = remotePath.split("\\\\");
            }
            // 获取当前目录
            String now = this.channel.pwd();
            String tempDir = null;
            for (int i = 0; i < dirs.length; i++) {
                tempDir = dirs[i];
                if (this.isNotEmpty(tempDir)) {
                    boolean dirExists = this.openDirs(tempDir);
                    if (!dirExists) {
                        this.channel.mkdir(tempDir);
                        this.channel.cd(tempDir);
                    }
                }
            }
            // 重新切换到上文当前目录
            this.channel.cd(now);

            log.info("切换至远程文件路径" + remotePath);
            // 切换到sftp服务器路径
            this.channel.cd(remotePath);
            log.info("上传文件开始");
            this.channel.put(input, fileName);
            log.info("上传文件结束");
            // 设置文件权限 
            this.channel.chmod(444, fileName);
            flag = true;
        } catch (SftpException e) {
            log.error("上传文件失败！" + e.getMessage());
            throw new RuntimeException("上传文件失败！");
        } catch (FileNotFoundException e) {
            log.error("上传文件找不到！" + e.getMessage());
            throw new RuntimeException("上传文件找不到！");
        } finally {
            if (Objects.nonNull(input)) {
                try {
                    input.close();
                } catch (Exception e2) {
                    log.error("输入流关闭失败：" + e2.getMessage());
                }
            }
            if (closeFlag) {
                this.disconnect();
            }
        }
        return flag;
    }

    /**
     * 外部类关闭连接
     * @param
     * @return void
     * @author liuc
     * @date 2021/11/24 15:16
     * @throws
     */
    public synchronized void disconn() {
        this.disconnect();
    }

    /**
     * 下载文件
     *
     * @param remotePath
     * @param remoteFile
     * @return
     * @author liuc
     * @date 2021/11/24 15:16
     * @throws Exception
     */
    public synchronized InputStream downloadFile(String remotePath, String remoteFile) throws Exception {
        this.isOpenConn();
        try {
            this.channel.cd(remotePath);
            return this.channel.get(remoteFile);
        } catch (Exception e) {
            log.error("文件下载失败！");
            throw new Exception("文件下载失败！", e);
        }
    }

    /**
     * 删除文件
     *
     * @param directory  要删除文件所在目录
     * @param deleteFile 要删除的文件
     * @param closeFlag
     * @author liuc
     * @date 2021/11/24 15:16
     * @throws Exception
     */
    public synchronized void delete(String directory, String deleteFile, boolean closeFlag) throws Exception {
        this.isOpenConn();
        try {
            log.info("开始删除文件！");
            this.channel.cd(directory);
            this.channel.rm(deleteFile);
            if (closeFlag) {
                this.disconnect();
            }
        } catch (SftpException e) {
            log.error("删除文件失败！");
            throw new Exception("删除文件失败！", e);
        }
    }

    /**
     * 列出目录下的文件
     *
     * @param directory 要列出的目录
     * @return
     * @author liuc
     * @date 2021/11/24 15:16
     * @throws Exception
     */
    public Vector<?> listFiles(String directory) throws Exception {
        this.isOpenConn();
        return this.channel.ls(directory);
    }

    /**
     * 关闭连接
     * @param
     * @return void
     * @author liuc
     * @date 2021/11/24 15:17
     * @throws
     */
    private synchronized void disconnect() {
        if (Objects.nonNull(this.channel)) {
            this.channel.exit();
        }
        if (Objects.nonNull(this.session)) {
            this.session.disconnect();
        }
        this.channel = null;
        log.info("关闭连接成功");
    }

    /**
     * 打开指定目录
     * @param directory
     * @return boolean
     * @author liuc
     * @date 2021/11/24 15:17
     * @throws
     */
    private boolean openDirs(String directory) throws Exception {
        this.isOpenConn();
        try {
            this.channel.cd(directory);
            return true;
        } catch (SftpException e) {
            return false;
        }
    }

    /**
     * 检查是否打开连接
     * @param
     * @return void
     * @author liuc
     * @date 2021/11/24 15:17
     * @throws
     */
    private void isOpenConn() throws Exception {
        if (Objects.isNull(this.channel)) {
            throw new Exception("sftp连接未打开");
        }
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFtpHost() {
        return ftpHost;
    }

    public void setFtpHost(String ftpHost) {
        this.ftpHost = ftpHost;
    }

    public int getFtpPort() {
        return ftpPort;
    }

    public void setFtpPort(int ftpPort) {
        this.ftpPort = ftpPort;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public String getPassphrase() {
        return passphrase;
    }

    public void setPassphrase(String passphrase) {
        this.passphrase = passphrase;
    }

    private boolean isNotEmpty(String str) {
        return !this.isEmpty(str);
    }

    private boolean isEmpty(String str) {
        return Objects.isNull(str) || str.length() == 0;
    }

}
