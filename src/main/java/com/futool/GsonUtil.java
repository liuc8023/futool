package com.futool;

import java.lang.reflect.Type;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;

/**
 * Json转换工具类
 */
public class GsonUtil {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    private static final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private static final DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");

    private static final JsonSerializer<LocalDateTime> dateTimeSerializer
            = (obj, type, ctx) -> new JsonPrimitive(dateTimeFormatter.format(obj));
    private static final JsonSerializer<LocalDate> dateSerializer
            = (obj, type, ctx) -> new JsonPrimitive(dateFormatter.format(obj));
    private static final JsonSerializer<LocalTime> timeSerializer
            = (obj, type, ctx) -> new JsonPrimitive(timeFormatter.format(obj));

    private static final JsonDeserializer<LocalDateTime> dateTimeDeserializer
            = (json, type, ctx) -> LocalDateTime.parse(json.getAsJsonPrimitive().getAsString(), dateTimeFormatter);
    private static final JsonDeserializer<LocalDate> dateDeserializer
            = (json, type, ctx) -> LocalDate.parse(json.getAsJsonPrimitive().getAsString(), dateFormatter);
    private static final JsonDeserializer<LocalTime> timeDeserializer
            = (json, type, ctx) -> LocalTime.parse(json.getAsJsonPrimitive().getAsString(), timeFormatter);

    /**
     * 不用创建对象,直接使用Gson.就可以调用方法
     */
    private static Gson gson = null;

    /**
     * 判断gson对象是否存在了,不存在则创建对象
     */
    static {
        if (gson == null) {
            // 当使用GsonBuilder方式时属性为空的时候输出来的json字符串是有键值key的,显示形式是"key":null，而直接new出来的就没有"key":null的
            GsonBuilder builder = new GsonBuilder();
            builder.disableHtmlEscaping();
            builder.enableComplexMapKeySerialization();
            // builder.excludeFieldsWithoutExposeAnnotation();
            builder.setDateFormat("yyyy-MM-dd HH:mm:ss");
            builder.registerTypeAdapter(LocalDateTime.class, dateTimeSerializer);
            builder.registerTypeAdapter(LocalDate.class, dateSerializer);
            builder.registerTypeAdapter(LocalTime.class, timeSerializer);
            builder.registerTypeAdapter(LocalDateTime.class, dateTimeDeserializer);
            builder.registerTypeAdapter(LocalDate.class, dateDeserializer);
            builder.registerTypeAdapter(LocalTime.class, timeDeserializer);
            gson = builder.create();
        }
    }

    private GsonUtil() {}

    public static Type makeJavaType(Type rawType, Type... typeArguments) {
        return TypeToken.getParameterized(rawType, typeArguments).getType();
    }

    /**
     * json 转对象
     * @param json
     * @return
     */
    public static JsonObject jsonToObj(String json) {
        JsonElement element = gson.fromJson (json, JsonElement.class);
        JsonObject jsonObj = element.getAsJsonObject();
        return jsonObj;
    }


    /**
     * 将对象转成json格式
     * @param object
     * @return String
     */
    public static String toJson(Object object) {
        String gsonString = null;
        if (gson != null) {
            gsonString = gson.toJson(object);
        }
        return gsonString;
    }

    /**
     * 将json字符串转成特定的cls的对象
     * @param gsonString
     * @param cls
     * @return
     */
    public static <T> T toBean(String gsonString, Class<T> cls) {
        T t = null;
        if (gson != null) {
            //传入json对象和对象类型,将json转成对象
            t = gson.fromJson(gsonString, cls);
        }
        return t;
    }

    /**
     * json字符串转成list
     * @param gsonString
     * @param cls
     * @return
     */
    public static <T> List<T> toList(String gsonString, Class<T> cls) {
        List<T> list = null;
        if (gson != null) {
            //根据泛型返回解析指定的类型,TypeToken<List<T>>{}.getType()获取返回类型
            list = gson.fromJson(gsonString, new TypeToken<List<T>>() {
            }.getType());
        }
        return list;
    }

    /**
     * json字符串转成list中有map的
     * @param gsonString
     * @return
     */
    public static <T> List<Map<String, T>> toListMaps(String gsonString) {
        List<Map<String, T>> list = null;
        if (gson != null) {
            list = gson.fromJson(gsonString,
                    new TypeToken<List<Map<String, T>>>() {
                    }.getType());
        }
        return list;
    }

    /**
     * json字符串转成map的
     * @param gsonString
     * @return
     */
    public static <T> Map<String, T> toMap(String gsonString) {
        Map<String, T> map = null;
        if (gson != null) {
            map = gson.fromJson(gsonString, new TypeToken<Map<String, T>>() {
            }.getType());
        }
        return map;
    }

    public static String jsonElementAsString(JsonElement jsonElement) {
        return jsonElement == null ? null : jsonElement.getAsString();
    }


    public static void main(String[] args) {
        String s = "{\"goods_id\":\"140861765\",\"cat_id\":\"210\",\"goods_sn\":\"171073501\",\"goods_sn_back\":\"171073501\",\"goods_upc\":null,\"goods_name\":\"Lace-Up Boxer Swimming Trunks\"}";
        Map<String, Object> map = toMap(s);
        System.out.println(map);
    }

}
