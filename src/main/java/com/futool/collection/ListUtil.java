package com.futool.collection;

import com.futool.BigDecimalUtil;
import com.futool.UtilValidate;
import org.apache.commons.collections4.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author liuc
 * @version V1.0
 * @description list工具类
 * @date 2021/8/24 11:19
 * @since JDK1.8
 */
public class ListUtil {
    /***
     * @description 通过遍历两个List按照str字段相等取出towList存放到新的List中并返回
     * @param oneList
     * @param towList
     * @param str
     * @return java.util.List<java.util.Map < java.lang.String, java.lang.Object>>
     * @author liuc
     * @date 2021/8/24 11:24 
     * @throws
     */
    public static List<Map<String, Object>> compareListHitData(List<Map<String, Object>> oneList, List<Map<String, Object>> towList, String str) {
        List<Map<String, Object>> resultList = oneList.stream().map(map -> towList.stream()
                .filter(m -> Objects.equals(m.get(str), map.get(str)))
                .findFirst().map(m -> {
                    map.putAll(m);
                    return m;
                }).orElse(null))
                .collect(Collectors.toList());
        return resultList.stream().filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    /***
     * @description 按照str字段将后面一个list合并到前面的list里
     * 将m2合并到m1里，相同的key对应的value，以m2的为准
     * @param m1
     * @param m2
     * @param str
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     * @author liuc
     * @date 2021/8/24 14:12
     * @throws
     */
    public static List<Map<String, Object>> mergeList(List<Map<String, Object>> m1, List<Map<String, Object>> m2, String str) {
        m1.addAll(m2);
        Set<String> set = new HashSet<String>(8);
        return m1.stream().filter(map -> map.get(str) != null).collect(Collectors.groupingBy(o->{
            //暂存所有key
            set.addAll(o.keySet());
            return o.get(str);
        })).entrySet().stream().map(o->{
            //合并
            Map<String,Object> map = o.getValue().stream().flatMap(m->{
                return m.entrySet().stream();
            }).collect(Collectors.toMap(Map.Entry::getKey,t->t.getValue()==null?"":t.getValue(),(a,b)->b));
            //如果没有的key赋值null
            set.stream().forEach(k->{
                if(!map.containsKey(k)){
                    map.put(k,null);
                }
            });
            return map;
        }).collect(Collectors.toList());
    }


    /**
     * 通过指定的key获取该key对应的value的list集合，并去重
     * @param list
     * @param key
     * @return java.util.List<java.lang.Object>
     * @author liuc
     * @date 2021/10/16 9:32
     * @throws
     */
    public static List<Object> getValueListFromKey(List<Map<String,Object>> list,String key){
        List<Object> list1 = list.stream().flatMap(each -> each.entrySet().stream())
                .filter(each -> each.getKey().contains(key)).map(each -> each.getValue())
                .collect(Collectors.toList());
        return list1.stream().distinct().filter(Objects::nonNull)
                .filter(e -> e!="")
                .collect(Collectors.toList());
    }

    /**
     * 通过指定的key获取该key对应的value的分类list集合
     * @author liuc
     * @date 2021/11/11 21:05
     * @since JDK1.8
     * @version V1.0
     */
    public static Map<String,List<Map<String,Object>>> getListGroupByKey(List<Map<String,Object>> list,String key){
        return list.stream().collect(
                Collectors.groupingBy(map ->(String)map.get(key)));
    }

    /**
     * 指定去重的列
     * @author liuc
     * @date 2021/11/11 21:38
     * @since JDK1.8
     * @version V1.0
     */
    @SuppressWarnings("unchecked")
    public static List uniqueList(List<Map<String, String>> rows) {

        List test = rows.stream().collect(Collectors.toMap(e -> {
            return e.get("column1") + "_" + e.get("column2");
        }, e-> {
            List result = new ArrayList();
            result.add(e);
            return result;
        }, (a, b) -> {
            a.addAll(b);
            return a;
        })).entrySet().stream().filter(entry -> entry.getValue().size() > 1)
                .collect(ArrayList::new, (list, entry)->{
                    list.addAll(entry.getValue());
                },(list1, list2) ->{
                    list1.addAll(list2);
                });
        return test;
    }
    
    /**
     * 获取重复的key
     * @author liuc
     * @date 2021/11/11 21:37
     * @since JDK1.8
     * @version V1.0
     */
    @SuppressWarnings("unchecked")
    public static Set getMultiKeys(List<Map<String, String>> rows) {

        Set test = rows.stream().collect(Collectors.toMap(e -> {
            return e.get("column1") + "_" + e.get("column2");
        }, e-> {
            List result = new ArrayList();
            result.add(e);
            return result;
        }, (a, b) -> {
            a.addAll(b);
            return a;
        })).entrySet().stream().filter(entry -> entry.getValue().size() > 1)
                .collect(HashSet::new, (list, entry)->{
                    list.add(entry.getKey());
                },(list1, list2) ->{
                    list1.addAll(list2);
                });
        return test;
    }

    /**
     * 移除list中的null值和空字符串
     * @param list
     * @return java.util.List<java.lang.String>
     * @author liuc
     * @date 2021/11/12 16:30
     * @throws
     */
    public static List<String> getListNoNull(List<String> list){
        return list.stream().filter(Objects::nonNull)
                .filter(e -> e!="")
                .collect(Collectors.toList());
    }

    /**
     * 循环截取某列表进行分页
     * @param dataList 分页数据
     * @param pageSize 页面大小
     * @param pageNo 当前页面
     * @return java.util.List<java.lang.String>
     * @author liuc
     * @date 2021/11/22 20:50
     * @throws
     */
    public static List<Object> pageList(List<Object> dataList,int pageNo,int pageSize) {
        List<Object> currentPageList = new ArrayList<Object>();
        if (dataList != null && dataList.size() > 0) {
            int currIdx = (pageNo > 1 ? (pageNo - 1) * pageSize : 0);
            for (int i = 0; i < pageSize && i < dataList.size() - currIdx; i++) {
                Object data = dataList.get(currIdx + i);
                currentPageList.add(data);
            }
        }
        return currentPageList;
    }

    /**
     * List<Map<String, Object> 按照sort字段排序，order:desc-降序排序，asc-升序排序
     * @param mapList
     * @param sort
     * @param order
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     * @author liuc
     * @date 2021/12/8 16:53
     * @throws
     */
    public static List<Map<String, Object>> sortListMap(List<Map<String, Object>> mapList, final Object sort, final String order) {
        Collections.sort(mapList, new Comparator<Map<String, Object>>() {
            @Override
            public int compare(Map<String, Object> o1, Map<String, Object> o2) {
                BigDecimal bd1 = BigDecimalUtil.objToBigDecimal(o1.get(sort));
                BigDecimal bd2 = BigDecimalUtil.objToBigDecimal(o2.get(sort));
                if ("DESC".equals(order.toUpperCase())) {
                    return bd2.compareTo(bd1);
                } else {
                    return bd1.compareTo(bd2);
                }
            }
        });
        return mapList;
    }

    /**
     * 根据map中的某个key 去除List中重复的map
     *
     * @param list
     * @param mapKey
     * @return
     * @author liuc
     */
    public static List<Map<String, Object>> removeRepeatMapByKey(List<Map<String, Object>> list, String mapKey) {
        if (UtilValidate.isEmpty(list)) {
            return null;
        }
        // 把list中的数据转换成msp,去掉同一id值多余数据，保留查找到第一个id值对应的数据
        List<Map<String, Object>> listMap = new ArrayList<>();
        Map<String, Map<String,Object>> msp = new HashMap<>();
        for (int i = list.size() - 1; i >= 0; i--) {
            Map<String,Object> map = list.get(i);
            String id = (String) map.get(mapKey);
            map.remove(mapKey);
            msp.put(id, map);
        }
        // 把msp再转换成list,就会得到根据某一字段去掉重复的数据的List<Map>
        Set<String> mspKey = msp.keySet();
        for (String key : mspKey) {
            Map<String,Object> newMap = msp.get(key);
            newMap.put(mapKey, key);
            listMap.add(newMap);
        }
        return listMap;
    }

    /**
     * 该方法可根据指定字段对List<Map>中的数据去重，存在重复的，保存第一组Map
     * @param originMapList
     * @param keys
     * @return
     */
    public static List<Object> deleteDuplicatedMapFromListByKeys(List<Map<String,Object>> originMapList, List keys) {
        if (CollectionUtils.isEmpty(originMapList)) {
            return null;
        }
        Map<String,Object> tempMap = new HashMap<String,Object>(8);
        for (Map<String,Object> originMap : originMapList) {
            String objHashCode = "";
            for (Object key : keys) {
                String value = originMap.get(key) != null ? originMap.get(key).toString() : "";
                objHashCode += value.hashCode();
            }
            tempMap.put(objHashCode, originMap);
        }
        return new ArrayList<Object>(tempMap.values());
    }

    /**
     * 该方法查找两个List<Map>中的数据差集
     * @param originMapList
     * @param newMapList
     * @return
     */
    public static List<Map<String,Object>> differenceMapByList(List<Map<String,Object>> originMapList,List<Map<String,Object>> newMapList){
        if(CollectionUtils.isEmpty(originMapList)) return null;
        if (CollectionUtils.isEmpty(newMapList)) return null;
        List<Map<String,Object>> retList = new ArrayList<>();
        List<Map<String,Object>> listAll = new ArrayList<>();
        listAll.addAll(originMapList);
        listAll.addAll(newMapList);
        //过滤相同的集合
        List<Map<String,Object>> listSameTemp = new ArrayList<>();
        originMapList.stream().forEach(a -> {
            if(newMapList.contains(a))
                listSameTemp.add(a);
        });
        retList = listSameTemp.stream().distinct().collect(Collectors.toList());
        //去除重复的，留下两个集合中的差集
        listAll.removeAll(retList);
        retList = listAll;
        return retList;
    }
}
