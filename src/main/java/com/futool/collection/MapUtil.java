package com.futool.collection;

import com.futool.StringUtils;
import com.futool.UtilValidate;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 */
public class MapUtil {

    /**
     * 将Map中的key首字母转小写
     * @param sourceMap
     * @return
     */
    public static Map<String,Object> keyInitialToLowCase(Map<String,Object> sourceMap){
        Map<String,Object> result = new HashMap<>();
        if (UtilValidate.isEmpty(sourceMap)) {
            return result;
        }
        Set<Map.Entry<String,Object>> entrySet = sourceMap.entrySet();
        for (Map.Entry<String,Object> entry:entrySet) {
            String key = entry.getKey();
            Object value = entry.getValue();
            //首字母转成小写
            result.put(StringUtils.lowerFirst(key),value);
        }
        return result;
    }

    /**
     * 将Map中的key首字母转大写
     * @param sourceMap
     * @return
     */
    public static Map<String,Object> keyInitialToUpperCase(Map<String,Object> sourceMap){
        Map<String,Object> result = new HashMap<>();
        if (UtilValidate.isEmpty(sourceMap)) {
            return result;
        }
        Set<Map.Entry<String,Object>> entrySet = sourceMap.entrySet();
        for (Map.Entry<String,Object> entry:entrySet) {
            String key = entry.getKey();
            Object value = entry.getValue();
            //首字母转成小写
            result.put(StringUtils.upperFirst(key),value);
        }
        return result;
    }
}
