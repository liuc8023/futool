package com.futool;

import com.hankcs.hanlp.HanLP;
import com.hankcs.hanlp.dictionary.py.Pinyin;
import com.hankcs.hanlp.dictionary.py.PinyinDictionary;

import java.util.List;

public class HanLPutils {
    /**
     * 根据输入的汉字字符串翻译成带有音标的拼音字符串，所有拼音字母都是小写
     * 如：我爱你中国——>wǒàinǐzhōngguó
     * @param chineseCharacters
     * @return
     */
    public static String getPinyinWithToneMark(String chineseCharacters){
        StringBuffer sbuff = new StringBuffer();
        if (UtilValidate.isNotEmpty(chineseCharacters)) {
            chineseCharacters = chineseCharacters.replaceAll(" ","");
            //判断输入的字符串是否是中文
            if (StringUtils.checkNameCheseNoSpeSymbol(chineseCharacters)) {
                List<Pinyin> pinyinList = HanLP.convertToPinyinList(chineseCharacters);
                for (Pinyin pinyin:pinyinList) {
                    sbuff.append(pinyin.getPinyinWithToneMark());
                }
            }
        }
        return sbuff.toString();
    }

    /**
     * 根据输入的汉字字符串翻译成带有音标的拼音字符串，所有拼音字母都是小写
     * 如：我爱你中国——>wǒàinǐzhōngguó
     * @param chineseCharacters
     * @return
     */
    public static String getPinyinWithToneMarkToUpperCase(String chineseCharacters){
        StringBuffer sbuff = new StringBuffer();
        if (UtilValidate.isNotEmpty(chineseCharacters)) {
            chineseCharacters = chineseCharacters.replaceAll(" ","");
            //判断输入的字符串是否是中文
            if (StringUtils.checkNameCheseNoSpeSymbol(chineseCharacters)) {
                chineseCharacters = HanLP.convertToTraditionalChinese(chineseCharacters);
                List<Pinyin> pinyinList =  PinyinDictionary.convertToPinyin(chineseCharacters);
                for (Pinyin pinyin:pinyinList) {
                    String py = pinyin.getPinyinWithToneMark();
                    String str = py.substring(0, 1).toUpperCase() + py.substring(1);
                    sbuff.append(str);
                }
            }
        }
        return sbuff.toString();
    }
}
