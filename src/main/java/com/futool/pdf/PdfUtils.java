package com.futool.pdf;

import com.futool.JsonUtil;
import com.futool.UtilValidate;
import com.futool.io.FileUtil;
import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.Pipeline;
import com.itextpdf.tool.xml.XMLWorker;
import com.itextpdf.tool.xml.XMLWorkerFontProvider;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.itextpdf.tool.xml.html.CssAppliersImpl;
import com.itextpdf.tool.xml.html.Tags;
import com.itextpdf.tool.xml.net.FileRetrieve;
import com.itextpdf.tool.xml.net.ReadingProcessor;
import com.itextpdf.tool.xml.parser.XMLParser;
import com.itextpdf.tool.xml.pipeline.css.CSSResolver;
import com.itextpdf.tool.xml.pipeline.css.CssResolverPipeline;
import com.itextpdf.tool.xml.pipeline.end.PdfWriterPipeline;
import com.itextpdf.tool.xml.pipeline.html.AbstractImageProvider;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipelineContext;
import com.itextpdf.tool.xml.pipeline.html.ImageProvider;
import freemarker.template.Configuration;
import freemarker.template.Template;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Entities;
import org.jsoup.select.Elements;
import java.io.*;
import java.nio.charset.Charset;
import java.util.Map;

/**
 * pdf 导出工具类
 * @author liuc
 */
@Slf4j
public class PdfUtils {

//    private static Configuration freemarkerCfg;
//
//    static {
//        //freemarker包
//        freemarkerCfg = new Configuration(Configuration.VERSION_2_3_23);
//        //设置模板加载文件夹
//        try {
//            freemarkerCfg.setDirectoryForTemplateLoading(new File(ResourceUtils.getURL("classpath:").getPath() + "resources/templates"));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

    /**
     *
     * @param template
     * @param variables
     * @return
     */
    public static String htmlGenerate(String template, Map<String, Object> variables) {
        String htmlStr = null;
        try {
            Configuration config = new Configuration(Configuration.VERSION_2_3_23);
            config.setDirectoryForTemplateLoading(new File(ResourceUtils.getParent(template)));
            config.setLogTemplateExceptions(false);
            config.setWrapUncheckedExceptions(true);
            Template tp = config.getTemplate(ResourceUtils.getFileName(template));
            StringWriter stringWriter = new StringWriter();
            BufferedWriter writer = new BufferedWriter(stringWriter);
            tp.process(variables, writer);
            htmlStr = formatHtml(stringWriter.toString());
            writer.flush();
            writer.close();
        } catch (Exception e) {
            log.error("", e);
        }
        return htmlStr;
    }

    /**
     * 创建pdf
     * @param htmlTemplate 模板路径
     * @param dataMap 输出到模板的具体内容
     * @param targetPdf 创建好pdf所存储的目标路径
     * @param pageSize 纸张类型
     * @param header 页眉
     * @param isFooter 页脚
     * @param watermark 水印
     * @throws Exception
     */
    public static void createPdf(String htmlTemplate, Map<String, Object> dataMap,
                                 String targetPdf, Rectangle pageSize, String header, boolean isFooter, File watermark)
            throws Exception {
        UtilValidate.notNull(htmlTemplate, "FilePath can not be null");
        UtilValidate.notNull(dataMap, "Content can not be null");
        /**
         * 根据freemarker模板生成html
         */
        //将参数中的换行符\n替换成<br/>标签
        String str = JsonUtil.toJson(dataMap).replace("\\n","<br/>");
        dataMap = JsonUtil.jsonToMap(str);
        String htmlStr = htmlGenerate(htmlTemplate, dataMap);
        //编码格式
        final String charsetName = "UTF-8";
        File file = FileUtil.getFile(targetPdf);
        OutputStream out = new FileOutputStream(file);
        /**
         * 设置文档格式,数字边距
         */
        Document document = new Document(pageSize);
        document.setMargins(30, 30, 30, 30);
        PdfWriter writer = PdfWriter.getInstance(document, out);
        /**
         * 添加页码
         */
        PdfBuilder builder = new PdfBuilder(header, 10, pageSize, watermark, isFooter);
        writer.setPageEvent(builder);
        // 打开文档
        document.open();
//        // 生成第一页
//        document.newPage();
//        //设置作者
//        document.addAuthor("liuc");
//        //设置创建日期
//        document.addCreationDate();
//        // 设置创建者
//        document.addCreator("www.baidu.com");
//        // 设置生产者
//        document.addProducer();
//        // 设置关键字
//        document.addKeywords("my");
//        //设置标题
//        document.addTitle("Set Attribute Example");
//        //设置主题
//        document.addSubject("An example to show how attributes can be added to pdf files.");

        /**
         * html内容解析
         */
        HtmlPipelineContext htmlContext =
                new HtmlPipelineContext(new CssAppliersImpl(new XMLWorkerFontProvider() {
                    @Override
                    public Font getFont(String fontName, String encoding, float size, final int style) {
                        Font font = null;
                        try {
                            /**
                             * 根据CSS中不同的字体名返回不同的字体实例
                             * 若出现*`Font '/yourFontDir/yourFont.ttf' with 'Identity-H' is not recognized`*这样的异常，请尝试在你的字体文件路径后面加上 [,0 |,1|,2]，即：
                             * BaseFont.createFont("/yourFontDir/yourFont.ttf,1",BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
                             * 如有特殊需求，只需要在FontsEnum加入新的定义，然后再HTML模板中加入样式就行了，比如
                             * 1、在FontsEnum枚举类中加入配置
                             *    //微软雅黑
                             *    MICROSOFT_ELEGANT_BLAC("MicrosoftYaHei", "templates/fonts/msyh.ttc,0", PdfEncodings.IDENTITY_H),
                             * 2、在 HTML中需要特殊样式的位置加上 font-family:MicrosoftYaHei
                             *    <h1 style="font-family:MicrosoftYaHei">内容</h1>
                             * 3、注意yourFontDir中有你需要的样式库如/yourFontDir/msyh.ttc
                             */
                            if (fontName == null) {
                                //默认宋体
                                fontName = "SongTi";
                            }
                            Map<String,Object> fontsEnumMap = FontsEnum.of(fontName);
                            fontName = (String) fontsEnumMap.get("fontName");
                            String encode = (String) fontsEnumMap.get("encoding");
                            if (fontName == null) {
                                log.error("字体不存在,请检查!");
                                throw new RuntimeException("字体不存在,请检查!");
                            }
                            return new Font(BaseFont.createFont(fontName, encode, BaseFont.NOT_EMBEDDED), size,
                                    style);
                        } catch (Exception e) {
                            log.error("", e);
                        }
                        return font;
                    }
                })) {
                    @Override
                    public HtmlPipelineContext clone() throws CloneNotSupportedException {
                        HtmlPipelineContext context = super.clone();
                        ImageProvider imageProvider = this.getImageProvider();
                        context.setImageProvider(imageProvider);
                        return context;
                    }
                };

        /**
         * 图片解析
         */
        htmlContext.setImageProvider(new AbstractImageProvider() {

            String rootPath = "C:\\Users\\Administrator\\Desktop\\刘亦菲\\";

            @Override
            public String getImageRootPath() {
                return rootPath;
            }

            @Override
            public Image retrieve(String src) {
                if (UtilValidate.isEmpty(src)) {
                    return null;
                }
                try {
                    Image image = Image.getInstance(new File(rootPath, src).toURI().toString());
                    /**
                     * 图片显示位置
                     */
                    image.setAbsolutePosition(400, 400);
                    if (image != null) {
                        store(src, image);
                        return image;
                    }
                } catch (Exception e) {
                    log.error("", e);
                }
                return super.retrieve(src);
            }
        });
        htmlContext.setAcceptUnknown(true).autoBookmark(true)
                .setTagFactory(Tags.getHtmlTagProcessorFactory());

        /**
         * css解析
         */
        CSSResolver cssResolver = XMLWorkerHelper.getInstance().getDefaultCssResolver(true);
        cssResolver.setFileRetrieve(new FileRetrieve() {
            @Override
            public void processFromStream(InputStream in, ReadingProcessor processor) throws IOException {
                try (InputStreamReader reader = new InputStreamReader(in, charsetName)) {
                    int i = -1;
                    while (-1 != (i = reader.read())) {
                        processor.process(i);
                    }
                } catch (Exception e) {
                    log.error("", e);
                }
            }

            /**
             * 解析href
             */
            @Override
            public void processFromHref(String href, ReadingProcessor processor) throws IOException {
                InputStream is = new ByteArrayInputStream(href.getBytes());
                try {
                    InputStreamReader reader = new InputStreamReader(is, charsetName);
                    int i = -1;
                    while (-1 != (i = reader.read())) {
                        processor.process(i);
                    }
                } catch (Exception e) {
                    log.error("", e);
                }

            }
        });

        HtmlPipeline htmlPipeline =
                new HtmlPipeline(htmlContext, new PdfWriterPipeline(document, writer));
        Pipeline<?> pipeline = new CssResolverPipeline(cssResolver, htmlPipeline);
        XMLWorker worker = null;
        worker = new XMLWorker(pipeline, true);
        XMLParser parser = new XMLParser(true, worker, Charset.forName(charsetName));
        try (InputStream inputStream = new ByteArrayInputStream(htmlStr.getBytes())) {
            parser.parse(inputStream, Charset.forName(charsetName));
        }
        // 关闭文档
        document.close();
    }


    /**
     * jsoup 格式化html
     * @param html
     * @return
     */
    private static String formatHtml(String html) {
        org.jsoup.nodes.Document doc = Jsoup.parse(html);
        // 去除过大的宽度
        String style = doc.attr("style");
        if ((!style.isEmpty()) && style.contains("width")) {
            doc.attr("style", "");
        }
        Elements divs = doc.select("div");
        for (Element div : divs) {
            String divStyle = div.attr("style");
            if ((!divStyle.isEmpty()) && divStyle.contains("width")) {
                div.attr("style", "");
            }
        }
        // jsoup生成闭合标签
        doc.outputSettings().syntax(org.jsoup.nodes.Document.OutputSettings.Syntax.xml);
        doc.outputSettings().escapeMode(Entities.EscapeMode.xhtml);
        return doc.html();
    }

}
