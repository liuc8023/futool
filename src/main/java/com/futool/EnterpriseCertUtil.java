package com.futool;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.security.SecureRandom;
import java.util.*;
import java.util.regex.Pattern;

/**
 * @author liuc
 * @version V1.0
 * @description 生成企业组织机构代码、营业执照代码、税务登记号码、统一社会信用代码并校验
 * @date 2021/9/22 19:25
 * @since JDK1.8
 */
@Slf4j
public class EnterpriseCertUtil {
    /**
     * 代码字符集
     */
    private static final String MECHANISM_CODE_STRING = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXY";

    /**
     * 9位组织机构加权因子
     */
    private static final int[] FACTOR = {3,7,9,10,5,8,4,2};

    /**
     * 验证组织机构代码字符长度及格式
     */
    private static final String MECHANISM_CODE_REGEX = "[" + MECHANISM_CODE_STRING + "]{9}";

    private static final List<Character> MECHANISM_CODES = new ArrayList<>();

    private static final char[] MECHANISM_CODE_ARRAY = MECHANISM_CODE_STRING.toCharArray();

    /**
     * 统一信用代码加权因子
     */
    private static final int[] WEIGHT = {1, 3, 9, 27, 19, 26, 16, 17, 20, 29, 25, 13, 8, 24, 10, 30, 28};

    /**
     * 最后一位编码
     */
    private static final List<String> LAST_CODE = Arrays.asList(
            "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
            "A", "B", "C", "D", "E", "F", "G", "H", "J", "K",
            "L", "M", "N", "P", "Q", "R", "T", "U", "W", "X", "Y"
    );

    static {
        for (char c : MECHANISM_CODE_ARRAY) {
            MECHANISM_CODES.add(c);
        }
    }

    /**
     * @description 生成企业组织机构代码
     *  全国组织机构代码由八位数字（或大写拉丁字母）本体代码和一位数字（或大写拉丁字母）校验码组成
     * @author liuc
     * @date 2021/9/22 19:27
     * @since JDK1.8
     * @version V1.0
     */
    public static String getOrganizationCode(){
        String data = "";
        String yz = "";
        int a = 0;
        //随机生成英文字母和数字
        for (int i = 0; i < FACTOR.length; i++){
            String word = getCharAndNumr(1,0).toUpperCase();
            if (word.matches("[A-Z]")) {
                a += FACTOR[i] * getAsc(word);
            }else{
                a += FACTOR[i] * Integer.parseInt(word);
            }
            data += word;
        }
        //C9代表校验码。用已经生成的前8位加权后与11取余，然后用11减
        int c9 = 11 - a % 11;
        //判断c9大小，安装 X 0 或者C9
        if (c9 == 10) {
            //当C9的值为10时，校验码应用大写的拉丁字母X表示；
            yz = "X";
        } else if (c9 == 11) {
            //当C9的值为11时校验码用0表示;
            yz = "0";
        } else {
            //否则C9本身
            yz = c9 + "";
        }
        data += "-"+yz;
        return data.toUpperCase();
    }

    /**
     * @description 生成营业执照代码
     * @author liuc
     * @date 2021/9/22 19:28
     * @since JDK1.8
     * @version V1.0
     */
    public static String getBusinessLisenseCode(){
        String data = "";
        //随机生成14位数字
        String number = getCharAndNumr(14,1);
        //获取校验后的第15位
        String yz = getBusinesslicenseCheckBit(number)+"";
        //拼凑
        data = number+yz;
        return data.toUpperCase();
    }

    /**
     * @description 生成税务登记号码
     * @author liuc
     * @date 2021/9/22 19:28
     * @since JDK1.8
     * @version V1.0
     */
    public static String getTAX_REGISTRATION_CODE(){
        String data = "";
        String first = "73"+getCharAndNumr(4,2);
        String end = getOrganizationCode();
        data= first+end;
        data =data.toUpperCase().replaceAll("-","");
        if (!test5(data.toUpperCase())) {
            getTAX_REGISTRATION_CODE();
        }
        return data;
    }

    /**
     * @description 生成统一社会信用代码
     * @author liuc
     * @date 2021/9/22 19:29
     * @since JDK1.8
     * @version V1.0
     */
    public static String getSOCIAL_CREDIT_CODE(){
        String data = "";
        String first = "Y2"+getCharAndNumr(6,3)+getCharAndNumr(9,3);
        String end = String.valueOf(getUSCCCheckBit(first));
        data = first + end;
        if (!test4(data.toUpperCase())) {
            getSOCIAL_CREDIT_CODE();
        }
        return data.toUpperCase();
    }

    public static String getCharAndNumr(int length,int status) {
        SecureRandom random = new SecureRandom();
        StringBuffer valSb = new StringBuffer();
        String charStr = "0123456789abcdefghijklmnopqrstuvwxyz";
        if (status == 1) {
            charStr = "0123456789";
        }
        if (status == 2) {
            charStr = "0123456789";
        }
        if (status == 3) {
            charStr = "0123456789ABCDEFGHJKLMNPQRTUWXYZ";
        }
        int charLength = charStr.length();
        for (int i = 0; i < length; i++) {
            int index = random.nextInt(charLength);
            if (status==1&&index==0){ index =3;}
            valSb.append(charStr.charAt(index));
        }
        return valSb.toString();
    }

    private static char getUSCCCheckBit(String businessCode) {
        if (("".equals(businessCode)) || businessCode.length() != 17) {
            return 0;
        }
        String baseCode = "0123456789ABCDEFGHJKLMNPQRTUWXY";
        char[] baseCodeArray = baseCode.toCharArray();
        Map<Character, Integer> codes = new HashMap<Character, Integer>();
        for (int i = 0; i < baseCode.length(); i++) {
            codes.put(baseCodeArray[i], i);
        }
        char[] businessCodeArray = businessCode.toCharArray();

        int sum = 0;
        for (int i = 0; i < 17; i++) {
            Character key = businessCodeArray[i];
            if (baseCode.indexOf(key) == -1) {
                return 0;
            }
            sum += (codes.get(key) * WEIGHT[i]);
        }
        int value = 31 - sum % 31;
        if(value == 31){
            value = 0;
        }
        return baseCodeArray[value];
    }

    public static int getAsc(String st) {
        byte[] gc = st.getBytes();
        int ascNum = (int) gc[0] - 55;
        return ascNum;
    }

    /**
     * 校验 营业执照注册号
     * @param businesslicense
     * @return
     */
    public static int getBusinesslicenseCheckBit(String businesslicense){
        if(businesslicense.length() != 14){
            return 0;
        }

        char[] chars = businesslicense.toCharArray();
        int[] ints = new int[chars.length];
        for(int i=0; i<chars.length;i++){
            ints[i] = Integer.parseInt(String.valueOf(chars[i]));
        }
        return getCheckCode(ints);
    }

    /**
     * 获取 营业执照注册号的校验码
     * @param
     * @return bit
     */
    private static int  getCheckCode(int[] ints){
        if (null != ints && ints.length > 1) {
            int ti = 0;
            int si = 0;// pi|11+ti
            int cj = 0;// （si||10==0？10：si||10）*2
            int pj = 10;// pj=cj|11==0?10:cj|11
            for (int i=0;i<ints.length;i++) {
                ti = ints[i];
                pj = (cj % 11) == 0 ? 10 : (cj % 11);
                si = pj + ti;
                cj = (0 == si % 10 ? 10 : si % 10) * 2;
                if (i == ints.length-1) {
                    pj = (cj % 11) == 0 ? 10 : (cj % 11);
                    return pj == 1 ? 1 : 11 - pj;
                }
            }
        }
        return -1;

    }

    /**
     * @description 验证是否是组织机构代码
     * @author liuc
     * @date 2021/9/22 19:54
     * @since JDK1.8
     * @version V1.0
     */
    public static boolean isOrganizationCode(String mechanismCode) {
        if (mechanismCode.contains("-")) {
            mechanismCode = mechanismCode.replace("-", "");
        }
        if (!Pattern.matches(MECHANISM_CODE_REGEX, mechanismCode)) {
            return false;
        }
        char[] socialCreditCodeArray = mechanismCode.toCharArray();
        char check = socialCreditCodeArray[8];
        int sum = 0;
        for (int i = 0; i < 8; i++) {
            sum += (MECHANISM_CODES.indexOf(socialCreditCodeArray[i]) * FACTOR[i]);
        }
        int mods = 11 - sum % 11;
        String c = "";
        switch (mods) {
            case 10:
                c = "X";
                break;
            case 11:
                c = "0";
                break;
            default:
                c = mods + "";
        }
        return  String.valueOf(check).equals(c);
    }

    /**
     * @description 统一社会信用代码校验
     * @author liuc
     * @date 2021/9/23 14:44
     * @since JDK1.8
     * @version V1.0
     */
    public static boolean checkUnifiedCreditCode(String unifiedCreditCode) {
        unifiedCreditCode = trim(unifiedCreditCode);
        if (StringUtils.isBlank(unifiedCreditCode) || unifiedCreditCode.length() != 18) {
            return false;
        }
        final String upperCaseCode = unifiedCreditCode.toUpperCase();
        // 统一社会信用代码由18位阿拉伯数字或英文大写字母表示（不包括I,O,Z,S,V以防止和阿拉伯字母混淆）
        if (upperCaseCode.contains("I") || upperCaseCode.contains("O") || upperCaseCode.contains("Z") || upperCaseCode.contains("S") || upperCaseCode.contains("V")) {
            return false;
        }
        char[] chars = upperCaseCode.toCharArray();
        int sumCode = 0;
        for (int i = 0; i < 17; i++) {
            String code = String.valueOf(chars[i]);
            int lastCodeIndex = LAST_CODE.indexOf(code);
            Integer factorNum = FACTOR[i];
            sumCode += (lastCodeIndex * factorNum);
        }
        int modCode = 31 - sumCode % 31;
        String lastCode = LAST_CODE.get(modCode % 31);
        return lastCode.equals(String.valueOf(chars[17]));
    }

    /**
     * 去空格
     *
     * @param str 处理字符串
     * @return 结果字符串
     */
    private static String trim(String str) {
        return str.replaceAll("\n", "").replace(" ", "").trim();
    }

    public static String getCheckBit(String code) {
        String yz = "";
        int[] in = { 3, 7, 9, 10, 5, 8, 4, 2 };
        int a = 0;
        for (int i = 0; i < in.length; i++) {
            if (code.substring(i, i + 1).matches("[A-Z]")) {
                a += in[i] * getAsc(code.substring(i, i + 1));
            }else{
                a += in[i] * Integer.parseInt(code.substring(i, i + 1));
            }
        }
        int c9 = 11 - a % 11;
        if (c9 == 10) {
            yz = "X";
        } else if (c9 == 11) {
            yz = "0";
        } else {
            yz = c9 + "";
        }

        return yz;
    }



    public static boolean test1(String data){
        return data.endsWith(String.valueOf(getBusinesslicenseCheckBit(data.substring(0,data.length()-1))));
    }

    public static boolean test2(String data){
        return data.endsWith(String.valueOf(getCheckBit(data.substring(6,data.length()-1))));
    }

    public static boolean test3(String data){
        return data.endsWith(String.valueOf(getUSCCCheckBit(data.substring(0,data.length()-1))));
    }

    public static boolean test4(String data){
        if(data==null) {
            return false;
        }
        if (data.length() != 18) {
            return false;
        }
        if(!data.matches("[a-zA-Z0-9]+")) {
            return false;
        }
        String regex = "^([159Y]{1})([1239]{1})([0-9ABCDEFGHJKLMNPQRTUWXY]{6})([0-9ABCDEFGHJKLMNPQRTUWXY]{9})([0-90-9ABCDEFGHJKLMNPQRTUWXY])$";
        if (!data.matches(regex)) {
            return false;
        }
        return true;
    }

    public static boolean test5(String data){
        String regex = "[1-8][1-6]\\d{4}[a-zA-Z0-9]{9}$";
        if (!data.matches(regex)) {
            return false;
        } else {
            return true;
        }
    }
    public static void main(String[] args) throws Exception
    {
        String code = getOrganizationCode();
        System.out.println(code);
        System.out.println(isOrganizationCode(code));
        code = getBusinessLisenseCode();
        System.out.println(code);
        System.out.println(test1(code));
        code = getTAX_REGISTRATION_CODE();
        System.out.println(code);
        System.out.println(test2(code));
        System.out.println(test5(code));
        code = getSOCIAL_CREDIT_CODE();
        System.out.println(code);
        System.out.println(test3(code));
        System.out.println("test4: "+test4(code));
    }
}
