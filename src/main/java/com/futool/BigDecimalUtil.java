package com.futool;

import com.futool.regex.RegexUtil;
import lombok.extern.slf4j.Slf4j;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.util.Arrays;

/**
 * @description 用于高精确处理常用的数学运算
 *  由于Java的简单类型不能够精确的对浮点数进行运算，这个工具类提供精确的浮点数运算，包括加减乘除和四舍五入及类型转换。
 * @author liuc
 * @date 2021/8/24 10:55
 * @since JDK1.8
 * @version V1.0
 */
@Slf4j
public class BigDecimalUtil {
    /**
     *  默认除法运算精度
     */
    private static final int DEF_DIV_SCALE = 10;

    /**
     * 提供精确的加法运算
     *
     * @param v1 被加数
     * @param v2 加数
     * @return 两个参数的和
     */
    public static BigDecimal add(Object v1, Object v2) {
        BigDecimal b1 = objToBigDecimal(v1);
        BigDecimal b2 = objToBigDecimal(v2);
        return b1.add(b2);
    }

    /**
     * 提供精确的加法运算
     *
     * @param v1    被加数
     * @param v2    加数
     * @param scale 保留scale 位小数
     * @return 两个参数的和
     */
    public static String add(Object v1, Object v2, int scale) {
        if (scale < 0) {
            throw new IllegalArgumentException(
                    "The scale must be a positive integer or zero");
        }
        BigDecimal b1 = objToBigDecimal(v1);
        BigDecimal b2 = objToBigDecimal(v2);
        return b1.add(b2).setScale(scale, RoundingMode.HALF_UP).toString();
    }

    /**
     * 提供精确的减法运算。
     *
     * @param v1 被减数
     * @param v2 减数
     * @return 两个参数的差
     */
    public static BigDecimal sub(Object v1, Object v2) {
        BigDecimal b1 = objToBigDecimal(v1);
        BigDecimal b2 = objToBigDecimal(v2);
        return b1.subtract(b2);
    }

    /**
     * 提供精确的减法运算
     *
     * @param v1    被减数
     * @param v2    减数
     * @param scale 保留scale 位小数
     * @return 两个参数的差
     */
    public static String sub(Object v1, Object v2, int scale) {
        if (scale < 0) {
            throw new IllegalArgumentException(
                    "The scale must be a positive integer or zero");
        }
        BigDecimal b1 = objToBigDecimal(v1);
        BigDecimal b2 = objToBigDecimal(v2);
        return b1.subtract(b2).setScale(scale, RoundingMode.HALF_UP).toString();
    }

    /**
     * 提供精确的乘法运算
     *
     * @param v1 被乘数
     * @param v2 乘数
     * @return 两个参数的积
     */
    public static BigDecimal mul(Object v1, Object v2) {
        BigDecimal b1 = objToBigDecimal(v1);
        BigDecimal b2 = objToBigDecimal(v2);
        return b1.multiply(b2);
    }

    /**
     * 提供精确的乘法运算
     *
     * @param v1    被乘数
     * @param v2    乘数
     * @param scale 保留scale 位小数
     * @return 两个参数的积
     */
    public static String mul(Object v1, Object v2, int scale) {
        if (scale < 0) {
            throw new IllegalArgumentException(
                    "The scale must be a positive integer or zero");
        }
        BigDecimal b1 = objToBigDecimal(v1);
        BigDecimal b2 = objToBigDecimal(v2);
        return b1.multiply(b2).setScale(scale, RoundingMode.HALF_UP).toString();
    }

    /**
     * 提供精确的除法运算
     *
     * @param v1 被除数
     * @param v2 除数
     * @return 两个参数的商
     */
    public static BigDecimal div(Object v1, Object v2) {
        BigDecimal b1 = objToBigDecimal(v1);
        BigDecimal b2 = objToBigDecimal(v2);
        if (BigDecimal.ZERO.compareTo(b2)== 0) {
            throw new IllegalArgumentException(
                    "Divider cannot be equal to zero");
        }
        BigDecimal returnBd = BigDecimal.ZERO;
        try{
            returnBd = b1.divide(b2);
        }catch (ArithmeticException e) {
            //当发生除不尽的情况时，由scale参数指定精度，以后的数字四舍五入
            returnBd = objToBigDecimal(div(b1, b2,2));
        }
        return returnBd;
    }

    /**
     * 提供（相对）精确的除法运算。当发生除不尽的情况时，由scale参数指
     * 定精度，以后的数字四舍五入
     *
     * @param v1    被除数
     * @param v2    除数
     * @param scale 表示需要精确到小数点以后几位
     * @return 两个参数的商
     */
    public static String div(Object v1, Object v2, int scale) {
        if (scale < 0) {
            throw new IllegalArgumentException("The scale must be a positive integer or zero");
        }
        BigDecimal b1 = objToBigDecimal(v1);
        BigDecimal b2 = objToBigDecimal(v2);
        if (BigDecimal.ZERO.compareTo(b2)== 0) {
            throw new IllegalArgumentException(
                    "Divider cannot be equal to zero");
        }
        return b1.divide(b2, scale, RoundingMode.HALF_UP).toString();
    }

    /**
     * 提供精确的小数位四舍五入处理
     *
     * @param v     需要四舍五入的数字
     * @param scale 小数点后保留几位
     * @return 四舍五入后的结果
     */
    public static String round(Object v, int scale) {
        if (scale < 0) {
            throw new IllegalArgumentException(
                    "The scale must be a positive integer or zero");
        }
        BigDecimal b = objToBigDecimal(v);
        return b.setScale(scale, RoundingMode.HALF_UP).toString();
    }

    /**
     * 取余数
     *
     * @param v1    被除数
     * @param v2    除数
     * @param scale 小数点后保留几位
     * @return 余数
     */
    public static String remainderStr(Object v1, Object v2, int scale) {
        if (scale < 0) {
            throw new IllegalArgumentException(
                    "The scale must be a positive integer or zero");
        }
        BigDecimal b1 = objToBigDecimal(v1);
        BigDecimal b2 = objToBigDecimal(v2);
        if (BigDecimal.ZERO.compareTo(b2)== 0) {
            throw new IllegalArgumentException(
                    "Divider cannot be equal to zero");
        }
        return b1.remainder(b2).setScale(scale, RoundingMode.HALF_UP).toString();
    }

    /**
     * 取余数  BigDecimal
     *
     * @param v1    被除数
     * @param v2    除数
     * @param scale 小数点后保留几位
     * @return 余数
     */
    public static BigDecimal remainderBigDecimal(Object v1, Object v2, int scale) {
        if (scale < 0) {
            throw new IllegalArgumentException(
                    "The scale must be a positive integer or zero");
        }
        BigDecimal b1 = objToBigDecimal(v1);
        BigDecimal b2 = objToBigDecimal(v2);
        if (BigDecimal.ZERO.compareTo(b2)== 0) {
            throw new IllegalArgumentException(
                    "Divider cannot be equal to zero");
        }
        return b1.remainder(b2).setScale(scale, RoundingMode.HALF_UP);
    }

    /**
     * 比较大小
     *
     * @param v1 被比较数
     * @param v2 比较数
     * @return 如果v1 大于v2 则 返回true 否则false
     */
    public static boolean compare(Object v1, Object v2) {
        BigDecimal b1 = objToBigDecimal(v1);
        BigDecimal b2 = objToBigDecimal(v2);
        int bj = b1.compareTo(b2);
        boolean res;
        if (bj > 0) {
            res = true;
        } else {
            res = false;
        }
        return res;
    }

    /**
     * Object转BigDecimal类型
     *
     * @param value
     * @return
     */
    public static BigDecimal objToBigDecimal(Object value) {
        BigDecimal ret = BigDecimal.ZERO;
        if (UtilValidate.isNotEmpty(value)) {
            if (value instanceof BigDecimal) {
                ret = (BigDecimal) value;
            } else if (value instanceof String) {
                value = ((String) value).trim();
                if (RegexUtil.isNumeric((String) value)) {
                    ret = new BigDecimal((String) value);
                }else {
                    if (UtilValidate.isEmpty(value)) {
                        ret = BigDecimal.ZERO;
                    } else {
                        ret = null;
                    }
                }
            } else if (value instanceof BigInteger) {
                ret = new BigDecimal((BigInteger) value);
            } else if (value instanceof Number) {
                ret = new BigDecimal(((Number) value).doubleValue());
            } else {
                throw new ClassCastException("Not possible to coerce [" + value + "] from class " + value.getClass()
                        + " into a BigDecimal.");
            }
        }
        return ret;
    }

    /**
     * Object转为BigDecimal类型并四舍五入保留N位小数
     *
     * @param n
     * @param obj
     * @return
     */
    public static BigDecimal objToBigDecimalRetainNbit(int n, Object obj) {
        BigDecimal bd = BigDecimal.ZERO;
        if (UtilValidate.isNotEmpty(obj)) {
            bd = objToBigDecimal(obj).setScale(n, RoundingMode.HALF_UP);
        }
        return bd;
    }

    /**
     * BigDecimal转换为String类型并避免输出科学计数法
     *
     * @param bd 要转换的参数
     * @return String 转换后输出的参数
     */
    public static String bigDecimalToStr(BigDecimal bd) {
        String str = null;
        if (UtilValidate.isEmpty(bd)) {
            return null;
        }
        if (UtilValidate.isNotEmpty(bd)) {
            str = bd.toPlainString().toString();
        }
        return str;
    }

    /**
     * BigDecimal转换为Integer
     *
     * @param bd 要转换的参数
     * @return Integer 转换后输出的参数
     */
    public static Integer bigDecimalToInteger(BigDecimal bd) {
        Integer inte = 0;
        if (UtilValidate.isNotEmpty(bd)) {
            inte = bd.intValue();
        }
        return inte;
    }

    /**
     * 判断BigDecimal类型值是否大于0，是返回true、否返回false
     *
     * @param bd
     * @return boolean
     */
    public static boolean isGreaterThanZero(BigDecimal bd) {
        boolean flag = false;
        if(UtilValidate.isNotEmpty(bd)) {
            if(bd.signum() > 0) {
                flag = true;
            }
        }
        return flag;
    }

    /**
     * 判断BigDecimal类型值是否小于0，是返回true、否返回false
     *
     * @param bd
     * @return boolean
     */
    public static boolean isLessThanZero(BigDecimal bd) {
        boolean flag = false;
        if(UtilValidate.isNotEmpty(bd)) {
            if(bd.signum() < 0) {
                flag = true;
            }
        }
        return flag;
    }

    /**
     * 判断BigDecimal类型值是否小于0，是返回true、否返回false
     * @param bd
     * @return boolean
     */
    public static boolean isEqualZero(BigDecimal bd) {
        boolean flag = false;
        if(UtilValidate.isNotEmpty(bd)) {
            if(bd.signum() == 0) {
                flag = true;
            }
        }
        return flag;
    }

    /**
     * 小数点后N位截取
     *
     * @param bigDecimal
     * @param scale
     * @return
     */
    public static String getBigDecimalPoint(String bigDecimal, int scale) {
        if (scale < 0 || !bigDecimal.contains(".")) {
            return bigDecimal;
        }
        if (scale == 0) {
            return bigDecimal.split("\\.")[0];
        }
        BigDecimal b = new BigDecimal(bigDecimal);
        BigDecimal one = BigDecimal.ONE;
        return b.divide(one, scale, RoundingMode.HALF_UP).doubleValue() + "";
    }

    /**
     * 将Bigdecial以财务形式显示,千分位表示，保留N位小数
     * @param number 需要转换的数据
     * @param i 保留N位，如保留一位小数，输入1，结果是1,000,000.0这种格式
     * @return java.lang.String
     * @author liuc
     * @date 2021/11/19 16:00
     * @throws
     */
    public static String bigDecimalFormatToFinance(BigDecimal number,Integer i){
        StringBuilder sb = new StringBuilder("");
        sb.append("#,###");
        if (i > 0) {
            sb.append(".");
            while(--i >= 0){
                sb.append("0");
            }
        }
        NumberFormat nf = new DecimalFormat(sb.toString());
        return  nf.format(number).equals("0")?"":nf.format(number);
    }

    /**
     * 逗号‘,’千位符的String转Bigdecimal
     * 如1,000,000 -> 1000000
     * 1,000,000.0 -> 1000000.0
     * 1,000,000.00 -> 1000000.00
     * @param str
     * @return java.math.BigDecimal
     * @author liuc
     * @date 2021/11/19 16:07
     * @throws
     */
    public static BigDecimal financeFormatToBigDecimal(String str){
        DecimalFormat format = new DecimalFormat();
        format.setParseBigDecimal(true);
        ParsePosition position = new ParsePosition(0);
        BigDecimal parse = (BigDecimal) format.parse(str, position);
        return  parse;
    }

    /**
     * Object 小数转为百分比字符串并保留两位小数
     * @param obj
     * @return java.lang.String
     * @author liuc
     * @date 2021/12/8 11:03
     * @throws
     */
    public static String objToPercent(Object obj){
        DecimalFormat df = new DecimalFormat("0.00%");
        BigDecimal d= objToBigDecimal(obj);
        return df.format(d);
    }

    /**
     * 百分比字符串数字转BigDecimal
     * 例如：10.01% -> 0.01001
     *      100% -> 1
     * @param str 需要转BigDecimal的百分比字符串
     * @return java.math.BigDecimal BigDecimal结果
     * @author liuc
     * @date 2021/12/8 13:56
     * @throws
     */
    public static BigDecimal percentToBigDecimal(String str){
        BigDecimal bd = null;
        if (UtilValidate.isNotEmpty(str)) {
            str = str.replace("%","");
            if (RegexUtil.isNumeric(str)) {
                bd = objToBigDecimal(str).divide(new BigDecimal(100));
            }
        }
        return bd;
    }

    /**
     * 百分比计算
     * @param obj 金额
     * @param percent 百分比数
     * @return BigDecimal 默认四舍五入
     */
    public static BigDecimal multiplyPercent(Object obj, BigDecimal percent) {
        if (objToBigDecimal(obj).compareTo(BigDecimal.ZERO) == 0) {
            return BigDecimal.ZERO;
        }
        if (defaultZero(percent).compareTo(BigDecimal.ZERO) == 0) {
            return BigDecimal.ZERO;
        }
        return objToBigDecimal(obj).multiply(percent)
                .divide(new BigDecimal("100"), 2, BigDecimal.ROUND_HALF_UP);
    }
    /**
     * 如果num为Null，默认为0
     * @param num 数值
     * @return BigDecimal
     */
    public static BigDecimal defaultZero(BigDecimal num) {
        if (num == null) {
            return BigDecimal.ZERO;
        }
        return num;
    }

    /**
     * 求最小数值
     * @param num
     * @return BigDecimal
     */
    public static BigDecimal min(BigDecimal... num) {
        if (num == null) {
            return null;
        }
        return Arrays.stream(num).min(BigDecimal::compareTo).get();
    }

    /**
     * 求最大数值
     * @param num
     * @return BigDecimal
     */
    public static BigDecimal max(BigDecimal... num) {
        if (num == null) {
            return null;
        }
        return Arrays.stream(num).max(BigDecimal::compareTo).get();
    }

    /**
     * 转成str
     * @param obj
     * @return
     */
    public static String objToStr(Object obj) {
    	return bigDecimalToStr(objToBigDecimal(obj));
    }
}
