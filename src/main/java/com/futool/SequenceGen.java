package com.futool;

import lombok.extern.log4j.Log4j2;

/**
 * 生成带有ip的全局流水
 * @author liuc
 * @date 2021/11/12 11:15
 * @since JDK1.8
 * @version V1.0
 */
@Log4j2
public class SequenceGen {

    private final long workerId;
    
    private static final long twepoch = 1288834974657L;
    
    private static long sequence = 0L;
    
    private static final long workerIdBits = 4L;
    
    private static final long maxWorkerId = -1L ^ -1L << workerIdBits;
    
    private static final long sequenceBits = 10L;
    
    private static final long workerIdShift = sequenceBits;
    
    private static final long timestampLeftShift = sequenceBits + workerIdBits;
    
    private static final long sequenceMask = -1L ^ -1L << sequenceBits;
    
    private static long lastTimestamp = -1L;

    /**
     * 申明SequenceGen类型的变量
     */
    private static volatile SequenceGen instance;

    /**
     * 对外提供公共的访问方式
     * @author liuc
     * @date 2021/11/12 9:56
     * @since JDK1.8
     * @version V1.0
     */
    public static SequenceGen getInstance(){
        //第一次判断，如果instance的值不为null，不需要抢占锁，直接返回对象
        if (instance == null) {
            synchronized (SequenceGen.class){
                //第二次判断
                if (instance == null) {
                    instance = new SequenceGen(2L);
                }
            }
        }
        return instance;
    }

    /**
     * 有参构造器
     * @author liuc
     * @date 2021/11/12 9:56
     * @since JDK1.8
     * @version V1.0
     */
    public SequenceGen(final long workerId) {
        super();
        if (workerId > SequenceGen.maxWorkerId || workerId < 0) {
            throw new IllegalArgumentException(
                String.format("worker Id can't be greater than %d or less than 0", SequenceGen.maxWorkerId));
        }
        this.workerId = workerId;
    }

    /**
     * 获取下一个seq
     * @author liuc
     * @date 2021/11/12 9:58
     * @since JDK1.8
     * @version V1.0
     */
    public synchronized String nextId() {
        long timestamp = this.timeGen();
        if (SequenceGen.lastTimestamp == timestamp) {
            SequenceGen.sequence = (SequenceGen.sequence + 1) & SequenceGen.sequenceMask;
            if (SequenceGen.sequence == 0) {
                timestamp = this.tilNextMillis(SequenceGen.lastTimestamp);
            }
        } else {
            SequenceGen.sequence = 0;
        }
        if (timestamp < SequenceGen.lastTimestamp) {
            try {
                throw new Exception(String.format("Clock moved backwards. Refusing to generate id for %d milliseconds",
                    SequenceGen.lastTimestamp - timestamp));
            } catch (Exception e) {
                log.error(e);
            }
        }
        
        SequenceGen.lastTimestamp = timestamp;
        String ip = IpUtil.getIp().replace(".","");
        @SuppressWarnings("static-access")
        long nextId =
            ((timestamp - twepoch << timestampLeftShift)) | (this.workerId << this.workerIdShift) | (this.sequence);
        String nextSeq = ip +String.valueOf(nextId);
        return nextSeq;
    }
    
    private long tilNextMillis(final long lastTimestamp) {
        long timestamp = this.timeGen();
        while (timestamp <= lastTimestamp) {
            timestamp = this.timeGen();
        }
        return timestamp;
    }
    
    private long timeGen() {
        return System.currentTimeMillis();
    }

    /**
     * 生成唯一全局流水号
     *
     * @return
     */
    public static String nextSeq() {
        return SequenceGen.getInstance().nextId();
    }

    public static void main(String[] args) {
        //测试
        System.out.println(SequenceGen.nextSeq());
    }
}
