package com.futool.concurrent;

import cn.hutool.core.date.StopWatch;
import com.futool.JsonUtil;
import com.futool.concurrent.factory.BusinessProcessFactory;
import com.futool.concurrent.factory.IBusinessProcessEntrance;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * 并发处理工具类
 */
public class ConcurrentUtil {
    /**
     * 并发处理批量数据
     * @param data 表示传入的数据
     * @param batchNum 表示每一次处理的数量，例如500条等。
     * @param serviceName 实现类名
     * @param methodName 真正处理业务的方法名
     * @throws InterruptedException
     */
    public static void batchDeal(List data, int batchNum,String serviceName,String methodName) throws InterruptedException {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        int totalNum = data.size();
        int pageNum = totalNum % batchNum == 0 ? totalNum / batchNum : totalNum / batchNum + 1;
        ThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(pageNum);
        try {
            CountDownLatch countDownLatch = new CountDownLatch(pageNum);
            List subData = null;
            int fromIndex, toIndex;
            for (int i = 0; i < pageNum; i++) {
                fromIndex = i * batchNum;
                toIndex = Math.min(totalNum, fromIndex + batchNum);
                subData = data.subList(fromIndex, toIndex);
                BatchTask task = new BatchTask(subData, countDownLatch,serviceName,methodName);
                executor.execute(task);
            }
            // 主线程必须在启动其它线程后立即调用CountDownLatch.await()方法，
            // 这样主线程的操作就会在这个方法上阻塞，直到其它线程完成各自的任务。
            // 计数器的值等于0时，主线程就能通过await()方法恢复执行自己的任务。
            countDownLatch.await();
            System.out.println("数据操作完成!可以在此开始其它业务");
        } finally {
            // 关闭线程池，释放资源
            executor.shutdown();
        }
        stopWatch.stop();
        System.out.println(serviceName+"服务批量处理耗时："+stopWatch);
    }

    private static class BatchTask implements Runnable {
        private List list;
        private CountDownLatch countDownLatch;
        private String serviceName;
        private String methodName;

        public BatchTask(List data, CountDownLatch countDownLatch,String serviceName,String methodName) {
            this.list = data;
            this.countDownLatch = countDownLatch;
            this.serviceName = serviceName;
            this.methodName = methodName;
        }

        @Override
        public void run() {
            if (null != list) {
                // 业务逻辑，例如批量insert或者update
//                System.out.println("现在操作的数据是:"+ JsonUtil.toJson(list));
                System.out.println("开始调用服务:"+ serviceName);
                //由静态工厂获取打印报告功能接口实现类
                IBusinessProcessEntrance businessProcessEntrance = null;
                try {
                    Map<String,Object> context = new HashMap<>(8);
                    context.put("methodName",methodName);
                    context.put("bizList",list);
                    businessProcessEntrance = BusinessProcessFactory.getServiceImpl(serviceName);
                    businessProcessEntrance.execute(context);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            // 发出线程任务完成的信号
            countDownLatch.countDown();
        }
    }
}
