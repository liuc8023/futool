package com.futool.concurrent.constants;

/**
 * 常量类
 * @author liuc
 */
public class BusinessProcessConstants {
	/**
	 * 其他类型报告
	 */
	public static final String BATCH_UPDATE_CATALOG="batchUpdateCatalog";
	/**
	 * 其他类型报告
	 */
	public static final String BATCH_CREATE_CATALOG="batchCreateCatalog";
}
