package com.futool.concurrent.factory;

import cn.hutool.json.JSONUtil;
import com.futool.concurrent.annotation.AnnotationUtil;
import com.futool.concurrent.annotation.GenBatch;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author liuc
 * 工厂类
 */
@SuppressWarnings("unchecked")
public class BusinessProcessFactory {
	private static final Logger logger = LoggerFactory.getLogger(BusinessProcessFactory.class);
	public static final String MODULE = BusinessProcessFactory.class.getName();

	/**
	 * 编程规范推荐使用static final修饰ThreadLocal对象。
	 * 	  ThreadLocal实例作为ThreadLocalMap的Key，针对一个线程内的所有操作是共享的，所以建议设置static修饰符，以便被所有的对象共享。
	 * 由于静态变量会在类第一次被使用时装载，只会分配一次存储空间，此类的所有实例都会共享这个存储空间，所以使用static修饰ThreadLocal就
	 * 会节约内存空间。另外，为了确保ThreadLocal实例的唯一性，除了使用static修饰之外，还会使用final进行加强修饰，以防止其在使用过程中发生动态变更。
	 * 避免并发窜流程
	 */
	private static final ThreadLocal<IBusinessProcessEntrance> THREAD_LOCAL = new ThreadLocal<>();

	/**
	 * 如果需要满足线程安全，可以用 Collections 的 synchronizedMap 方法使HashMap 具有线程安全的能力，或者使用 ConcurrentHashMap。
	 * JDK1.8的ConcurrentHashMap取消了segments字段，采用了transient volatile HashEntry<K,V>[] table保存数据。这样对每个table
	 * 数组元素加锁，见源代码中synchronized(f)，因此可以减少并发冲突的概率，提高并发性能。所以ConcurrentHashMap的put并发性更好，因此相
	 * 同工作下ConcurrentHashMap花费时间更少。
	 * ConcurrentHashMap的get方法采用了与HashMap一样的思路 ，并没有加锁，所以性能上优于SynchrinizedMap的get方法。
	 *
	 * //不建议使用
	 * private static Map<String,Class<?>> serviceMap = Collections.synchronizedMap(new HashMap<String,Class<?>>());
	 */
	private static Map<String,Class<?>> serviceMap = new ConcurrentHashMap<>();

	/**
	 * 打印功能实现类编码与名称K,V映射
	 */
	public static Map<String,String> serviceNameMap = Collections.unmodifiableMap(new HashMap<String,String>());

	//类首次加载时初始化,仅执行1次
	static {
		try {
			//获取指定包下带有指定注解的类
			Reflections reflections = new Reflections("com.futool");
			List<Object> list = new ArrayList<>();
			list.add(GenBatch.class);
			Set<Class<?>> classesList = new HashSet<>();
			for (Object obj: list) {
				classesList.addAll(reflections.getTypesAnnotatedWith((Class<? extends Annotation>) obj));
			}
			//输出所有使用了特定注解的类的注解值
			try {
				logger.info(JSONUtil.toJsonStr(AnnotationUtil.mapAnnotationAndClass(classesList,list)));
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
			Map<String,Object> retMap = AnnotationUtil.mapAnnotationAndClass(classesList,list);
			serviceMap = (Map<String, Class<?>>)retMap.get("serviceMap");
			serviceNameMap = (Map<String, String>)retMap.get("serviceNameMap");
			logger.info("=========serviceMap=====:{}", JSONUtil.toJsonStr(serviceMap));
			logger.info("=========serviceNameMap=====:{}", JSONUtil.toJsonStr(serviceNameMap));
		} catch (RuntimeException e) {
			logger.error(e.getMessage(), MODULE);
		}
	}

	/**
	 * <IBusinessProcessEntrance>
	 * <功能详细描述>
	 * @param value 注解值
	 * @return IBusinessProcessEntrance
	 * @see [类、类#方法、类#成员]
	 */
	public static IBusinessProcessEntrance getServiceImpl(String value){
		if (logger.isInfoEnabled()) {
			logger.info("=========getServiceImpl=====start========输入的注解为[{}]", JSONUtil.toJsonStr(value));
		}
		if (value == null || "".equals(value)) {
			throw new RuntimeException("请输入注解值");
		}
		Class<?> clzz = serviceMap.get(value);
		if(clzz == null) {
			throw new RuntimeException("通过注解["+value+"]未获取到实现类!");
		}
		try {
			/**
			 * 流程处理统一接口
			 */
			IBusinessProcessEntrance iBusinessProcessEntrance = getImplByClassName(clzz.getName());
			//向本地线程副本中设置实现类
			THREAD_LOCAL.set(iBusinessProcessEntrance);
			//返回打印报告接口实现类
			logger.info("=========getServiceImpl=====end========通过注解[{}]获取到的实现类为[{}]",JSONUtil.toJsonStr(value),JSONUtil.toJsonStr(clzz.getName()));
			return THREAD_LOCAL.get();
		} finally {
			/**
			 * 凡事都有两面性，使用static、final修饰ThreadLocal实例也会带来副作用，使得Thread实例内部的ThreadLocalMap中Entry的Key在Thread实例的生命期内将始终保持为非null，
			 * 从而导致Key所在的Entry不会被自动清空，这就会让Entry中的Value指向的对象（IBusinessProcessEntrance）一直存在强引用，于是Value指向的对象（person）在线程生命期内不会
			 * 被释放，最终导致内存泄漏。所以，在使用完static、final修饰的ThreadLocal实例之后，必须调用remove()来进行显式的释放操作。
			 */
			logger.info("释放ThreadLocal对象，以防内存泄漏!");
			THREAD_LOCAL.remove();
		}
	}

	/**
	 * 通过类名反射获取对象
	 * @param className 类名
	 * @return IPrintReportEntrance
	 */
	private static IBusinessProcessEntrance getImplByClassName(String className){
		Class<IBusinessProcessEntrance> cls = null;
		try {
			//根据类名初始化类
			cls = (Class<IBusinessProcessEntrance>) Class.forName(className);
		} catch (ClassNotFoundException e) {
			logger.error(e.getMessage(), MODULE);
			throw new RuntimeException("通过类名反射获取对象失败!");
		}
		/**
		 * 返回实例化的对象
		 */
		IBusinessProcessEntrance iBusinessProcessEntrance = null;
		try {
			iBusinessProcessEntrance = cls.getDeclaredConstructor().newInstance();
		} catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
			logger.error(e.getMessage(), MODULE);
			throw new RuntimeException("通过类名反射获取对象失败!");
		}
		return iBusinessProcessEntrance;
	}
}
