package com.futool.concurrent.factory;

import java.util.Map;

public interface IBusinessProcessEntrance {
	/**
	 * <业务逻辑处理抽象方法>
	 * <由各实现类根据具体业务逻辑做对应实现>
	 * @param context 入参
	 * @return Map<String, Object>
	 * @throws Exception 异常
	 */
	Map<String, Object> execute(Map<String, Object> context) throws Exception;
}
