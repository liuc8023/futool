package com.futool.concurrent.annotation;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import cn.hutool.json.JSONUtil;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 注解工具类
 * @author liuc
 */
@SuppressWarnings("unchecked")
public class AnnotationUtil {
	private static final Logger logger = LoggerFactory.getLogger(AnnotationUtil.class);
	
	/**
	 * 本地测试
	 */
	public static void main(String[] args) {
		//获取指定包下带有指定注解的java类
		Reflections reflections = new Reflections("com.futool");
		//指定自定义注解
		List<Object> list = new ArrayList<>();
		list.add(GenBatch.class);
		//含有指定自定义注解的类
		Set<Class<?>> classesList = new HashSet<>();
		for (Object obj: list) {
			classesList.addAll(reflections.getTypesAnnotatedWith((Class<? extends Annotation>) obj));
		}
		//输出所有使用了特定注解的类的注解值
		try {
			logger.info(JSONUtil.toJsonStr(AnnotationUtil.mapAnnotationAndClass(classesList,list)));
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	/**
	 * 输出所有使用特定注解的类的注解值
	 */
	public static Map<String,Object> mapAnnotationAndClass(Set<Class<?>> clsList,List<Object> list) {
		Map<String,Object> retMap = new HashMap<>(8);
		try {
			//注解:类名
			Map<String,Class<?>> serviceMap = new HashMap<>(8);
			//注解:节点名称
			Map<String,String> serviceNameMap = new HashMap<>(8);

			if (clsList != null && !clsList.isEmpty()) {
				for (Class<?> cls : clsList) {
					if (list != null && !list.isEmpty()) {
						for (Object bean : list) {
							Annotation anno = cls.getAnnotation((Class<? extends Annotation>) bean);
							// @PrintReport注解
							if (anno instanceof GenBatch) {
								GenBatch genBatchAnnotation = (GenBatch) anno;
								if (serviceMap.get(genBatchAnnotation.value()) != null) {
									logger.error("注解值[{}]重复!", genBatchAnnotation.value());
									throw new RuntimeException("注解值[" + genBatchAnnotation.value() + "]重复!");
								}
								//类名
								serviceMap.put(genBatchAnnotation.value(), cls);
								//节点名称
								serviceNameMap.put(genBatchAnnotation.value(), genBatchAnnotation.name());
							}
						}
					}
				}
			}
			retMap.put("serviceMap", serviceMap);
			retMap.put("serviceNameMap", serviceNameMap);
		} catch (RuntimeException e){
			logger.error("获取含有自定义注解的类失败,原因为:{}",e.getMessage());
			throw new RuntimeException("获取含有自定义注解的类失败,原因为:"+e.getMessage());
		}
		logger.info(JSONUtil.toJsonStr(retMap));
		return retMap;
	}
	
	/**
	 * 通过接口名取得某个接口下所有实现这个接口的类
	 */
	public static List<Class<?>> getAllClassByInterface(Class<?> c) {
		List<Class<?>> returnClassList = null;
		if (c.isInterface()) {
			// 获取当前的包名
			String packageName = c.getPackage().getName();
			// 获取当前包下以及子包下所以的类
			List<Class<?>> allClass = getClasses(packageName);
			returnClassList = new ArrayList<>();
			for (Class<?> cls : allClass) {
				// 判断是否是同一个接口
				if (c.isAssignableFrom(cls)) {
					// 本身不加入进去
					if (!c.equals(cls)) {
						returnClassList.add(cls);
					}
				}
			}
		}
		return returnClassList;
	}

	
	/**
	 * 从包package中获取所有的Class
	 * @param packageName 包名
	 * @return List<Class<?>>
	 */
	private static List<Class<?>> getClasses(String packageName) {
		// 第一个class类的集合
		List<Class<?>> classes = new ArrayList<>();
		// 是否循环迭代
		boolean recursive = true;
		// 获取包的名字 并进行替换
		String packageDirName = packageName.replace('.', '/');
		// 定义一个枚举的集合 并进行循环来处理这个目录下的things
		Enumeration<URL> dirs;
		try {
			dirs = Thread.currentThread().getContextClassLoader().getResources(packageDirName);
			// 循环迭代下去
			while (dirs.hasMoreElements()) {
				// 获取下一个元素
				URL url = dirs.nextElement();
				// 得到协议的名称
				String protocol = url.getProtocol();
				// 如果是以文件的形式保存在服务器上
				if ("file".equals(protocol)) {
					// 获取包的物理路径
					String filePath = URLDecoder.decode(url.getFile(), "UTF-8");
					// 以文件的方式扫描整个包下的文件 并添加到集合中
					findAndAddClassesInPackageByFile(packageName, filePath, recursive, classes);
				} else if ("jar".equals(protocol)) {
					// 如果是jar包文件
					// 定义一个JarFile
					JarFile jar;
					try {
						// 获取jar
						jar = ((JarURLConnection) url.openConnection()).getJarFile();
						// 从此jar包 得到一个枚举类
						Enumeration<JarEntry> entries = jar.entries();
						// 同样的进行循环迭代
						while (entries.hasMoreElements()) {
							// 获取jar里的一个实体 可以是目录 和一些jar包里的其他文件 如META-INF等文件
							JarEntry entry = entries.nextElement();
							String name = entry.getName();
							// 如果是以/开头的
							if (name.charAt(0) == '/') {
								// 获取后面的字符串
								name = name.substring(1);
							}
							// 如果前半部分和定义的包名相同
							if (name.startsWith(packageDirName)) {
								int idx = name.lastIndexOf('/');
								// 如果以"/"结尾 是一个包
								if (idx != -1) {
									// 获取包名 把"/"替换成"."
									packageName = name.substring(0, idx).replace('/', '.');
								}
								// 如果可以迭代下去 并且是一个包
								// 如果是一个.class文件 而且不是目录
								if (name.endsWith(".class") && !entry.isDirectory()) {
									// 去掉后面的".class" 获取真正的类名
									String className = name.substring(packageName.length() + 1, name.length() - 6);
									try {
										// 添加到classes
										classes.add(Class.forName(packageName + '.' + className));
									} catch (ClassNotFoundException e) {
										e.printStackTrace();
									}
								}
							}
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
 
		return classes;
	}
	
	
	/**
	 * 以文件的形式来获取包下的所有Class
	 * 
	 * @param packageName 包名
	 * @param packagePath 包路径
	 * @param recursive 是否递归
	 * @param classes class集合
	 */
	private static void findAndAddClassesInPackageByFile(String packageName, String packagePath, final boolean recursive, List<Class<?>> classes) {
		// 获取此包的目录 建立一个File
		File dir = new File(packagePath);
		// 如果不存在或者 也不是目录就直接返回
		if (!dir.exists() || !dir.isDirectory()) {
			return;
		}
		// 如果存在 就获取包下的所有文件 包括目录
		// 自定义过滤规则 如果可以循环(包含子目录) 或则是以.class结尾的文件(编译好的java类文件)
		File[] dirfiles = dir.listFiles(file -> (recursive && file.isDirectory()) || (file.getName().endsWith(".class")));
		// 循环所有文件
		if (dirfiles != null) {
			for (File file : dirfiles) {
				// 如果是目录 则继续扫描
				if (file.isDirectory()) {
					findAndAddClassesInPackageByFile(packageName + "." + file.getName(), file.getAbsolutePath(), recursive, classes);
				} else {
					// 如果是java类文件 去掉后面的.class 只留下类名
					String className = file.getName().substring(0, file.getName().length() - 6);
					try {
						// 添加到集合中去
						classes.add(Class.forName(packageName + '.' + className));
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

}
