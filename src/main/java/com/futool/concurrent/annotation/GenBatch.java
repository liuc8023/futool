package com.futool.concurrent.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface GenBatch {
    /**
     * 服务处理方法
     */
    String value() ;

    /**
     * 服务解释，可为空
     */
    String name() default "";
}
