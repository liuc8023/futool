package com.futool.concurrent.impl.catalog;

import cn.hutool.json.JSONUtil;
import com.futool.concurrent.annotation.GenBatch;
import com.futool.concurrent.constants.BusinessProcessConstants;
import com.futool.concurrent.factory.IBusinessProcessEntrance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.lang.reflect.Method;
import java.util.Map;

@GenBatch(value = BusinessProcessConstants.BATCH_UPDATE_CATALOG,name="目录批量服务")
public class CatalogBatchServices implements IBusinessProcessEntrance {
    private static final Logger logger = LoggerFactory.getLogger(CatalogBatchServices.class);
    @Override
    public Map<String, Object> execute(Map<String, Object> context) throws Exception {
        String methodName = (String) context.get("methodName");
        Method method =this.getClass().getDeclaredMethod(methodName,Map.class);
        method.invoke(this,context);
        return null;
    }


    public Map<String, Object> batchUpdateCatalog(Map<String, Object> context) throws Exception {
//        logger.info("批量修改目录,{}", JSONUtil.toJsonStr(context.get("bizList")));
        return null;
    }
}
