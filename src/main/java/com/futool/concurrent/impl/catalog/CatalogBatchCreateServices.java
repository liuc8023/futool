package com.futool.concurrent.impl.catalog;

import cn.hutool.json.JSONUtil;
import com.futool.concurrent.annotation.GenBatch;
import com.futool.concurrent.constants.BusinessProcessConstants;
import com.futool.concurrent.factory.IBusinessProcessEntrance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

@GenBatch(value = BusinessProcessConstants.BATCH_CREATE_CATALOG,name="批量新增目录")
public class CatalogBatchCreateServices implements IBusinessProcessEntrance {
    private static final Logger logger = LoggerFactory.getLogger(CatalogBatchCreateServices.class);
    @Override
    public Map<String, Object> execute(Map<String, Object> context) throws Exception {
//        logger.info("批量新增目录,{}", JSONUtil.toJsonStr(context.get("bizList")));
        return null;
    }
}