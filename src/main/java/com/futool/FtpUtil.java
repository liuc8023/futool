package com.futool;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.net.ftp.*;

import java.io.*;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;

/**
 * ftp读取文件
 * https://blog.csdn.net/wangyingjief/article/details/106774587
 * https://blog.csdn.net/beijiaoguzai/article/details/124866155
 * @author liuc
 */
@Slf4j
public class FtpUtil {
    private static final String DEFAULT_CHARSET = "UTF-8";
    private static final String CHARSET_GBK = "GBK";
    private static final int DEFAULT_TIMEOUT = 60 * 1000;
    private static final String DAILY_FILE_PATH = "get";
    private static volatile String ftpBasePath;

    private static FTPClient client = null;
    private static String host = "";
    private static int port = 21;
    private static String username = "";
    private static String password = "";
    private static String ftpPath = "";
    /**
     * 连接FTP Server
     *
     * @throws IOException
     */
    public static final String ANONYMOUS_USER = "anonymous";

    @SuppressWarnings("unused")
    private FtpUtil(String host, String username, String password) {
    }

    private FtpUtil(String host,int port, String username, String password) {
        this(host, port, username, password, DEFAULT_CHARSET);
        setTimeout(DEFAULT_TIMEOUT, DEFAULT_TIMEOUT, DEFAULT_TIMEOUT);
    }

    private FtpUtil(String host, int port, String username, String password, String charset) {
        client = new FTPClient();
        client.setControlEncoding(charset);
        FtpUtil.host = StringUtils.isEmpty(host) ? "localhost" : host;
        FtpUtil.port = (port <= 0) ? 21 : port;
        FtpUtil.username = StringUtils.isEmpty(username) ? "anonymous" : username;
        FtpUtil.password = password;
    }

    /**
     * 获取当前FTP所在目录位置
     *
     * @return
     */
    public String getCurrentPath() {
        return ftpPath;
    }

    /**
     * 设置超时时间
     * @param defaultTimeout 超时时间
     * @param connectTimeout 超时时间
     * @param dataTimeout 超时时间
     */
    public void setTimeout(int defaultTimeout, int connectTimeout, int dataTimeout) {
        client.setDefaultTimeout(defaultTimeout);
        client.setConnectTimeout(connectTimeout);
        client.setDataTimeout(dataTimeout);
    }

    /**
     * 创建默认的ftp客户端
     * @param host
     * @param username
     * @param password
     * @return
     */
    public static FtpUtil createFtpClient(String host,int port, String username, String password) {
        return new FtpUtil(host,port, username, password);
    }

    /**
     * 创建自定义属性的ftp客户端
     * @param host
     * @param port
     * @param username
     * @param password
     * @param charset
     * @return
     */
    public static FtpUtil createFtpClient(String host, int port, String username, String password, String charset) {
        return new FtpUtil(host, port, username, password, charset);
    }

    /**
     * 方法描述:  转码
     */
    private static String transcode(String text){
        try {
            return new String(text.getBytes("GBK"),FTP.DEFAULT_CONTROL_ENCODING);
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    /**
     * ftp登录
     * @throws Exception
     */
    public static Boolean connect() throws Exception {
        log.info(host + ":" + port+" FTP Connecting...");
        boolean flag = false;
        if (client == null) {
            client = new FTPClient();
        }
        // 已经连接
        if (client.isConnected()) {
            return flag;
        }

        try {
            // 连接FTP Server
            try {
                client.connect(host, port);
            } catch (UnknownHostException e) {
                throw new IOException("Please Check whether the ftp port is correct!" + port);
            }

            // 登陆
            if (username == null || "".equals(username)) {
                // 使用匿名登陆
                client.login(ANONYMOUS_USER, ANONYMOUS_USER);
            } else {
                client.login(username, password);
            }
            // 设置文件格式
            client.setFileType(FTPClient.BINARY_FILE_TYPE);
            FTPClientConfig config = new FTPClientConfig(client.getSystemType().split(" ")[0]);
            config.setServerLanguageCode("zh");
            client.configure(config);
            // 使用被动模式设为默认
            client.enterLocalPassiveMode();
            // 设置文件类型为二进制（如果从FTP下载或上传的文件是压缩文件的时候，不进行该设置可能会导致获取的压缩文件解压失败）
            client.setFileType(FTP.BINARY_FILE_TYPE);
            // 设置模式
            client.setFileTransferMode(FTP.STREAM_TRANSFER_MODE);
            //设置编码格式
            if(FTPReply.isPositiveCompletion(client.sendCommand("OPTS UTF8","ON"))){
                client.setControlEncoding(DEFAULT_CHARSET);
            }else{
                client.setControlEncoding(CHARSET_GBK);
            }
            //登陆成功，返回码是230
            int reply = client.getReplyCode();
            // 判断返回码是否合法
            if (!FTPReply.isPositiveCompletion(reply)) {
                client.disconnect();
                log.error("FTP server refused connection.");
                throw new Exception("FTP server refused connection.");
            } else {
                initFtpBasePath();
                flag = true;
                log.info("FTP connection success!");
            }
        } catch (IOException e) {
            log.info("ftp connection FTP fail!",e);
            throw new Exception("connection FTP fail! " + e.getMessage());
        }
        return flag;
    }

    /**
     * 关闭FTP服务连接
     */
    public static void close() {
        try{
            if(client.isConnected()){
                client.disconnect();
                log.info("FTP is closed already.");
            }
        }catch(IOException e) {
            log.info("disConnection FTP fail!"+e);
        }
    }


    /**
     * 连接ftp时保存刚登陆ftp时的路径
     * @throws IOException
     */
    private static void initFtpBasePath() throws IOException {
        if (StringUtils.isEmpty(ftpBasePath)) {
            synchronized (client) {
                if (StringUtils.isEmpty(ftpBasePath)) {
                    ftpBasePath = client.printWorkingDirectory();
                }
            }
        }
    }

    /**
     * 获取文件列表
     *
     * @return
     */
    public List<FTPFile> list() {
        List<FTPFile> list = null;
        try {
            FTPFile ff[] = client.listFiles(ftpPath);
            if (ff != null && ff.length > 0) {
                list = new ArrayList<FTPFile>(ff.length);
                Collections.addAll(list, ff);
            } else {
                list = new ArrayList<FTPFile>(0);
            }
        } catch (IOException e) {
            log.error("获取文件列表异常,"+e.getMessage());
        }
        return list;
    }

    /**
     * 获取文件夹下某个文件信息流
     * @param ftpClient
     * @param path 文件路径
     * @param fileName  文件名
     * @return
     */
    public static InputStream getFTPFileInputStream(FTPClient ftpClient,String path,String fileName){
        InputStream in = null;
        try {
            if(ftpClient == null){
                return in;
            }
            FTPFile[] files = ftpClient.listFiles();
            if(files.length > 0){
                //解决中文路径问题
                in = ftpClient.retrieveFileStream(new String(fileName.getBytes("gbk"), FTP.DEFAULT_CONTROL_ENCODING));
            }
        }catch (Exception e){
            log.error("FTP读取数据异常,"+e.getMessage());
        }finally{
            //关闭连接
            if(ftpClient != null){
                close();
            }
            return in;
        }
    }

    /**
     * 获取Ftp 文件内容
     * @param path 路径
     * @param fileName 文件名
     * @param encoding 编码
     * @return
     * @throws Exception
     */
    public static String getFtpFile(String path,String fileName,String encoding) throws Exception{
        log.info(path+" " +fileName);
        if(client == null){
            log.error("FTP连接失败!");
            return null;
        }

        BufferedReader br = null;
        InputStream in = null;
        in = getFTPFileInputStream(client,path,fileName);
        StringBuffer result = new StringBuffer();
        if(in == null){
            log.error("读取文件失败!");
            return null;
        }
        try {
            if(StringUtils.isEmpty(encoding)){
                encoding = "UTF-8";
            }
            br = new BufferedReader(new InputStreamReader(in, encoding));
            String data = null;
            while ((data = br.readLine()) != null) {
                result.append(data+"\n");
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //关闭ftp连接
            close();
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    log.error("BufferedReader关闭异常,"+e.getMessage());
                }
            }
            if (in != null) {
                try {
                    //关闭io流
                    in.close();
                } catch (IOException e) {
                    log.error("InputStream关闭异常,"+e.getMessage());
                }
            }
        }
        return result.toString();
    }

    /**
     * 获取文件夹下的所有文件列表
     * @param ftpClient
     * @param path 路径
     * @return
     */
    public static FTPFile[] getFTPDirectoryFiles(FTPClient ftpClient,String path) {
        FTPFile[] files = null;
        try {
            ftpClient.changeWorkingDirectory(path);
            files = ftpClient.listFiles();
        } catch (Exception e) {
            log.error("FTP读取数据异常,"+e.getMessage());
        }
        return files;
    }

    /**
     * 判断是否是.txt 和 .csv 文件
     * @param fileName  文件名称
     * @return
     */
    public static boolean getFileStatus(String fileName){
        boolean resutl = false;
        if(StringUtils.isNotEmpty(fileName)){
            int index = fileName.lastIndexOf(".");
            if(index > 0){
                String fileFormat = fileName.substring(index);

                if(".txt".equalsIgnoreCase(fileFormat) || ".csv".equalsIgnoreCase(fileFormat)){
                    resutl = true;
                }
            }
        }
        return resutl;
    }
    /**
     * 获取当前所处的工作目录
     * @return java.lang.String 当前所处的工作目录
     */
    public static String printWorkingDirectory() {
        if (!client.isConnected()) {
            return "";
        }
        try {
            return client.printWorkingDirectory();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";
    }

    /**
     * 切换到当前工作目录的父目录下
     * @return boolean 切换成功返回true
     */
    public static boolean changeToParentDirectory() {
        if (!client.isConnected()) {
            log.info("FTP服务未连接,请检查!");
            return false;
        }
        try {
            boolean flag = client.changeToParentDirectory();
            //当前换成功后
            if (flag) {
                return flag;
            }
        } catch (IOException e) {
            log.error("切换到当前工作目录的父目录异常,"+e.getMessage());
        }
        return false;
    }

    /**
     * 返回当前工作目录的上一级目录
     * @return java.lang.String 当前工作目录的父目录
     */
    public static String printParentDirectory() {
        log.info("当前工作目录为："+printWorkingDirectory());
        if (!client.isConnected()) {
            return "";
        }
        String w = printWorkingDirectory();
        changeToParentDirectory();
        String p = printWorkingDirectory();
        changeWorkingDirectory(w);
        log.info("当前工作目录的父目录为："+p);
        return p;
    }

    /**
     * 改变工作目录
     * @param dir ftp服务器上目录
     * @return boolean 改变成功返回true
     */
    public static boolean changeWorkingDirectory(String dir) {
        if(client == null) {
            return false;
        }
        if (!client.isConnected()) {
            return false;
        }
        try {
            boolean flag = client.changeWorkingDirectory(dir);
            if (flag) {
                return flag;
            }
        } catch (IOException e) {
            log.error("工作目录异常,"+e.getMessage());
        }
        return false;
    }

    /**
     * 列出ftp上文件目录下的文件
     * @param filePath ftp上文件目录
     * @return java.util.List<java.lang.String>
     * @throws IOException
     */
    public List<String> listFileNames(String filePath) throws IOException {
        FTPFile[] ftpFiles = client.listFiles(filePath);
        List<String> fileList = new ArrayList<>();
        if (ftpFiles != null) {
            for (int i = 0; i < ftpFiles.length; i++) {
                FTPFile ftpFile = ftpFiles[i];
                if (ftpFile.isFile()) {
                    fileList.add(ftpFile.getName());
                }
            }
        }

        return fileList;
    }

    /**
     * 从FTP服务器上下载文件到本地,支持断点续传，下载百分比汇报
     * @param directory 下载目录
     * @param downloadFile 下载的文件
     * @param saveFile 存在本地的路径
     * @throws IOException
     */
    public static Boolean downloadFile(String directory, String downloadFile, String saveFile) {
        //文件下载成功标识
        boolean flag = false;
        log.info("开始从FTP服务器上下载文件>>>>>FTP目录：[{}],下载到本地目录：[{}],下载的文件为：[{}]",directory,saveFile,downloadFile);
        if(!directory.endsWith("/")) {
            directory = directory + "/";
        }
        if(!saveFile.endsWith("/")) {
            saveFile = saveFile + "/";
        }
        try {
            Boolean loginFlag = connect();
            if(!loginFlag) {
                log.error("FTP登录失败");
                return flag;
            }
            //进入ftp目录
            boolean cdFlag = changeWorkingDirectory(directory);
            if(!cdFlag) {
                log.error("进入FTP目录失败，该目录不存在！！！");
                return flag;
            }
            //若本地没有目录，则直接创建
            File fileDir = new File(saveFile);
            if(!fileDir.exists()) {
                log.info("本地不存在目录["+saveFile+"],正在创建!" );
                fileDir.mkdirs();
            }
            //判断文件是否存在
            FTPFile[] files =client.listFiles();
            if(files==null || files.length<=0){
                log.info("该文件不存在！！！");
                return flag;
            }

            long lRemoteSize = files[0].getSize();
            File f = new File(saveFile+downloadFile);
            // 本地存在文件，进行断点下载
            if (f.exists()) {
                long localSize = f.length();
                // 判断本地文件大小是否大于远程文件大小
                if (localSize >= lRemoteSize) {
                    log.info("本地文件大于远程文件，下载中止");
                }
                // 进行断点续传，并记录状态
                FileOutputStream out = new FileOutputStream(f, true);
                client.setRestartOffset(localSize);
                InputStream in = client.retrieveFileStream(directory+downloadFile);
                byte[] bytes = new byte[1024];
                long step = lRemoteSize / 100;
                // 文件过小，step可能为0
                step = step == 0 ? 1 : step;
                long process = localSize / step;
                int c;
                while ((c = in.read(bytes)) != -1) {
                    out.write(bytes, 0, c);
                    localSize += c;
                    long nowProcess = localSize / step;
                    if (nowProcess > process) {
                        process = nowProcess;
                        if (process % 10 == 0) {
                            log.info("下载进度：" + process+"%");
                        }
                    }
                }
                in.close();
                out.close();
                boolean isDo = client.completePendingCommand();
                if (isDo) {
                    log.info("下载进度：100%");
                    log.info("断点续传成功！");
                    flag = true;
                } else {
                    log.info("断点续传失败！");
                }
            } else {
                OutputStream out = new FileOutputStream(f);
                InputStream in = client.retrieveFileStream(directory+downloadFile);
                byte[] bytes = new byte[1024];
                long step = lRemoteSize / 100;
                // 文件过小，step可能为0
                step = step == 0 ? 1 : step;
                long process = 0;
                long localSize = 0L;
                int c;
                while ((c = in.read(bytes)) != -1) {
                    out.write(bytes, 0, c);
                    localSize += c;
                    long nowProcess = localSize / step;
                    if (nowProcess > process) {
                        process = nowProcess;
                        if (process % 10 == 0) {
                            log.info("下载进度：" + process+"%");
                        }
                    }
                }
                in.close();
                out.close();
                boolean upNewStatus = client.completePendingCommand();
                if (upNewStatus) {
                    log.info("下载进度：100%");
                    log.info("文件下载成功>>>>>文件名："+downloadFile);
                    log.info("FTP文件下载成功！");
                    flag = true;
                } else {
                    log.info("文件下载失败>>>>>文件名："+downloadFile);
                }
            }
        } catch (Exception e) {
            log.error("FTP文件下载失败！", e);
            e.printStackTrace();
        } finally {
            close();
        }
        return flag;
    }

    /**
     * 上传文件到服务器,新上传和断点续传
     *
     * @param remoteFile
     *            远程文件名，在上传之前已经将服务器工作目录做了改变
     * @param localFile
     *            本地文件File句柄，绝对路径
     * @return
     * @throws IOException
     */

    public static boolean uploadFile(String remoteFile, String localFile, String uploadFile) throws IOException {
        //文件下载成功标识
        boolean flag = false;
        Boolean loginFlag = null;
        try {
            loginFlag = connect();
            if(!loginFlag) {
                log.error("FTP登录失败");
                return flag;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // 显示进度的上传
        File file = new File(localFile+uploadFile);
        log.info("localFile.length():" + file.length());
        long step = file.length() / 100;
        // 文件过小，step可能为0
        step = step == 0 ? 1 : step;
        long process = 0;
        long localreadbytes = 0L;
        RandomAccessFile raf = new RandomAccessFile(localFile+uploadFile, "r");
        OutputStream out = client.appendFileStream(remoteFile+uploadFile);
        // 断点续传
        File file1 = new File(remoteFile+uploadFile);
        long remoteSize = file.length() - file1.length();
        if (remoteSize > 0) {
            client.setRestartOffset(remoteSize);
            process = remoteSize / step;
            raf.seek(remoteSize);
            localreadbytes = remoteSize;
        }
        byte[] bytes = new byte[1024];
        int c;
        while ((c = raf.read(bytes)) != -1) {
            out.write(bytes, 0, c);
            localreadbytes += c;
            if (localreadbytes / step != process) {
                process = localreadbytes / step;
                if (process % 10 == 0) {
                    log.info("上传进度：" + process);
                }
            }
        }
        out.flush();
        raf.close();
        out.close();
        boolean result = client.completePendingCommand();
        if (remoteSize > 0) {
            if (result) {
                log.info("断点续传成功!");
                flag = true;
            } else {
                log.info("断点续传失败!");
            }
        } else {
            if (result) {
                log.info("上传新文件成功!");
                flag = true;
            } else {
                log.info("上传新文件失败!");
            }
        }
        return flag;
    }

    /**
     * 删除ftp服务器上的文件
     *
     * @param filePath  ftp服务器保存目录
     * @return 成功返回true，否则返回false
     */
    public static boolean delFile(String filePath){
        log.info("删除ftp服务器上的文件：[{}]",filePath);
        boolean flag = false;
        // 1、创建FTPClient对象
        try {
            Boolean loginFlag = connect();
            if(!loginFlag) {
                log.error("FTP登录失败");
                return flag;
            }
            client.deleteFile(filePath);
            client.logout();
            flag = true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally{
            try {
                close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return flag;
    }

    public static void main(String[] args) throws Exception {
        createFtpClient("10.2.14.237",21,"JRS","FTPj1r2s3@mychery");
        connect();
        ftpPath = "zsdeai002_dms";
//        //获取目录下文件列表
        System.out.println("当前位置：" + printWorkingDirectory());
        FTPFile[] ftpFiles = getFTPDirectoryFiles(client,ftpPath);
        for(FTPFile f : ftpFiles){
            if(getFileStatus(f.getName())){
                System.out.println(f.getName());
            }
        }
        // 移动工作空间、切换目录
//        changeWorkingDirectory(printParentDirectory()+"/zsdeai002_dms");
//        System.out.println("当前位置：" + printWorkingDirectory());
//        FTPFile[] ftpFiles1 = getFTPDirectoryFiles(client,FtpUtil.getCurrentPath());
//        for(FTPFile f : ftpFiles1){
//            if(getFileStatus(f.getName())){
//                System.out.println(f.getName());
//            }
//        }
//
//        // 移动工作空间、切换目录
//        changeWorkingDirectory(printParentDirectory()+"/zsdeai2017_sy");
//        System.out.println("当前位置：" + printWorkingDirectory());
//        FTPFile[] ftpFiles2 = getFTPDirectoryFiles(client,ftpUtil.getCurrentPath());
//        for(FTPFile f : ftpFiles2){
//            if(getFileStatus(f.getName())){
//                System.out.println(f.getName());
//            }
//        }
////        // 查询目录下的所有文件夹以及文件
//        List<String> list = ftpUtil.listFileNames(printParentDirectory());
//        for (String s : list) {
//            System.out.println("查询目录下的所有文件夹以及文件:"+s);
//        }

        System.out.println("文件内容是:");
        //获取某个ftp文件内容
        String fileData = null;
        try {
            fileData = getFtpFile(printParentDirectory()+"/zsdeai002_dms","ZSDEAI002A_20221115173251.txt",CHARSET_GBK);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(fileData);

//        downloadFile("/oracle/eaiftp/SYSTEM/JRS/zsdeai001_dms","ZSDEAI001A_20221116115728.txt","E:\\test\\");
//        uploadFile("/oracle/eaiftp/SYSTEM/JRS/zsdeai001_dms/","E:\\test\\","1.txt");
//        ftpPath = "zsdeai001_dms";
////        //获取目录下文件列表
//        System.out.println("当前位置：" + printWorkingDirectory());
//        FTPFile[] ftpFiles2 = getFTPDirectoryFiles(client,ftpPath);
//        for(FTPFile f : ftpFiles2){
//            if(getFileStatus(f.getName())){
//                System.out.println(f.getName());
//            }
//        }
//        delFile("/oracle/eaiftp/SYSTEM/JRS/zsdeai001_dms/1.txt");
//        FTPFile[] ftpFiles1 = getFTPDirectoryFiles(client,ftpPath);
//        for(FTPFile f : ftpFiles1){
//            if(getFileStatus(f.getName())){
//                System.out.println(f.getName());
//            }
//        }
    }
}
