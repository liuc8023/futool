package com.futool;

import com.futool.constant.Constant;
import com.futool.regex.RegexUtil;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Nullable;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

/**
 * @description 字符串处理工具类
 * @author liuc
 * @date 2021/8/24 14:57
 * @since JDK1.8
 * @version V1.0
 */
@Slf4j
public class StringUtils {
    /***
     * @description 判断字符串是否都是数字
     * @param str
     * @return boolean
     * @author liuc
     * @date 2021/8/24 14:59
     * @throws
     */
    public static boolean isNumeric(String str){
        Pattern pattern = Pattern.compile(Constant.IS_NUMERIC);
        return pattern.matcher(str).matches();
    }

    /**
     * @description 字符串的数字部分加1操作，例如01加1得到的是02
     * @author liuc
     * @date 2021/9/21 17:04
     * @since JDK1.8
     * @version V1.0
     */
    public static String addOneForStr(String str){
        if(str != null){
            int fIndex = -1;
            for(int i = 0; i < str.length(); i++){
                char c = str.charAt(i);
                if (c >= '0' && c <= '9'){
                    fIndex = i;
                    break;
                }
            }
            if(fIndex == -1){
                return str;
            }
            String substring = str.substring(fIndex, str.length());
            int num = Integer.parseInt(substring) + 1;
            String zeroStr = addZeroForLeft(substring, String.valueOf(num));
            String preStr = str.substring(0, fIndex);
            String wholeStr = preStr + zeroStr + num;
            return wholeStr;
        }
        return null;
    }

    /**
     * @description 字符串左补0
     * @author liuc
     * @date 2021/9/21 17:04
     * @since JDK1.8
     * @version V1.0
     */
    public static String addZeroForLeft(String sourceStr, String numberStr){
        StringBuffer sb = new StringBuffer();
        int len = sourceStr.length() - numberStr.length();
        for(int i = 0; i < len; i++){
            sb.append("0");
        }
        return sb.toString();
    }

    /**
     * 去除null以及空字符串
     * @param s
     * @return java.lang.String
     * @author liuc
     * @date 2021/11/1 10:28
     * @throws
     */
    public static String trimBlank(String s) {
        if (s == null) {
            return "";
        } else {
            return s.trim().replace("null", "");
        }
    }

    /**
     * 将文件名中的汉字转为UTF8编码的串,以便下载时能正确显示另存的文件名
     * @param s
     * @return java.lang.String
     * @author liuc
     * @date 2021/11/1 10:28
     * @throws
     */
    public static String toUtf8String(String s) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c <= 255) {
                sb.append(c);
            } else {
                byte[] b;

                try {
                    b = Character.toString(c).getBytes(StandardCharsets.UTF_8);
                } catch (Exception ex) {
                    b = new byte[0];
                }
                for (int b1 : b) {
                    int k = b1;
                    if (k < 0) {
                        k += 256;
                    }
                    sb.append("%").append(Integer.toHexString(k).toUpperCase());
                }
            }
        }
        return sb.toString();
    }

    /**
     * 去空格
     * @param str
     * @return java.lang.String
     * @author liuc
     * @date 2021/11/1 10:30
     * @throws
     */
    public static String trim(String str) {
        return (str == null ? "" : str.trim());
    }

    /**
     * 下划线转驼峰命名
     * 例如：hello_world->helloWorld;HELLO_WORLD->helloWorld
     * @param str 下划线命名的字符串
     * @return java.lang.String
     * @author liuc
     * @date 2021/11/1 10:30
     * @throws
     */
    public static String underlineToHump(String str) {
        StringBuilder result = new StringBuilder();
        String[] strArray = str.split(Constant.UNDERLINE);
        for (String s : strArray) {
            if (!str.contains(Constant.UNDERLINE)) {
                result.append(s);
                continue;
            }
            if (result.length() == 0) {
                result.append(s.toLowerCase());
            } else {
                result.append(s.substring(0, 1).toUpperCase());
                result.append(s.substring(1).toLowerCase());
            }
        }
        return result.toString();
    }

    /**
     * 驼峰命名转为下划线命名
     * 例如：helloWorld->HELLO_WORLD
     * @param str 驼峰命名的字符串
     * @return java.lang.String
     * @author liuc
     * @date 2021/11/1 14:44
     * @throws
     */
    public static String humpToUnderline(String str) {
        StringBuilder sb = new StringBuilder(str);
        //定位
        int temp = 0;
        if (!str.contains(Constant.UNDERLINE)) {
            for (int i = 0; i < str.length(); i++) {
                if (Character.isUpperCase(str.charAt(i))) {
                    sb.insert(i + temp, Constant.UNDERLINE);
                    temp += 1;
                }
            }
        }
        return sb.toString().toUpperCase();
    }

    /**
     * 将下划线大写方式命名的字符串转换为驼峰式。如果转换前的下划线大写方式命名的字符串为空，
     * 则返回空字符串。 例如：HELLO_WORLD->HelloWorld
     * @param name 转换前的下划线大写方式命名的字符串
     * @return java.lang.String 转换后的驼峰式命名的字符串
     * @author liuc
     * @date 2021/11/1 10:35
     * @throws
     */
    public static String convertToCamelCase(String name) {
        StringBuilder result = new StringBuilder();
        // 快速检查
        if (name == null || name.isEmpty()) {
            // 没必要转换
            return "";
        } else if (!name.contains(Constant.UNDERLINE)) {
            // 不含下划线，仅将首字母大写
            return name.substring(0, 1).toUpperCase() + name.substring(1);
        }
        // 用下划线将原始字符串分割
        String[] camels = name.split(Constant.UNDERLINE);
        for (String camel : camels) {
            // 跳过原始字符串中开头、结尾的下换线或双重下划线
            if (camel.isEmpty()) {
                continue;
            }
            // 首字母大写
            result.append(camel.substring(0, 1).toUpperCase());
            result.append(camel.substring(1).toLowerCase());
        }
        return result.toString();
    }

    /**
     * 字符串转List
     * @param str
     * @return java.util.List<java.lang.String>
     * @author liuc
     * @date 2021/11/1 11:35
     * @throws
     */
    public static List<String> stringToList(String str){
        List<String> list = null;
        if (str == null || str.isEmpty()) {
            list = new ArrayList<String>();
        }
        list = Arrays.asList(str.split(Constant.COMMA));
        return list;
    }

    /**
     * List转String,以逗号隔开
     * @param list
     * @return java.lang.String
     * @author liuc
     * @date 2021/11/1 14:35
     * @throws
     */
    public static String listToString(List<?> list){
        String str = null;
        if (list != null && list.size() >0) {
            str = org.apache.commons.lang3.StringUtils.join(list.toArray(), Constant.COMMA);
        }
        return str;
    }

    public static String replace(String inString, String oldPattern, @Nullable String newPattern) {
        if (!hasLength(inString) || !hasLength(oldPattern) || newPattern == null) {
            return inString;
        }
        int index = inString.indexOf(oldPattern);
        if (index == -1) {
            // no occurrence -> can return input as-is
            return inString;
        }

        int capacity = inString.length();
        if (newPattern.length() > oldPattern.length()) {
            capacity += 16;
        }
        StringBuilder sb = new StringBuilder(capacity);

        int pos = 0;  // our position in the old string
        int patLen = oldPattern.length();
        while (index >= 0) {
            sb.append(inString, pos, index);
            sb.append(newPattern);
            pos = index + patLen;
            index = inString.indexOf(oldPattern, pos);
        }

        // append any characters to the right of a match
        sb.append(inString, pos, inString.length());
        return sb.toString();
    }

    public static boolean hasLength(@Nullable String str) {
        return (str != null && !str.isEmpty());
    }

    /**
     * Object转换为String
     * @param o
     * @return java.lang.String
     * @author liuc
     * @date 2021/12/8 16:53
     * @throws
     */
    public static String objectToString(Object o){
        if(o == null){
            return "";
        }else if(o instanceof String){
            return (String)o;
        }else if(o instanceof Integer){
            return String.valueOf((Integer)o);
        }else if(o instanceof Long){
            return String.valueOf((Long)o);
        }else if(o instanceof Double){
            return String.valueOf((Double)o);
        }else if(o instanceof Float){
            return String.valueOf((Float)o);
        } else{
            return "";
        }
    }

    /**
     * 判定输入的是否是汉字
     * @param c 被校验的字符
     * @return true代表是汉字
     * @throws
     * @date 2020/1/3 15:06
     * @author liuc
     **/
    public static boolean isChinese(char c) {
        Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
        if (ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS
                || ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS
                || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A
                || ub == Character.UnicodeBlock.GENERAL_PUNCTUATION
                || ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION
                || ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS) {
            return true;
        }
        return false;
    }

    /**
     * 校验String是否全是中文
     * @param name 被校验的字符串
     * @return true代表全是汉字
     * @throws
     * @date 2020/1/3 15:07
     * @author liuc
     **/
    public static boolean checkNameChese(String name) {
        boolean res = true;
        //转换为数组
        char[] cTemp = name.toCharArray();
        for (int i = 0; i < name.length(); i++) {
            //逐个判断是否为中文
            if (!isChinese(cTemp[i])) {
                res = false;
                break;
            }
        }
        return res;
    }

    /**
     * 校验String是否全是中文
     * @param name 被校验的字符串
     * @return true代表全是汉字
     * @throws
     * @date 2020/1/3 15:07
     * @author liuc
     **/
    public static boolean checkNameCheseNoSpeSymbol(String name) {
        boolean res = true;
        if (isContainChinessSymbol(name)) {
            return false;
        }
        if (validateLegalString(name)) {
            return false;
        }
        //转换为数组
        char[] cTemp = name.toCharArray();
        for (int i = 0; i < name.length(); i++) {
            //逐个判断是否为中文
            if (!isChinese(cTemp[i])) {
                res = false;
                break;
            }
        }
        return res;
    }


    /**
     * 该函数判断一个字符串是否包含标点符号（中文英文标点符号）。
     * 原理是原字符串做一次清洗，清洗掉所有标点符号。
     * 此时，如果原字符串包含标点符号，那么清洗后的长度和原字符串长度不同。返回true。
     * 如果原字符串未包含标点符号，则清洗后长度不变。返回false。
     * @param s 被校验的字符串
     * @return 返回true 包含标点符号
     * @throws
     * @date 2020/1/3 15:50
     * @author liuc
     **/
    public static boolean isContainChinessSymbol(String s) {
        boolean b = false;
        String tmp = s;
        tmp = tmp.replaceAll("\\p{P}", "");
        if (s.length() != tmp.length()) {
            b = true;
        }
        return b;
    }

    /**
     * 校验一个字符是否是汉字
     * @param c 被校验的字符
     * @return true代表是汉字
     * @throws
     * @date 2020/1/3 15:13
     * @author liuc
     **/
    public static boolean isChineseChar(char c) {
        try {
            return String.valueOf(c).getBytes("UTF-8").length > 1;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 验证字符串内容是否包含下列非法字符
     * `~!#%^&*=+\\|{};:'\",<>/?○●★☆☉♀♂※¤╬の〆
     * @param content 被校验字符串内容
     * @return true 代表包含非法字符
     * @throws
     * @date 2020/1/3 15:52
     * @author liuc
     **/
    public static boolean validateLegalString(String content) {
        boolean flag = false;
        String illegal = "`~!#%^&*=+\\|{};:'\",<>/?○●★☆☉♀♂※¤╬の〆";
        for (int i = 0; i < content.length(); i++) {
            for (int j = 0; j < illegal.length(); j++) {
                if (content.charAt(i) == illegal.charAt(j)) {
                    flag =true;
                }
            }
        }
        return flag;
    }

    /**
     * 包括零的正整数字符串左侧补N个0
     * @param sourceStr 需要左侧补0的包括零的正整数字符串
     * @param number 补零之后数字的总长度
     * @return
     */
    public static String strLeftFillZero(String sourceStr, String number){
        if (UtilValidate.isEmpty(sourceStr)) {
            return null;
        }
        boolean flag = RegexUtil.isPositiveIntIncludesZero(sourceStr);
        if (!flag) {
            throw new RuntimeException("请输入包括零的正整数字符串");
        }
        if (!RegexUtil.isPositiveIntIncludesZero(number)) {
            throw new RuntimeException("请输入补0后的字符串长度!!!");
        }
        if (sourceStr.length() > Integer.valueOf(number)) {
            throw new RuntimeException("补0后的字符串长度需要小于等于补零之后数字的总长度!!!");
        }
        StringBuffer sb = new StringBuffer();
        sb.append(sourceStr);
        int temp = Integer.valueOf(number) - sb.length();
        if (temp > 0) {
            while (sb.length() < Integer.valueOf(number)) {  //若长度不足进行补零
                sb.insert(0, "0");
            }
        }
        return sb.toString();
    }

    /**
     * 左侧删除0
     * @param sourceStr
     * @return
     */
    public static String strLeftRemoveZero(String sourceStr){
        if (UtilValidate.isEmpty(sourceStr)) {
            return null;
        }
        return sourceStr.replaceAll("^(0+)","");
    }

    /**
     * 字符串首字母小写
     * @param str
     * @return
     */
    public static String lowerFirst(String str){
        char[] cs = str.toCharArray();
        cs[0] += 32;
        return String.valueOf(cs);
    }
    /**
     * 字符串首字母大写
     * @param str
     * @return
     */
    public static String upperFirst(String str){
        // 进行字母的ascii编码前移，效率要高于截取字符串进行转换的操作
        char[] cs=str.toCharArray();
        cs[0] -= 32;
        return String.valueOf(cs);
    }
}
