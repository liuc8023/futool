package com.futool.io;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPOutputStream;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream;
import org.apache.commons.io.IOUtils;

@Log4j2
public class GZipUtils {
    public static final String GZIP_ENCODE_UTF_8 = "UTF-8";
    /**
     * 使用TAR算法进行压缩.
     * @param tarFileBytesMap 待压缩文件的Map集合.
     * @return 压缩后的TAR文件字节数组.
     * @throws Exception 压缩过程中可能发生的异常，若发生异常，则返回的字节数组长度为0.
     */
    public static byte[] compressByTar(Map<String, byte[]> tarFileBytesMap) throws Exception {
        // 变量定义.
        ByteArrayOutputStream tarBaos = null;
        TarArchiveOutputStream tarTaos = null;
        TarArchiveEntry tarTae = null;

        try {
            // 压缩变量初始化.
            tarBaos = new ByteArrayOutputStream();
            tarTaos = new TarArchiveOutputStream(tarBaos);
            // // 将文件添加到TAR条目中.
            for (Map.Entry<String, byte[]> fileEntry : tarFileBytesMap.entrySet()) {
                tarTae = new TarArchiveEntry(fileEntry.getKey());
                tarTae.setName(fileEntry.getKey());
                tarTae.setSize(fileEntry.getValue().length);
                tarTaos.putArchiveEntry(tarTae);
                tarTaos.write(fileEntry.getValue());
                tarTaos.closeArchiveEntry();
            }
        } finally {
            if (tarTaos != null) {
                tarTaos.close();
            }
            if (null == tarBaos) {
                tarBaos = new ByteArrayOutputStream();
            }
        }
        return tarBaos.toByteArray();
    }

    /**
     * 使用TAR算法进行解压.
     * @param sourceTarFileBytes TAR文件字节数组.
     * @return 解压后的文件Map集合.
     * @throws Exception 解压过程中可能发生的异常，若发生异常，返回Map集合长度为0.
     */
    public static Map<String, byte[]> uncompressByTar(byte[] sourceTarFileBytes) throws Exception {
        // 变量定义.
        TarArchiveEntry sourceTarTae = null;
        ByteArrayInputStream sourceTarBais = null;
        TarArchiveInputStream sourceTarTais = null;
        Map<String, byte[]> targetFilesFolderMap = null;

        try {
            // 解压变量初始化.
            targetFilesFolderMap = new HashMap<String, byte[]>();
            sourceTarBais = new ByteArrayInputStream(sourceTarFileBytes);
            sourceTarTais = new TarArchiveInputStream(sourceTarBais);
            // 条目解压缩至Map中.
            while ((sourceTarTae = sourceTarTais.getNextTarEntry()) != null) {
                targetFilesFolderMap.put(sourceTarTae.getName(), IOUtils.toByteArray(sourceTarTais));
            }
        } finally {
            if (sourceTarTais != null) {
                sourceTarTais.close();
            }
        }
        return targetFilesFolderMap;
    }

    /**
     * 使用GZIP算法进行压缩.
     * @param sourceFileBytes 待压缩文件的Map集合.
     * @return 压缩后的GZIP文件字节数组.
     * @throws Exception 压缩过程中可能发生的异常，若发生异常，则返回的字节数组长度为0.
     */
    public static byte[] compressByGZip(byte[] sourceFileBytes) throws IOException {
        // 变量定义.
        ByteArrayOutputStream gzipBaos = null;
        GzipCompressorOutputStream gzipGcos = null;

        try {
            // 压缩变量初始化.
            gzipBaos = new ByteArrayOutputStream();
            gzipGcos = new GzipCompressorOutputStream(gzipBaos);
            // 采用commons-compress提供的方式进行压缩.
            gzipGcos.write(sourceFileBytes);
        } finally {
            if (gzipGcos != null) {
                gzipGcos.close();
            }
            if (null == gzipBaos) {
                gzipBaos = new ByteArrayOutputStream();
            }
        }
        return gzipBaos.toByteArray();
    }

    /**
     * 使用GZIP算法进行解压.
     * @param sourceGZipFileBytes GZIP文件字节数组.
     * @return 解压后的文件Map集合.
     * @throws Exception 解压过程中可能发生的异常，若发生异常，则返回的字节数组长度为0.
     */
    public static byte[] uncompressByGZip(byte[] sourceGZipFileBytes) throws IOException {
        // 变量定义.
        ByteArrayOutputStream gzipBaos = null;
        ByteArrayInputStream sourceGZipBais = null;
        GzipCompressorInputStream sourceGZipGcis = null;

        try {
            // 解压变量初始化.
            gzipBaos = new ByteArrayOutputStream();
            sourceGZipBais = new ByteArrayInputStream(sourceGZipFileBytes);
            sourceGZipGcis = new GzipCompressorInputStream(sourceGZipBais);
            // 采用commons-compress提供的方式进行解压.
            gzipBaos.write(IOUtils.toByteArray(sourceGZipGcis));
        } catch (IOException e) {
            log.error(e.getMessage());
            throw new IOException("解压缩失败!");
        } finally {
            if (sourceGZipGcis != null) {
                try {
                    gzipBaos.close();
                    sourceGZipGcis.close();
                } catch (IOException e) {
                    log.error(e.getMessage());
                    throw new IOException("关闭流失败!");
                }
            }
        }
        return gzipBaos.toByteArray();
    }

    /**
     * 将字符串压缩为GZIP字节数组
     * @param str
     * @return
     */
    public static byte[] compress(String str){
        return compress(str,GZIP_ENCODE_UTF_8);
    }

    /**
     * 将字符串压缩为GZIP字节数组
     * @param str 需要压缩的字符串
     * @param encoding 压缩使用的字节编码
     * @return
     */
    public static byte[] compress(String str,String encoding){
        if (str == null || str.length() == 0) {
            return null;
        }
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        GZIPOutputStream gzip;
        try {
            gzip = new GZIPOutputStream(out);
            gzip.write(str.getBytes(encoding));
            gzip.close();
        }catch (IOException e) {
            log.error(e.getMessage());
        }
        return out.toByteArray();
    }

    /**
     * 解压并返回字符串
     * @param bytes 需要解压的字节数组
     * @return String 解压后返回的字符串
     * @throws IOException
     */
    public static String uncompressToString(byte[] bytes) throws IOException {
        byte[] result = new byte[0];
        try {
            result = uncompressByGZip(bytes);
        } catch (IOException e) {
            log.error(e.getMessage());
            throw new IOException("解压缩字节数组为字符串失败!");
        }
        return new String(result);
    }
}
