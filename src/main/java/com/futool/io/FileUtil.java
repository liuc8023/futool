package com.futool.io;

import com.futool.UtilValidate;
import lombok.extern.slf4j.Slf4j;
import java.io.*;
import java.util.List;

/**
 * @author liuc
 * @version V1.0
 * @description
 * @date 2021/9/22 16:55
 * @since JDK1.8
 */
@Slf4j
public class FileUtil {
    private static final long B = 1;
    private static final long K = B<<10;
    private static final long M = K<<10;
    private static final long G = M<<10;
    private static final long T = G<<10;

    /**
     * 将list字符串写入txt文件
     * @param list
     * @param path 文件存放路径
     * @param fileName 文件名
     * @return 写入文件成功返回true，否则返回false
     */
    public static boolean writeFileToContext(List<String> list ,String path,String fileName){
        boolean flag = false;
        try {
            if(UtilValidate.isNotEmpty(path)){
                File filePath =new File(path);
                //判断文件夹是否存在，不存在创建文件夹
                if(!filePath.exists()&& !filePath .isDirectory()){
                    log.error("文件夹[{}]不存在",filePath);
                    log.info("开始创建文件：[{}]",filePath);
                    File fileDir = new File(path);
                    fileDir.mkdirs();
                    log.info("创建文件夹完成");
                }
            }
            File file = new File(path+fileName);
            //如果没有文件就创建
            if (!file.exists()){
                log.error("文件[{}]不存在",fileName);
                log.info("开始创建文件：[{}]",fileName);
                file.createNewFile();
                log.info("创建文件完成");
            }
        }catch (IOException e){
            log.error("创建文件[{}]失败，失败原因：[{}]",fileName,e.getMessage());
        }
        FileOutputStream out = null;
        try {
            log.info("开始写入文件[{}]",path+fileName);
            out = new FileOutputStream(path+fileName,true);
            for (int i = 0; i < list.size(); i++) {
                out.write(list.get(i).getBytes("UTF-8"));
                String newLine = System.getProperty("line.separator");
                out.write(newLine.getBytes());
                out.flush();
            }
            log.info("写入文件成功!");
            flag = true;
        }catch (IOException e){
            log.error("写入文件失败，失败原因：[{}]",e.getMessage());
            flag = false;
        }finally {
            if (out != null){
                try {
                    //释放资源
                    out.close();
                    log.info("关闭流，释放资源成功");
                }catch (IOException e){
                    log.error("关闭写入文件流失败，失败原因:[{}]",e.getMessage());
                }
            }
        }
        return flag;
    }

    /**
     * 删除目标文件夹及以下的所有文件夹及文件
     * @param fileName
     * @param isForceDelete 是否强制删除，true-强制删除 false-不强制删除(如果文件夹下面还是文件夹，那就不删除)
     *     递归的执行效率很低，并且对资源的占用情况随着任务规模的扩大，对资源的占用将呈几何式增长么，
     *     你想一下，如果目标文件夹下面存在大量的层级比较深的文件和文件夹时，这时候的执行效率是比较
     *     低的，而且很占资源。一旦你选择了递归算法，我觉得执行效率没啥改变了，但是资源占用方面可以
     *     有所改变，因此可以从这方面考虑下。这时候不用疑惑，java的GC线程虽然是实时的在检测着，但
     *     是一旦系统规模大了，难免有些照应不过了，因此有些垃圾对象可能会删除的有点迟，这里可以查一
     *     下，gc回收垃圾的机制有一种是查询对象是否还有被引用，一旦没有被引用，则立即启用回收，准备回收
     */
    public static void deleteFile(String fileName,boolean isForceDelete){
        File file = new File(fileName);
        if(file.exists()){
            file.delete();
        }
        if(file.exists()){
            if(!isForceDelete){
                return;
            }
            String[] paths = file.list();
            for(String str:paths){
                deleteFile(fileName+File.separator+str,isForceDelete);
            }
            file.delete();
            paths  = null;
        }
        file = null;
    }

    /**
     *
     * 删除文件或者目录，如果目录不为空，则递归删除
     * @param filePath 你想删除的文件的路径
     * @throws FileNotFoundException 如果该路径所对应的文件不存在，抛出异常
     *
     */
    public static void delete(String filePath) throws FileNotFoundException {
        File file = new File(filePath);
        delete(file);
    }

    /**
     *
     * 删除文件或者目录，如果目录不为空，则递归删除
     * @param file filepath 你想删除的文件
     * @throws FileNotFoundException 如果文件不存在，抛出异常
     *
     */
    public static void delete(File file) throws FileNotFoundException {
        if (file.exists()) {
            File[] fileList = null;
            //如果是目录，则递归删除该目录下的所有东西
            if (file.isDirectory() && (fileList = file.listFiles()).length != 0) {
                for (File f : fileList) {
                    delete(f);
                }
                //现在可以当前目录或者文件了
                file.delete();
            } else {
                throw new FileNotFoundException(file.getAbsolutePath() + " is not exists!!!");
            }
        }
    }
    /**
     * through output write the byte data which read from input
     * 将从input处读取的字节数据通过output写到文件
     * @param input
     * @param output
     * @throws IOException
     */
    private static void copyfile(InputStream input, OutputStream output) {
        byte[] buf = new byte[1024];
        int len;
        try {
            while((len=input.read(buf))!=-1){
                output.write(buf, 0, len);
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        } finally{
            try {
                input.close();
                output.close();
            } catch (IOException e) {
                log.error(e.getMessage());
            }

        }
    }

    public static void copyFile(String src, String des, boolean overlay) throws FileNotFoundException{
        copyFile(new File(src), new File(des), overlay);
    }

    /**
     *
     * @param src 源文件
     * @param des 目标文件
     * @throws FileNotFoundException
     */
    public static void copyFile(File src, File des, boolean overlay) throws FileNotFoundException {
        FileInputStream fis = null;
        FileOutputStream fos = null;
        BufferedInputStream bis = null;
        BufferedOutputStream bos = null;
        if(src.exists()){
            try {
                fis = new FileInputStream(src);
                bis = new BufferedInputStream(fis);
                boolean canCopy = false;
                //是否能够写到des
                if(!des.exists()){
                    des.createNewFile();
                    canCopy = true;
                }else if(overlay){
                    canCopy = true;
                } if(canCopy){
                    fos = new FileOutputStream(des);
                    bos = new BufferedOutputStream(fos);
                    copyfile(bis, bos);
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            throw new FileNotFoundException(src.getAbsolutePath()+" not found!!!");
        }
    }

    public static void copyDirectory(String src, String des) throws FileNotFoundException{
        copyDirectory(new File(src), new File(des));
    }

    public static void copyDirectory(File src, File des) throws FileNotFoundException{
        if(src.exists()){
            if(!des.exists()){
                des.mkdirs();
            }
            File[] fileList = src.listFiles();
            for(File file:fileList){
                //如果是目录则递归处理
                if(file.isDirectory()){
                    copyDirectory(file, new File(des.getAbsolutePath()+"/"+file.getName()));
                } else{
                    //如果是文件，则直接拷贝
                    copyFile(file, new File(des.getAbsolutePath()+"/"+file.getName()), true);
                }
            }
        } else{
            throw new FileNotFoundException(src.getAbsolutePath()+" not found!!!");
        }
    }

    public static String toUnits(long length){
        String sizeString = "";
        if(length<0){
            length = 0;
        }
        if(length>=T){
            sizeString = sizeString + length / T + "T"  ;
            length %= T;
        }
        if(length>=G){
            sizeString = sizeString + length / G + "G" ;
            length %= G;
        }
        if(length>=M){
            sizeString = sizeString + length / M + "M";
            length %= M;
        }
        if(length>=K){
            sizeString = sizeString + length / K+ "K";
            length %= K;
        }
        if(length>=B){
            sizeString = sizeString + length / B+ "B";
            length %= B;
        }
        return sizeString;
    }

    private static long getTotalLength1(File file){
        long cnt = 0;
        if(file.isDirectory()){
            File[] fileList = file.listFiles();
            for(File f : fileList){
                cnt += getTotalLength1(f);
            }
        }
        else{
            cnt += file.length();
        }
        return cnt;
    }

    public static long getTotalLength(String filepath) throws FileNotFoundException{
        return getTotalLength(new File(filepath));
    }

    public static long getTotalLength(File file) throws FileNotFoundException{
        if(file.exists()){
            return getTotalLength1(file);
        }
        else{
            throw new FileNotFoundException(file.getAbsolutePath()+" not found!!!");
        }
    }

    /**
     * 根据文件路径，如果文件路径有就不生成，没有就创建路径且生成文件
     * @param path
     * @return java.io.File
     * @author liuc
     * @date 2021/11/6 15:05
     * @throws
     */
    public static File getFile(String path){
        File file = new File(path);
        //判断指定文件路径是否存在，不存在就创建文件路径
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }
        //判断指定文件是否存在，不存在就创建文件
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            log.info("文件已创建完成!");
        }
        return file;
    }

    /**
     * 根据文件全路径判断文件是否存在，例如/home/temp/1.txt
     * @param filePath 文件全路径
     * @return boolean 存在返回true,不存在返回false
     * @author liuc
     * @date 2021/11/8 20:18
     * @throws
     */
    public static boolean isExists(String filePath){
        File file = new File(filePath);
        if (file.exists()) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * File转byte[]数组
     *
     * @param fileFullPath
     * @return
     */
    public static byte[] fileTobyte(String fileFullPath) {
        if (fileFullPath == null || "".equals(fileFullPath)) {
            return null;
        }
        return fileToByte(new File(fileFullPath));
    }

    /**
     * File转byte[]数组
     *
     * @param file
     * @return
     */
    public static byte[] fileToByte(File file) {
        if (file == null) {
            return null;
        }
        FileInputStream fileInputStream = null;
        ByteArrayOutputStream byteArrayOutputStream = null;
        try {
            fileInputStream = new FileInputStream(file);
            byteArrayOutputStream = new ByteArrayOutputStream();
            byte[] b = new byte[1024];
            int n;
            while ((n = fileInputStream.read(b)) != -1) {
                byteArrayOutputStream.write(b, 0 , n);
            }
            return byteArrayOutputStream.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (byteArrayOutputStream != null) {
                try {
                    byteArrayOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    /**
     * byte[]数组转File
     *
     * @param bytes
     * @param fileFullPath
     * @return
     */
    public static File byteToFile(byte[] bytes, String fileFullPath) {
        if (bytes == null) {
            return null;
        }
        FileOutputStream fileOutputStream = null;
        try {
            File file = new File(fileFullPath);
            //判断文件是否存在
            if (file.exists()) {
                file.mkdirs();
            }
            fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(bytes);
            return file;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                }  catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            }
        }
        return null;
    }
}
