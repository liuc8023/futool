package com.futool.pinyin.conver;

import com.futool.UtilValidate;
import com.futool.pinyin.exception.IllegalPinyinException;
import com.futool.pinyin.pinyin4j.PinyinHelper;
import com.futool.pinyin.util.ArrayUtils;
import com.futool.pinyin.voc.Py4jDictionary;
import com.google.common.collect.ArrayListMultimap;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.HanyuPinyinVCharType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;
import java.util.List;

/**
* 
* @className PinyinConverter
* @author liuc
* @date 2019-12-21 22:19
* @since JDK 1.8
**/
public class PinyinConverter implements Converter {

    private final ArrayListMultimap<String,String> duoYinZiMap;
    public PinyinConverter(){
        this.duoYinZiMap = Py4jDictionary.getDefault().getDuoYinZiMap();
    }

    @Override
    public String[] getPinyin(char ch) throws IllegalPinyinException {
        try{
            HanyuPinyinOutputFormat outputFormat = new HanyuPinyinOutputFormat();
            outputFormat.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
            outputFormat.setCaseType(HanyuPinyinCaseType.LOWERCASE);
            outputFormat.setVCharType(HanyuPinyinVCharType.WITH_U_AND_COLON);
            //ASCII >=33 ASCII<=125的直接返回 ,ASCII码表：http://www.asciitable.com/
            if(ch>=32 && ch<=125){
                return new String[]{String.valueOf(ch)};
            }
            return ArrayUtils.distinct(PinyinHelper.toHanyuPinyinStringArray(ch, outputFormat));
        } catch (BadHanyuPinyinOutputFormatCombination e) {
            throw new IllegalPinyinException(e);
        }
    }

    @Override
    public String[] getPinyinWithV(char ch) throws IllegalPinyinException {
        try{
            HanyuPinyinOutputFormat outputFormat = new HanyuPinyinOutputFormat();
            outputFormat.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
            outputFormat.setCaseType(HanyuPinyinCaseType.LOWERCASE);
            outputFormat.setVCharType(HanyuPinyinVCharType.WITH_V);
            //ASCII >=33 ASCII<=125的直接返回 ,ASCII码表：http://www.asciitable.com/
            if(ch>=32 && ch<=125){
                return new String[]{String.valueOf(ch)};
            }
            return ArrayUtils.distinct(PinyinHelper.toHanyuPinyinStringArray(ch, outputFormat));
        } catch (BadHanyuPinyinOutputFormatCombination e) {
            throw new IllegalPinyinException(e);
        }
    }

    @Override
    public String[] getPinyinWithU(char ch) throws IllegalPinyinException {
        try{
            HanyuPinyinOutputFormat outputFormat = new HanyuPinyinOutputFormat();
            outputFormat.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
            outputFormat.setCaseType(HanyuPinyinCaseType.LOWERCASE);
            outputFormat.setVCharType(HanyuPinyinVCharType.WITH_U_UNICODE);
            //ASCII >=33 ASCII<=125的直接返回 ,ASCII码表：http://www.asciitable.com/
            if(ch>=32 && ch<=125){
                return new String[]{String.valueOf(ch)};
            }
            return ArrayUtils.distinct(PinyinHelper.toHanyuPinyinStringArray(ch, outputFormat));
        } catch (BadHanyuPinyinOutputFormatCombination e) {
            throw new IllegalPinyinException(e);
        }
    }

    public String convertInitialToUpperCase(String str) {
        if (str == null || str.length()==0) {
            return "";
        }
        return str.substring(0, 1).toUpperCase()+str.substring(1);
    }

    @Override
    public String[] getPinyinWithToneNumber(char ch) throws IllegalPinyinException {
        try{
            HanyuPinyinOutputFormat outputFormat = new HanyuPinyinOutputFormat();
            outputFormat.setCaseType(HanyuPinyinCaseType.LOWERCASE);
            outputFormat.setToneType(HanyuPinyinToneType.WITH_TONE_NUMBER);
            outputFormat.setVCharType(HanyuPinyinVCharType.WITH_U_AND_COLON);
            //ASCII >=33 ASCII<=125的直接返回 ,ASCII码表：http://www.asciitable.com/
            if(ch>=32 && ch<=125){
                return new String[]{String.valueOf(ch)};
            }
            return ArrayUtils.distinct(PinyinHelper.toHanyuPinyinStringArray(ch, outputFormat));
        } catch (BadHanyuPinyinOutputFormatCombination e) {
            throw new IllegalPinyinException(e);
        }
    }

    @Override
    public String[] getPinyinWithToneNumberV(char ch) throws IllegalPinyinException {
        try{
            HanyuPinyinOutputFormat outputFormat = new HanyuPinyinOutputFormat();
            outputFormat.setCaseType(HanyuPinyinCaseType.LOWERCASE);
            outputFormat.setToneType(HanyuPinyinToneType.WITH_TONE_NUMBER);
            outputFormat.setVCharType(HanyuPinyinVCharType.WITH_V);
            //ASCII >=33 ASCII<=125的直接返回 ,ASCII码表：http://www.asciitable.com/
            if(ch>=32 && ch<=125){
                return new String[]{String.valueOf(ch)};
            }
            return ArrayUtils.distinct(PinyinHelper.toHanyuPinyinStringArray(ch, outputFormat));
        } catch (BadHanyuPinyinOutputFormatCombination e) {
            throw new IllegalPinyinException(e);
        }
    }

    @Override
    public String[] getPinyinWithToneNumberU(char ch) throws IllegalPinyinException {
        try{
            HanyuPinyinOutputFormat outputFormat = new HanyuPinyinOutputFormat();
            outputFormat.setCaseType(HanyuPinyinCaseType.LOWERCASE);
            outputFormat.setToneType(HanyuPinyinToneType.WITH_TONE_NUMBER);
            outputFormat.setVCharType(HanyuPinyinVCharType.WITH_U_UNICODE);
            //ASCII >=33 ASCII<=125的直接返回 ,ASCII码表：http://www.asciitable.com/
            if(ch>=32 && ch<=125){
                return new String[]{String.valueOf(ch)};
            }
            return ArrayUtils.distinct(PinyinHelper.toHanyuPinyinStringArray(ch, outputFormat));
        } catch (BadHanyuPinyinOutputFormatCombination e) {
            throw new IllegalPinyinException(e);
        }
    }

    @Override
    public String[] getPinyinWithToneMark(char ch) throws IllegalPinyinException {
        try{
            HanyuPinyinOutputFormat outputFormat = new HanyuPinyinOutputFormat();
            // 设置声调格式
            /**
             *  HanyuPinyinToneType.WITH_TONE_NUMBER 用数字表示声调，例如：liu2
             HanyuPinyinToneType.WITHOUT_TONE 无声调表示，例如：liu
             HanyuPinyinToneType.WITH_TONE_MARK 用声调符号表示，例如：liú
             */
            outputFormat.setToneType(HanyuPinyinToneType.WITH_TONE_MARK);
            // 设置特殊拼音的显示格式
            /**
             *  HanyuPinyinVCharType.WITH_U_AND_COLON 以U和一个冒号表示该拼音，例如：lu:
             HanyuPinyinVCharType.WITH_V 以V表示该字符，例如：lv
             HanyuPinyinVCharType.WITH_U_UNICODE 以ü表示
             */
            outputFormat.setVCharType(HanyuPinyinVCharType.WITH_U_AND_COLON);
            // 设置大小写
            outputFormat.setCaseType(HanyuPinyinCaseType.LOWERCASE);
            //ASCII >=33 ASCII<=125的直接返回 ,ASCII码表：http://www.asciitable.com/
            if(ch>=32 && ch<=125){
                return new String[]{String.valueOf(ch)};
            }
            return ArrayUtils.distinct(PinyinHelper.toHanyuPinyinStringArray(ch, outputFormat));
        } catch (BadHanyuPinyinOutputFormatCombination e) {
            throw new IllegalPinyinException(e);
        }
    }

    @Override
    public String[] getPinyinWithToneMarkU(char ch) throws IllegalPinyinException {
        try{
            HanyuPinyinOutputFormat outputFormat = new HanyuPinyinOutputFormat();
            // 设置声调格式
            /**
             *  HanyuPinyinToneType.WITH_TONE_NUMBER 用数字表示声调，例如：liu2
             HanyuPinyinToneType.WITHOUT_TONE 无声调表示，例如：liu
             HanyuPinyinToneType.WITH_TONE_MARK 用声调符号表示，例如：liú
             */
            outputFormat.setToneType(HanyuPinyinToneType.WITH_TONE_MARK);
            // 设置特殊拼音的显示格式
            /**
             *  HanyuPinyinVCharType.WITH_U_AND_COLON 以U和一个冒号表示该拼音，例如：lu:
             HanyuPinyinVCharType.WITH_V 以V表示该字符，例如：lv
             HanyuPinyinVCharType.WITH_U_UNICODE 以ü表示
             */
            outputFormat.setVCharType(HanyuPinyinVCharType.WITH_U_UNICODE);
            // 设置大小写
            outputFormat.setCaseType(HanyuPinyinCaseType.LOWERCASE);
            //ASCII >=33 ASCII<=125的直接返回 ,ASCII码表：http://www.asciitable.com/
            if(ch>=32 && ch<=125){
                return new String[]{String.valueOf(ch)};
            }
            return ArrayUtils.distinct(PinyinHelper.toHanyuPinyinStringArray(ch, outputFormat));
        } catch (BadHanyuPinyinOutputFormatCombination e) {
            throw new IllegalPinyinException(e);
        }
    }

    @Override
    public String[] getPinyinWithToneMarkV(char ch) throws IllegalPinyinException {
        try{
            HanyuPinyinOutputFormat outputFormat = new HanyuPinyinOutputFormat();
            // 设置声调格式
            /**
             *  HanyuPinyinToneType.WITH_TONE_NUMBER 用数字表示声调，例如：liu2
             HanyuPinyinToneType.WITHOUT_TONE 无声调表示，例如：liu
             HanyuPinyinToneType.WITH_TONE_MARK 用声调符号表示，例如：liú
             */
            outputFormat.setToneType(HanyuPinyinToneType.WITH_TONE_MARK);
            // 设置特殊拼音的显示格式
            /**
             *  HanyuPinyinVCharType.WITH_U_AND_COLON 以U和一个冒号表示该拼音，例如：lu:
             HanyuPinyinVCharType.WITH_V 以V表示该字符，例如：lv
             HanyuPinyinVCharType.WITH_U_UNICODE 以ü表示
             */
            outputFormat.setVCharType(HanyuPinyinVCharType.WITH_V);
            // 设置大小写
            outputFormat.setCaseType(HanyuPinyinCaseType.LOWERCASE);
            //ASCII >=33 ASCII<=125的直接返回 ,ASCII码表：http://www.asciitable.com/
            if(ch>=32 && ch<=125){
                return new String[]{String.valueOf(ch)};
            }
            return ArrayUtils.distinct(PinyinHelper.toHanyuPinyinStringArray(ch, outputFormat));
        } catch (BadHanyuPinyinOutputFormatCombination e) {
            throw new IllegalPinyinException(e);
        }
    }

    @Override
    public String getPinyin(String chinese,HanyuPinyinToneType hanyuPinyinToneType,HanyuPinyinVCharType hanyuPinyinVCharType) throws IllegalPinyinException {
        if(UtilValidate.isEmpty(chinese)){
            return null;
        }
        chinese = chinese.replaceAll("[\\.，\\,！·\\!？\\?；\\;\\(\\)（）\\[\\]\\:： ]+", " ").trim();
        StringBuilder py_sb = new StringBuilder(32);
        for (int i = 0; i < chinese.length(); i++) {
            char chs = chinese.charAt(i);
            String[] py_arr = null;
            if (HanyuPinyinToneType.WITH_TONE_NUMBER.equals(hanyuPinyinToneType)) {
                if (HanyuPinyinVCharType.WITH_U_AND_COLON.equals(hanyuPinyinVCharType)) {
                    py_arr = getPinyinWithToneNumber(chs);
                }
                if (HanyuPinyinVCharType.WITH_U_UNICODE.equals(hanyuPinyinVCharType)) {
                    py_arr = getPinyinWithToneNumberU(chs);
                }
                if (HanyuPinyinVCharType.WITH_V.equals(hanyuPinyinVCharType)) {
                    py_arr = getPinyinWithToneNumberV(chs);
                }
            }
            if (HanyuPinyinToneType.WITH_TONE_MARK.equals(hanyuPinyinToneType)){
                if (HanyuPinyinVCharType.WITH_U_AND_COLON.equals(hanyuPinyinVCharType)) {
                    py_arr = getPinyinWithToneMark(chs);
                }
                if (HanyuPinyinVCharType.WITH_U_UNICODE.equals(hanyuPinyinVCharType)) {
                    py_arr = getPinyinWithToneMarkU(chs);
                }
                if (HanyuPinyinVCharType.WITH_V.equals(hanyuPinyinVCharType)) {
                    py_arr = getPinyinWithToneMarkV(chs);
                }
            }
            if (HanyuPinyinToneType.WITHOUT_TONE.equals(hanyuPinyinToneType)){
                if (HanyuPinyinVCharType.WITH_V.equals(hanyuPinyinVCharType)){
                    py_arr = getPinyinWithV(chs);
                }
                if (HanyuPinyinVCharType.WITH_U_UNICODE.equals(hanyuPinyinVCharType)) {
                    py_arr = getPinyinWithU(chs);
                }if (HanyuPinyinVCharType.WITH_U_AND_COLON.equals(hanyuPinyinVCharType)) {
                    py_arr = getPinyin(chs);
                }
            }
            if(py_arr==null || py_arr.length<1){
                throw new IllegalPinyinException("pinyin array is empty, char:"+chs+",chinese:"+chinese);
            }
            if(py_arr.length==1){
                py_sb.append(convertInitialToUpperCase(py_arr[0]));
            }else if(py_arr.length==2 && py_arr[0].equals(py_arr[1])){
                py_sb.append(convertInitialToUpperCase(py_arr[0]));
            }else{
                String resultPy = null, defaultPy = null;
                for (String py : py_arr) {
                    List<String> duoYinZiList = duoYinZiMap.get(py);
                    //向左多取一个字,例如 银[行]
                    String left = null;
                    if(i>=1 && i+1<=chinese.length()){
                        left = chinese.substring(i-1,i+1);
                        if(duoYinZiMap.containsKey(py) && duoYinZiList.contains(left)){
                            resultPy = py;
                            break;
                        }
                    }
                    //向右多取一个字,例如 [长]沙
                    String right = null;
                    if(i<=chinese.length()-2){
                        right = chinese.substring(i,i+2);
                        if(duoYinZiMap.containsKey(py) && duoYinZiList.contains(right)){
                            resultPy = py;
                            break;
                        }
                    }
                    //左右各多取一个字,例如 龙[爪]槐
                    String middle = null;
                    if(i>=1 && i+2<=chinese.length()){
                        middle = chinese.substring(i-1,i+2);
                        if(duoYinZiMap.containsKey(py) && duoYinZiList.contains(middle)){
                            resultPy = py;
                            break;
                        }
                    }
                    //向左多取2个字,如 芈月[传],列车长
                    String left3 = null;
                    if(i>=2 && i+1<=chinese.length()){
                        left3 = chinese.substring(i-2,i+1);
                        if(duoYinZiMap.containsKey(py) && duoYinZiList.contains(left3)){
                            resultPy = py;
                            break;
                        }
                    }
                    //向右多取2个字,如 [长]孙无忌
                    String right3 = null;
                    if(i<=chinese.length()-3){
                        right3 = chinese.substring(i,i+3);
                        if(duoYinZiMap.containsKey(py) && duoYinZiList.contains(right3)){
                            resultPy = py;
                            break;
                        }
                    }
                    //左取一个右取两个字,例如 大[汗]淋漓
                    String left1Right2 = null;
                    if(i>=1 && i+3<=chinese.length()){
                        left1Right2 = chinese.substring(i-1,i+3);
                        if(duoYinZiMap.containsKey(py) && duoYinZiList.contains(left1Right2)){
                            resultPy = py;
                            break;
                        }
                    }
                    //默认拼音
                    if(duoYinZiMap.containsKey(py) && duoYinZiMap.get(py).contains(String.valueOf(chs))){
                        defaultPy = py;
                    }
                }
                if(UtilValidate.isEmpty(resultPy)){
                    if(UtilValidate.isNotEmpty(defaultPy)){
                        resultPy = defaultPy;
                    }else{
                        resultPy = py_arr[0];
                    }
                }
                py_sb.append(convertInitialToUpperCase(resultPy));
            }
        }
        return py_sb.toString();
    }
}
