package com.futool.pinyin.conver;

import com.futool.pinyin.exception.IllegalPinyinException;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.HanyuPinyinVCharType;

/**
* 
* @className Converter
* @author liuc
* @date 2019-12-21 22:18
* @since JDK 1.8
**/
public interface Converter {

    public String[] getPinyin(char ch) throws IllegalPinyinException;

    public String[] getPinyinWithV(char ch) throws IllegalPinyinException;

    public String[] getPinyinWithU(char ch) throws IllegalPinyinException;

    public String[] getPinyinWithToneNumber(char ch) throws IllegalPinyinException;

    public String[] getPinyinWithToneNumberV(char ch) throws IllegalPinyinException;

    public String[] getPinyinWithToneNumberU(char ch) throws IllegalPinyinException;

    public String[] getPinyinWithToneMark(char ch) throws IllegalPinyinException;

    public String[] getPinyinWithToneMarkV(char ch) throws IllegalPinyinException;

    public String[] getPinyinWithToneMarkU(char ch) throws IllegalPinyinException;

    public String getPinyin(String chinese, HanyuPinyinToneType hanyuPinyinToneType, HanyuPinyinVCharType hanyuPinyinVCharType) throws IllegalPinyinException;

}
