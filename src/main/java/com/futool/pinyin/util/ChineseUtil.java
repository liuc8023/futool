package com.futool.pinyin.util;

import com.futool.StringUtils;
import com.futool.pinyin.conver.PinyinConverter;
import com.futool.pinyin.exception.IllegalPinyinException;
import com.futool.pinyin.pinyin4j.PinyinHelper;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.HanyuPinyinVCharType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;
import java.util.List;

/**
* 中文工具:汉字转拼音
* @className ChineseUtil
* @author liuc
* @date 2019-12-21 22:39
* @since JDK 1.8
**/
@Slf4j
public class ChineseUtil {
    static PinyinConverter pinyinConverter = null;
    private static final String SEPARATE = "#";
    static {
        pinyinConverter = new PinyinConverter();
    }

    /**
     * 将中文字符串转换成首字母大写的拼音字符串
     * @param
     * @return
     * @throws
     * @date 2019/12/21 23:00
     * @author liuc
     **/
    public static String getPinyinStringV(String chinese) throws IllegalPinyinException {
        StringBuffer sBuff = new StringBuffer();
        // 如果字符是中文,则将中文转为汉语拼音
        if (StringUtils.checkNameCheseNoSpeSymbol(chinese)) {
            sBuff.append(pinyinConverter.getPinyin(chinese,HanyuPinyinToneType.WITHOUT_TONE,HanyuPinyinVCharType.WITH_V));
        }  else {
            // 否则不转换
        }
        return sBuff.toString();
    }

    /**
     * 将中文字符串转换成首字母大写的拼音字符串
     * @param
     * @return
     * @throws
     * @date 2019/12/21 23:00
     * @author liuc
     **/
    public static String getPinyinString(String chinese) throws IllegalPinyinException {
        StringBuffer sBuff = new StringBuffer();
        // 如果字符是中文,则将中文转为汉语拼音
        if (StringUtils.checkNameCheseNoSpeSymbol(chinese)) {
            sBuff.append(pinyinConverter.getPinyin(chinese,HanyuPinyinToneType.WITHOUT_TONE,HanyuPinyinVCharType.WITH_U_UNICODE));
        }  else {
            // 否则不转换
        }
        return sBuff.toString();
    }

    /**
     * 将中文字符串转换成首字母大写且带有数字声调的拼音字符串
     * @param chinese
     * @return
     * @throws
     * @date 2019/12/21 23:00
     * @author liuc
     **/
    public static String getPinyinStringWithToneNumberV(String chinese) throws IllegalPinyinException {
        StringBuffer sBuff = new StringBuffer();
        // 如果字符是中文,则将中文转为汉语拼音
        if (StringUtils.checkNameCheseNoSpeSymbol(chinese)) {
            sBuff.append(pinyinConverter.getPinyin(chinese, HanyuPinyinToneType.WITH_TONE_NUMBER, HanyuPinyinVCharType.WITH_V));
        }  else {
            // 否则不转换
        }
        return sBuff.toString();
    }

    /**
     * 将中文字符串转换成首字母大写且带有数字声调的拼音字符串
     * @param chinese
     * @return
     * @throws
     * @date 2019/12/21 23:00
     * @author liuc
     **/
    public static String getPinyinStringWithToneNumber(String chinese) throws IllegalPinyinException {
        StringBuffer sBuff = new StringBuffer();
        // 如果字符是中文,则将中文转为汉语拼音
        if (StringUtils.checkNameCheseNoSpeSymbol(chinese)) {
            sBuff.append(pinyinConverter.getPinyin(chinese, HanyuPinyinToneType.WITH_TONE_NUMBER, HanyuPinyinVCharType.WITH_U_AND_COLON));
        }  else {
            // 否则不转换
        }
        return sBuff.toString();
    }

    /**
     * 将中文字符串转换成首字母大写且带有数字声调的拼音字符串
     * @param chinese
     * @return
     * @throws
     * @date 2019/12/21 23:00
     * @author liuc
     **/
    public static String getPinyinStringWithToneNumberU(String chinese) throws IllegalPinyinException {
        StringBuffer sBuff = new StringBuffer();
        // 如果字符是中文,则将中文转为汉语拼音
        if (StringUtils.checkNameCheseNoSpeSymbol(chinese)) {
            sBuff.append(pinyinConverter.getPinyin(chinese, HanyuPinyinToneType.WITH_TONE_NUMBER, HanyuPinyinVCharType.WITH_U_UNICODE));
        }  else {
            // 否则不转换
        }
        return sBuff.toString();
    }

    /**
     *
     * @param chinese
     * @return
     * @throws
     * @date 2019/12/21 23:00
     * @author liuc
     **/
    public static String getPinyinStringWithToneMark(String chinese) throws IllegalPinyinException {
        StringBuffer sBuff = new StringBuffer();
        // 如果字符是中文,则将中文转为汉语拼音
        if (StringUtils.checkNameCheseNoSpeSymbol(chinese)) {
            sBuff.append(pinyinConverter.getPinyin(chinese, HanyuPinyinToneType.WITH_TONE_MARK, HanyuPinyinVCharType.WITH_U_UNICODE));
        }  else {
            // 否则不转换
        }
        return sBuff.toString();
    }

    /**
     *
     * @param chinese
     * @return
     * @throws
     * @date 2019/12/23 15:01
     * @author liuc
     **/
//    public static String getPinyinWithToneMark(String chinese) {
//        StringBuffer sbBuff = new StringBuffer();
//        // 如果字符是中文,则将中文转为汉语拼音
//        if (StringUtils.checkNameCheseNoSpeSymbol(chinese)) {
//            List<Pinyin> pinyinList = HanLP.convertToPinyinList(chinese);
//            for (Pinyin pinyin : pinyinList) {
//                sbBuff.append(pinyinConverter.convertInitialToUpperCase(pinyin.getPinyinWithToneMark()));
//            }
//        }  else {
//            // 否则不转换
//        }
//        return sbBuff.toString();
//    }

    /**
     * 将中文字符串转换成首字母大写且带有数字声调的拼音字符串
     * @param chinese
     * @return
     * @throws
     * @date 2019/12/21 23:00
     * @author liuc
     **/
    public static String getPinyinStringWithToneMarkU(String chinese) throws IllegalPinyinException {
        StringBuffer sBuff = new StringBuffer();
        // 如果字符是中文,则将中文转为汉语拼音
        if (StringUtils.checkNameCheseNoSpeSymbol(chinese)) {
            sBuff.append(pinyinConverter.getPinyin(chinese, HanyuPinyinToneType.WITH_TONE_MARK,HanyuPinyinVCharType.WITH_U_UNICODE));
        }  else {
            // 否则不转换
        }
        return sBuff.toString();
    }

    /**
     * 拼音小写输出
     * @param
     * @return
     * @throws
     * @date 2019/12/21 23:00
     * @author liuc
     **/
    public static String getPinyinToLowerCase(String chinese) throws IllegalPinyinException {
        StringBuffer sBuff = new StringBuffer();
        // 如果字符是中文,则将中文转为汉语拼音
        if (StringUtils.checkNameCheseNoSpeSymbol(chinese)) {
            sBuff.append(getPinyinString(chinese).toLowerCase());
        } else {
            // 否则不转换
        }
        return sBuff.toString();
    }

    /**
     * 获取每个拼音的简称
     * @param
     * @return
     * @throws
     * @date 2019/12/21 23:00
     * @author liuc
     **/
    public static String getPinyinConvertJianPin(String chinese) {
        StringBuffer sBuff = new StringBuffer();
        if (StringUtils.checkNameCheseNoSpeSymbol(chinese)) {
            char[] arr = null;
            try {
                // 将字符串转化成char型数组
                arr = getPinyinString(chinese).toCharArray();
                for (int i = 0; i < arr.length; i++) {
                    // 判断是否是大写字母
                    if (arr[i] >= 65 && arr[i] < 91) {
                        sBuff.append(arr[i]).append("");
                    }
                }
            } catch (IllegalPinyinException e) {
                log.error("汉字转拼音失败");
            }
        } else {
            // 否则不转换
        }
        return sBuff.toString();
    }

    /**
     * 拼音大写输出
     * @param
     * @return
     * @throws
     * @date 2019/12/21 23:00
     * @author liuc
     **/
    public static String getPinyinToUpperCase(String chinese) throws IllegalPinyinException {
        StringBuffer sBuff = new StringBuffer();
        // 如果字符是中文,则将中文转为汉语拼音
        if (StringUtils.checkNameCheseNoSpeSymbol(chinese)) {
            sBuff.append(getPinyinString(chinese).toUpperCase());
        } else {
            // 否则不转换
        }
        return sBuff.toString();
    }

    /**
     * 获取文本的拼音
     *
     * @param str     需要转换拼音的文本
     * @param retain  true：保留中文以外的其他字符
     * @param initial true：只需要首字母
     * @return 拼音
     */
    public static String toPinYinString(String str, boolean retain, boolean initial) {
        StringBuilder sb = new StringBuilder();
        try {
            List<String> list = Lists.newArrayList();
            StringBuilder notChinese = new StringBuilder();
            for (int i = 0; i < str.length(); i++) {
                if (str.charAt(i) < 0x4E00 || str.charAt(i) > 0x9FA5) {
                    notChinese.append(str.charAt(i));
                    if (i == str.length() - 1) {
                        list.add(notChinese.toString());
                    }
                } else {
                    if (notChinese.length() > 0) {
                        list.add(notChinese.toString());
                        notChinese = new StringBuilder();
                    }
                }
            }
            HanyuPinyinOutputFormat outputFormat = new HanyuPinyinOutputFormat();
            // 设置声调格式
            /**
             *  HanyuPinyinToneType.WITH_TONE_NUMBER 用数字表示声调，例如：liu2
             HanyuPinyinToneType.WITHOUT_TONE 无声调表示，例如：liu
             HanyuPinyinToneType.WITH_TONE_MARK 用声调符号表示，例如：liú
             */
            outputFormat.setToneType(HanyuPinyinToneType.WITH_TONE_MARK);
            // 设置特殊拼音的显示格式
            /**
             *  HanyuPinyinVCharType.WITH_U_AND_COLON 以U和一个冒号表示该拼音，例如：lu:
             HanyuPinyinVCharType.WITH_V 以V表示该字符，例如：lv
             HanyuPinyinVCharType.WITH_U_UNICODE 以ü表示
             */
            outputFormat.setVCharType(HanyuPinyinVCharType.WITH_U_UNICODE);
            // 设置大小写
            outputFormat.setCaseType(HanyuPinyinCaseType.LOWERCASE);
            String pinyin = PinyinHelper.toHanYuPinyinString(str, outputFormat, SEPARATE, retain);
            System.out.println(pinyin);
            Splitter.on(SEPARATE).split(pinyin).forEach(py -> {
                if (list.contains(py)) {
                    sb.append(py);
                    return;
                }
                if (initial) {
                    sb.append(py.charAt(0));
                } else {
                    sb.append(py);
                }
            });
        } catch (BadHanyuPinyinOutputFormatCombination e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

}
