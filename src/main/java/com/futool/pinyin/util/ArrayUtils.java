package com.futool.pinyin.util;

import java.util.HashSet;

/**
* 
* @className ArrayUtils
* @author liuc
* @date 2019-12-21 22:16
* @since JDK 1.8
**/
public class ArrayUtils {

    private ArrayUtils() {}

    public static String[] distinct(String[] arr) {
        if(arr==null || arr.length==0) {
            return arr;
        }
        HashSet<String> set = new HashSet<>();
        for (String str : arr) {
            set.add(str);
        }
        return set.toArray(new String[0]);
    }
}