package com.futool.pinyin.exception;
/**
* 
* @className IllegalPinyinException
* @author liuc
* @date 2019-12-21 22:12
* @since JDK 1.8
**/
public class IllegalPinyinException extends Exception {

    private static final long serialVersionUID = 4447260855879734366L;

    public IllegalPinyinException() {
    }

    public IllegalPinyinException(String message) {
        super(message);
    }

    public IllegalPinyinException(String message, Throwable cause) {
        super(message, cause);
    }

    public IllegalPinyinException(Throwable cause) {
        super(cause);
    }
}
