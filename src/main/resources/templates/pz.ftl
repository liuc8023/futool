<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Title</title>
    <style>
        @page {
            size: 279mm 216mm;
        }

        table.pzjbt {
            border-collapse:collapse;
            border-color:#0094b5;
            border-style:solid;
            border-width:1px;
            margin-bottom:3px;
            margin-top:3px;
            text-align: center;
        }
        td.pzjbt {
            border-left-width:1px;
            border-left-style:solid;
            border-left-color:#000000;
            border-top-width:1px;
            border-top-style:solid;
            border-top-color:#000000;
            border-right-width:1px;
            border-right-style:solid;
            border-right-color:#000000;
            border-bottom-width:1px;
            border-bottom-style:solid;
            border-bottom-color:#000000;
            padding-top:1px;
            padding-bottom:1px;
            padding-left:1px;
            padding-right:1px;
            vertical-align:top;
            text-align: center;
        }
        p.pzdl {
            color:#000000;
            font-family:NSimSun;
            font-size:12px;
            font-style:normal;
            font-variant:normal;
            font-weight:300;
            line-height:1.2;
            margin-bottom:0;
            margin-left:0;
            margin-right:0;
            margin-top:0;
            orphans:1;
            page-break-after:auto;
            page-break-before:auto;
            text-align:center;
            text-align-last:center;
            text-decoration:none;
            text-indent:0;
            text-transform:none;
            widows:1;
        }
        td.CellOverride-1 {
            border-bottom-color:#199fbc;
            border-bottom-style:solid;
            border-bottom-width:1px;
            border-left-color:#0094b5;
            border-left-style:solid;
            border-left-width:1px;
            border-right-color:#0094b5;
            border-right-style:solid;
            border-right-width:1px;
            border-top-color:#0094b5;
            border-top-style:solid;
            border-top-width:2px;
            vertical-align:middle;
        }
        td.CellOverride-2 {
            border-bottom-color:#0094b5;
            border-bottom-style:solid;
            border-bottom-width:1px;
            border-left-color:#199fbc;
            border-left-style:solid;
            border-left-width:1px;
            border-right-color:#191919;
            border-right-style:solid;
            border-right-width:0px;
            border-top-color:#199fbc;
            border-top-style:solid;
            border-top-width:1px;
            vertical-align:middle;
        }
        td.CellOverride-3 {
            border-bottom-color:#0094b5;
            border-bottom-style:solid;
            border-bottom-width:1px;
            border-left-color:#191919;
            border-left-style:solid;
            border-left-width:0px;
            border-right-color:#0094b5;
            border-right-style:solid;
            border-right-width:1px;
            border-top-color:#0094b5;
            border-top-style:solid;
            border-top-width:1px;
            vertical-align:middle;
        }
        td.CellOverride-4 {
            border-bottom-color:#0094b5;
            border-bottom-style:solid;
            border-bottom-width:1px;
            border-left-color:#199fbc;
            border-left-style:solid;
            border-left-width:1px;
            border-right-color:#191919;
            border-right-style:solid;
            border-right-width:0px;
            border-top-color:#0094b5;
            border-top-style:solid;
            border-top-width:1px;
            vertical-align:middle;
        }
        td.CellOverride-5 {
            border-bottom-color:#199fbc;
            border-bottom-style:solid;
            border-bottom-width:1px;
            border-left-color:#199fbc;
            border-left-style:solid;
            border-left-width:1px;
            border-right-color:#191919;
            border-right-style:solid;
            border-right-width:0px;
            border-top-color:#0094b5;
            border-top-style:solid;
            border-top-width:1px;
            vertical-align:middle;
        }
        td.CellOverride-6 {
            background-color:#0094b5;
            border-bottom-style:solid;
            border-bottom-width:0px;
            border-left-style:solid;
            border-left-width:0px;
            border-right-color:#0094b5;
            border-right-style:solid;
            border-right-width:1px;
            border-top-color:#199fbc;
            border-top-style:solid;
            border-top-width:1px;
            vertical-align:middle;
        }
        td.CellOverride-7 {
            background-color:#0094b5;
            border-bottom-style:solid;
            border-bottom-width:0px;
            border-left-color:#0094b5;
            border-left-style:solid;
            border-left-width:1px;
            border-right-color:#0094b5;
            border-right-style:solid;
            border-right-width:1px;
            border-top-color:#199fbc;
            border-top-style:solid;
            border-top-width:1px;
            vertical-align:middle;
        }
        td.CellOverride-8 {
            background-color:#0094b5;
            border-bottom-style:solid;
            border-bottom-width:0px;
            border-left-style:solid;
            border-left-width:0px;
            border-right-style:solid;
            border-right-width:0px;
            border-top-color:#0094b5;
            border-top-style:solid;
            border-top-width:1px;
            vertical-align:middle;
        }
        p.ParaOverride-1 {
            text-align:center;
            text-align-last:center;
        }
        p.ParaOverride-2 {
            text-align:left;
        }
        span.CharOverride-1 {
            color:#191919;
            font-family:NSimSun;
            font-style:normal;
            font-weight:normal;
        }
        span.CharOverride-2 {
            font-family:NSimSun;
            font-size:9px;
            font-style:normal;
            font-weight:normal;
        }
        span.CharOverride-3 {
            color:#f5760a;
            font-family:NSimSun;
            font-size:9px;
            font-style:normal;
            font-weight:300;
        }
        span.CharOverride-4 {
            font-family:NSimSun;
            font-style:normal;
            font-weight:300;
        }
        td._idGenCellOverride-1 {
            border-right-style:solid;
            border-right-width:0px;
        }
        col._idGenTableRowColumn-1 {
            width:81px;
        }
        col._idGenTableRowColumn-2 {
            width:35px;
        }
        col._idGenTableRowColumn-3 {
            width:45px;
        }
        tr._idGenTableRowColumn-4 {
            min-height:26px;
        }
        tr._idGenTableRowColumn-5 {
            min-height:23px;
        }
        tr._idGenTableRowColumn-6 {
            min-height:31px;
        }
        img._idGenObjectAttribute-1 {
            height:25px;
            width:57px;
        }
        img._idGenObjectAttribute-2 {
            height:20px;
            width:46px;
        }
    </style>

</head>
<body>

<div id="_idContainer002" style="width: 1000px;">
    <table id="table001" class="pzjbt" style="width: 100%;margin-left: 20%;">
        <colgroup>
            <col class="_idGenTableRowColumn-1" />
            <col class="_idGenTableRowColumn-2" />
            <col class="_idGenTableRowColumn-3" />
            <col class="_idGenTableRowColumn-1" />
            <col class="_idGenTableRowColumn-1" />
        </colgroup>

        <tr class="pzjbt _idGenTableRowColumn-4">
            <td class="pzjbt CellOverride-1" colspan="5">
                <p class="pzdl ParaOverride-1"><span class="CharOverride-1">收  据</span></p>
            </td>
        </tr>
        <tr class="pzjbt _idGenTableRowColumn-5">
            <td class="pzjbt CellOverride-2" colspan="2">
                <p class="pzdl ParaOverride-1"><span class="CharOverride-2">日期</span></p>
            </td>
            <td class="pzjbt CellOverride-3" colspan="3">
                <p class="pzdl ParaOverride-1"><span class="CharOverride-3">${(createTime)!}</span></p>
            </td>
        </tr>
        <tr class="pzjbt _idGenTableRowColumn-5">
            <td class="pzjbt CellOverride-4" colspan="2">
                <p class="pzdl ParaOverride-1"><span class="CharOverride-2">交易编号</span></p>
            </td>
            <td class="pzjbt CellOverride-3" colspan="3">
                <p class="pzdl ParaOverride-1"><span class="CharOverride-3">${orderCode!''}</span></p>
            </td>
        </tr>
        <tr class="pzjbt _idGenTableRowColumn-5">
            <td class="pzjbt CellOverride-4" colspan="2">
                <p class="pzdl ParaOverride-1"><span class="CharOverride-2">交易类型</span></p>
            </td>
            <td class="pzjbt CellOverride-3" colspan="3">
                <p class="pzdl ParaOverride-1"><span class="CharOverride-3">捐赠</span></p>
            </td>
        </tr>
        <tr class="pzjbt _idGenTableRowColumn-5">
            <td class="pzjbt CellOverride-4" colspan="2">
                <p class="pzdl ParaOverride-1"><span class="CharOverride-2">交易金额</span></p>
            </td>
            <td class="pzjbt CellOverride-3" colspan="3">
                <p class="pzdl ParaOverride-1"><span class="CharOverride-3">${productPrice!0}</span></p>
            </td>
        </tr>
        <tr class="pzjbt _idGenTableRowColumn-5">
            <td class="pzjbt CellOverride-5" colspan="2">
                <p class="pzdl ParaOverride-1"><span class="CharOverride-2">付款人</span></p>
            </td>
            <td class="pzjbt CellOverride-3" colspan="3">
                <p class="pzdl ParaOverride-1"><span class="CharOverride-3">${userName!''}</span></p>
            </td>
        </tr>

    </table>
</div>

</body>
</html>