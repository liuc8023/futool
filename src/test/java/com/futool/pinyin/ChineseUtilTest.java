package com.futool.pinyin;

import com.futool.pinyin.util.ChineseUtil;
import com.hankcs.hanlp.HanLP;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

public class ChineseUtilTest {

    @SneakyThrows
    @Test
    void chineseUtilTest(){
        System.out.println("-----:"+ ChineseUtil.getPinyinStringWithToneMark("女"));
        String s = "抖颤";
        System.out.println("-----:"+ChineseUtil.getPinyinStringWithToneMarkU(s));
        System.out.println(ChineseUtil.getPinyinString(s));
        System.out.println("-----:"+ChineseUtil.getPinyinStringWithToneMark(s));
        System.out.println("一行人:"+ChineseUtil.getPinyinString("一行人"));
        System.out.println("一行:"+ChineseUtil.getPinyinString("一行"));
        System.out.println("都是:"+ChineseUtil.getPinyinString("都是"));
        System.out.println("首都:"+ChineseUtil.getPinyinString("首都"));
        System.out.println("降落:"+ChineseUtil.getPinyinString("降落"));
        System.out.println("降落:"+ChineseUtil.getPinyinStringWithToneNumber("降落"));
        System.out.println("投降:"+ChineseUtil.getPinyinStringWithToneNumber("投降"));
        System.out.println("投降大大落落落下:"+ChineseUtil.getPinyinString("投降大大落落落下"));
        System.out.println("降服:"+ChineseUtil.getPinyinString("降服"));
        System.out.println("阿房宫:"+ChineseUtil.getPinyinString("阿房宫"));
        //龘(dá)靐(bìng)龗(líng)齾(yà)齫(yǔn)爩(yù)虌(biē)黭(yǎn)滟(yàn)韊(lán)
        System.out.println("龘靐龗齾齫爩虌黭灩韊:"+ChineseUtil.getPinyinString("龘靐龗齾齫爩虌黭灩韊"));
        System.out.println("长大:"+ChineseUtil.getPinyinString("长大"));
        System.out.println("长大茜茜:"+ChineseUtil.getPinyinToLowerCase("长大茜茜"));
        System.out.println("长大茜國:"+ChineseUtil.getPinyinToUpperCase("长大茜國"));
        System.out.println("犄角旮旯龙行龘龘:"+ChineseUtil.getPinyinString("犄角旮旯龙行龘龘"));
        System.out.println("范闲:"+ChineseUtil.getPinyinConvertJianPin("范闲"));
        System.out.println("范闲:"+ChineseUtil.getPinyinStringWithToneNumber("范闲"));
        //龘(dá)靐(bìng)龗(líng)齾(yà)齫(yǔn)爩(yù)虌(biē)黭(yǎn)滟(yàn)韊(lán)
        System.out.println("龘靐龗齾齫爩虌黭灩韊:"+ChineseUtil.getPinyinStringWithToneNumber("䶮女龘靐龗齾齫爩虌黭灩韊"));
        System.out.println("龑:"+ChineseUtil.getPinyinStringWithToneNumber("龑"));
        System.out.println("䶮:"+ChineseUtil.getPinyinStringWithToneNumber("䶮"));
        System.out.println("席卷八荒:"+ChineseUtil.getPinyinStringWithToneMark("席卷八荒"));
        System.out.println("慌张:"+ChineseUtil.getPinyinStringWithToneMark("慌张"));
        System.out.println("精神恍惚:"+ChineseUtil.getPinyinStringWithToneMark("精神恍惚"));
        System.out.println("可汗大汗大汗淋漓重载不是重担:"+ChineseUtil.getPinyinStringWithToneNumber("可汗大汗大汗淋漓重载不是重担"));
        System.out.println("可汗大汗大汗淋漓重载不是重担:"+ChineseUtil.getPinyinString("可汗大汗大汗淋漓重载不是重担"));
    }
}
