package com.futool;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Date;

class DateUtilTest {

    @SneakyThrows
    @Test
    void convertObjToTimestamp() {
        System.out.println(DateUtil.convertObjToTimestamp("20211116"));
        System.out.println(DateUtil.convertObjToTimestamp("2021-11-17"));
        System.out.println(DateUtil.convertObjToTimestamp("2021-11-17 20:21:36"));
        System.out.println(DateUtil.convertObjToTimestamp("2021-11-17 20:23"));
        System.out.println(DateUtil.convertObjToTimestamp("20211117 202140"));
        System.out.println(DateUtil.convertObjToTimestamp("2021.11.18"));
        System.out.println(DateUtil.convertObjToTimestamp("2021/11/19"));
        System.out.println(DateUtil.convertObjToTimestamp("20211105192430143"));
        System.out.println(DateUtil.convertObjToTimestamp("2021-11-05 19:24:30:143"));
        Date date = new Date();
        System.out.println(DateUtil.convertObjToTimestamp(date));
        LocalDateTime ldt = LocalDateTime.now();
        System.out.println(DateUtil.convertObjToTimestamp(ldt));
        LocalDate ld = LocalDate.now();
        System.out.println(DateUtil.convertObjToTimestamp(ld));
        java.sql.Date sqlDate = new java.sql.Date(date.getTime());
        System.out.println(DateUtil.convertObjToTimestamp(sqlDate));
        System.out.println("==========================================");
        System.out.println(DateUtil.convertObjToSqlDate("20211116"));
        System.out.println(DateUtil.convertObjToSqlDate("2021-11-17"));
        System.out.println(DateUtil.convertObjToSqlDate("2021-11-17 20:21:36"));
        System.out.println(DateUtil.convertObjToSqlDate("2021-11-17 20:23"));
        System.out.println(DateUtil.convertObjToSqlDate("20211117 202140"));
        System.out.println(DateUtil.convertObjToSqlDate("2021.11.18"));
        System.out.println(DateUtil.convertObjToSqlDate("2021/11/19"));
        System.out.println(DateUtil.convertObjToSqlDate("20211105192430143"));
        System.out.println(DateUtil.convertObjToSqlDate("2021-11-05 19:24:30:143"));
        System.out.println(DateUtil.convertObjToSqlDate(date));
        System.out.println(DateUtil.convertObjToSqlDate(ldt));
        System.out.println(DateUtil.convertObjToSqlDate(ld));
        System.out.println(DateUtil.convertObjToSqlDate(sqlDate));
        System.out.println("==========================================");
        System.out.println(DateUtil.convertObjToUtilDate("2023-01-31 09:37:25:6666605"));
        System.out.println(DateUtil.covertObjToString("2023-01-31 09:37:25:6666605"));
        System.out.println("20211116:"+DateUtil.convertObjToUtilDate("20211116"));
        System.out.println(DateUtil.convertObjToUtilDate("2021-11-17"));
        System.out.println(DateUtil.convertObjToUtilDate("2021-11-17 20:21:36"));
        System.out.println(DateUtil.convertObjToUtilDate("2021-11-17 20:23"));
        System.out.println(DateUtil.convertObjToUtilDate("20211117 202140"));
        System.out.println(DateUtil.convertObjToUtilDate("2021.11.18"));
        System.out.println(DateUtil.convertObjToUtilDate("2021/11/19"));
        System.out.println(DateUtil.convertObjToUtilDate("20211105192430143"));
        System.out.println(DateUtil.convertObjToUtilDate("2021-11-05 19:24:30:143"));
        System.out.println(DateUtil.convertObjToUtilDate(date));
        System.out.println(DateUtil.convertObjToUtilDate(ldt));
        System.out.println(DateUtil.convertObjToUtilDate(ld));
        System.out.println(DateUtil.convertObjToUtilDate(sqlDate));
        System.out.println("==========================================");
        System.out.println(DateUtil.convertObjToLdt("20211116"));
        System.out.println(DateUtil.convertObjToLdt("2021-11-17"));
        System.out.println(DateUtil.convertObjToLdt("2021-11-17 20:21:36"));
        System.out.println(DateUtil.convertObjToLdt("2021-11-17 20:23"));
        System.out.println(DateUtil.convertObjToLdt("20211117 202140"));
        System.out.println(DateUtil.convertObjToLdt("2021.11.18"));
        System.out.println(DateUtil.convertObjToLdt("2021/11/19"));
        System.out.println(DateUtil.convertObjToLdt("20211105192430143"));
        System.out.println(DateUtil.convertObjToLdt("2021-11-05 19:24:30:143"));
        System.out.println(DateUtil.convertObjToLdt(date));
        System.out.println(DateUtil.convertObjToLdt(ldt));
        System.out.println(DateUtil.convertObjToLdt(ld));
        System.out.println(DateUtil.convertObjToLdt(sqlDate));
        System.out.println("==========================================");
        System.out.println(DateUtil.convertObjToLd("20211116"));
        System.out.println(DateUtil.convertObjToLd("2021-11-17"));
        System.out.println(DateUtil.convertObjToLd("2021-11-17 20:21:36"));
        System.out.println(DateUtil.convertObjToLd("2021-11-17 20:23"));
        System.out.println(DateUtil.convertObjToLd("20211117 202140"));
        System.out.println(DateUtil.convertObjToLd("2021.11.18"));
        System.out.println(DateUtil.convertObjToLd("2021/11/19"));
        System.out.println(DateUtil.convertObjToLd("20211105192430143"));
        System.out.println(DateUtil.convertObjToLd("2021-11-05 19:24:30:143"));
        System.out.println(DateUtil.convertObjToLd(date));
        System.out.println(DateUtil.convertObjToLd(ldt));
        System.out.println(DateUtil.convertObjToLd(ld));
        System.out.println(DateUtil.convertObjToLd(sqlDate));
        System.out.println("==========================================");
        System.out.println(DateUtil.covertObjToString("2021-11-17 20:21:36.0"));
        System.out.println(DateUtil.covertObjToString("20211116"));
        System.out.println(DateUtil.covertObjToString("2021-11-17"));
        System.out.println(DateUtil.covertObjToString("2021-11-17 20:21:36"));
        System.out.println(DateUtil.covertObjToString("2021-11-17 20:23"));
        System.out.println(DateUtil.covertObjToString("20211117 202140"));
        System.out.println(DateUtil.covertObjToString("2021.11.18"));
        System.out.println(DateUtil.covertObjToString("2021/11/19"));
        System.out.println(DateUtil.covertObjToString("20211105192430143"));
        System.out.println(DateUtil.covertObjToString("2021-11-05 19:24:30:143"));
        System.out.println(DateUtil.covertObjToString("2023-04-07",DateUtil.DATE_SHORT_TIME_PATTERN_3));
        System.out.println(DateUtil.covertObjToString(date));
        System.out.println(DateUtil.covertObjToString(ldt));
        System.out.println(DateUtil.covertObjToString(ld));
        System.out.println(DateUtil.covertObjToString(sqlDate));
        System.out.println("==========================================");
        System.out.println(DateUtil.covertObjToString("20211116",DateUtil.DATE_FORMATTER));
        System.out.println(DateUtil.covertObjToString("2021-11-17",DateUtil.DATE_POINT_PATTERN));
        System.out.println(DateUtil.covertObjToString("2021-11-17 20:21:36",DateUtil.DATE_PATTERN_2));
        System.out.println(DateUtil.covertObjToString("2021-11-17 20:23",DateUtil.DATE_FORMAT_COMPACT));
        System.out.println(DateUtil.covertObjToString("20211117 202140",DateUtil.DATE_FORMAT_COMPACTFULL));
        System.out.println("Long to String "+DateUtil.covertObjToString(1623859200000L));
        System.out.println(DateUtil.covertObjToString("2021/11/19"));
        System.out.println(DateUtil.covertObjToString("20211105192430143"));
        System.out.println(DateUtil.covertObjToString("2021-11-05 19:24:30:143"));
        System.out.println(DateUtil.covertObjToString(date));
        System.out.println(DateUtil.covertObjToString(ldt));
        System.out.println(DateUtil.covertObjToString(ld));
        System.out.println(DateUtil.covertObjToString(sqlDate));
        System.out.println(DateUtil.covertObjToString("2021-11-05 19:24:30:143",DateUtil.DATE_FORMATTER));
        System.out.println(DateUtil.covertObjToString("2021-11-05 19:24:30:143",DateUtil.DATE_SHORT_TIME_PATTERN));
        System.out.println(DateUtil.covertObjToString("2021-11-05 19:24:30:143",DateUtil.DATETIME_FORMATTER));
        System.out.println(DateUtil.covertObjToString(DateUtil.convertObjToTimestamp("20211116"),DateUtil.DATETIME_FORMATTER));
        System.out.println(DateUtil.covertObjToString(DateUtil.convertObjToTimestamp("2021-11-17 20:21:36"),DateUtil.DATETIME_FORMATTER));
        System.out.println("字符串时间戳:"+DateUtil.covertObjToString("1638257256794",DateUtil.DATE_FORMAT_FULL_MSE));
        System.out.println("字符串时间戳:"+DateUtil.covertObjToString("1862019271",DateUtil.DATE_FORMATTER));
        System.out.println("ZonedDateTime:"+ZonedDateTime.now()+" 转换:"+DateUtil.covertObjToString(ZonedDateTime.now(),DateUtil.DATETIME_FORMATTER));
        System.out.println("Instant:"+ Instant.now()+" 转换:"+DateUtil.covertObjToString(Instant.now(),DateUtil.DATETIME_FORMATTER));
        System.out.println("Instant:"+ Instant.now()+" 转换:"+DateUtil.covertObjToZdt(Instant.now()));
        System.out.println("字符串时间戳:"+DateUtil.covertObjToZdt("1638257256794"));
        System.out.println(DateUtil.covertObjToInstant("2021-11-05 19:24:30:143"));
        System.out.println(DateUtil.covertObjToDateTime("2021-11-05 19:24:30:143"));
        System.out.println(DateUtil.covertObjToDateTime(DateUtil.getCurrDateTime()));
        System.out.println(DateUtil.getCurrDateTime());
        System.out.println(DateUtil.getCurrSqlDate());
        System.out.println(DateUtil.getCurrTimestamp());
        System.out.println(DateUtil.covertObjToString(DateUtil.getCurrDateTime(),DateUtil.SHORT_DATE_FORMAT_FULL_UTC));
        System.out.println(DateUtil.covertObjToString(DateUtil.getCurrDateTime(),DateUtil.TIME_FORMAT_SHORT_FORMATTER));
    }

    @Test
    void getLastMonthEndDay(){
        System.out.println(DateUtil.getLastMonthEndDay(DateUtil.DATE_FORMATTER));
    }

    @Test
    void getLastMonthStartDay(){
        System.out.println(DateUtil.getLastMonthStartDay(3,DateUtil.DATE_FORMATTER));
    }

    @Test
    void getNextMonthStartDay(){
        System.out.println(DateUtil.getLastMonthsEndDay(2,DateUtil.DATE_FORMATTER));
        System.out.println(DateUtil.getLastMonthsEndDay(3,DateUtil.DATE_FORMATTER));
    }

    @Test
    void getNextMonthEndDay(){
        System.out.println(DateUtil.getNextMonthEndDay(DateUtil.DATE_FORMATTER));
    }

    @Test
    void getNextMonthsEndDay(){
        System.out.println(DateUtil.getNextMonthsEndDay(2,DateUtil.DATE_FORMATTER));
        System.out.println(DateUtil.getNextMonthsEndDay(3,DateUtil.DATE_FORMATTER));
    }

}