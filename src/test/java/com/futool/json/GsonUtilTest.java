package com.futool.json;

import com.futool.GsonUtil;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GsonUtilTest {
    @Test
    void toJson1(){
        List<String> list = new ArrayList<String>();
        list.add("张三");
        list.add("李四");
        list.add("王五");
        System.out.println("toJson1:"+GsonUtil.toJson(list));
    }

    @Test
    void toJson2(){
        List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
        Map<String,Object> map1 = new HashMap<String,Object>(8);
        map1.put("name","张三");
        map1.put("age",24);
        map1.put("sex","男");
        Map<String,Object> map2 = new HashMap<String,Object>(8);
        map2.put("name","李四");
        map2.put("age",26);
        map2.put("sex","男");
        Map<String,Object> map3 = new HashMap<String,Object>(8);
        map3.put("name","王五");
        map3.put("age",28);
        map3.put("sex","男");
        list.add(map1);
        list.add(map2);
        list.add(map3);
        System.out.println("toJson2:"+GsonUtil.toJson(list));
    }
}
