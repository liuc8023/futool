package com.futool;

import org.junit.jupiter.api.Test;
import java.math.BigDecimal;

class BigDecimalUtilTest {

    @Test
    void bigDecimalFormatToFinance() {
        System.out.println(BigDecimalUtil.bigDecimalFormatToFinance(new BigDecimal("1000000"),0));
        System.out.println(BigDecimalUtil.bigDecimalFormatToFinance(new BigDecimal("1000000"),1));
        System.out.println(BigDecimalUtil.bigDecimalFormatToFinance(new BigDecimal("1000000"),2));
        System.out.println(BigDecimalUtil.financeFormatToBigDecimal("1000000.0"));
        System.out.println(BigDecimalUtil.financeFormatToBigDecimal("1,000,000"));
        System.out.println(BigDecimalUtil.financeFormatToBigDecimal("1,000,000.0"));
        System.out.println(BigDecimalUtil.financeFormatToBigDecimal("1,000,000.00"));
    }

    @Test
    void objToPercent() {
        System.out.println(BigDecimalUtil.objToPercent(0.1));
        System.out.println(BigDecimalUtil.objToPercent(1));
        System.out.println(BigDecimalUtil.objToPercent(10));
        System.out.println(BigDecimalUtil.objToPercent(0.23444));
        System.out.println(BigDecimalUtil.objToPercent(0.23555));
        System.out.println(BigDecimalUtil.objToPercent("0.23555"));
        double a = 2.23;
        System.out.println(BigDecimalUtil.objToPercent(a));
        float b = 0.21f;
        System.out.println(BigDecimalUtil.objToPercent(b));
    }

    @Test
    void percentToBigDecimal() {
        System.out.println(BigDecimalUtil.percentToBigDecimal("100%"));
        System.out.println(BigDecimalUtil.percentToBigDecimal("10%"));
        System.out.println(BigDecimalUtil.percentToBigDecimal("-1%"));
        System.out.println(BigDecimalUtil.percentToBigDecimal("+2%"));
        System.out.println(BigDecimalUtil.percentToBigDecimal("+2.01%"));
        System.out.println(BigDecimalUtil.percentToBigDecimal("null"));
        System.out.println(BigDecimalUtil.percentToBigDecimal(null));
        System.out.println(BigDecimalUtil.percentToBigDecimal(""));
        System.out.println(BigDecimalUtil.percentToBigDecimal(" "));
    }

    @Test
    void objToBigDecimal() {
        System.out.println(BigDecimalUtil.objToBigDecimal("-1.2"));
        System.out.println(BigDecimalUtil.objToBigDecimal("a"));
        System.out.println(BigDecimalUtil.objToBigDecimal(" "));
        System.out.println(BigDecimalUtil.objToBigDecimal(""));
        System.out.println(BigDecimalUtil.objToBigDecimal(null));
    }

    @Test
    void div(){
        System.out.println(BigDecimalUtil.div("2.5","3.6",2));
        System.out.println(BigDecimalUtil.div("5","2",1));
        System.out.println(BigDecimalUtil.div("5","0.00000",1));
        System.out.println(BigDecimalUtil.div("5","-0.00000",1));
        System.out.println(BigDecimalUtil.div("5","3"));
    }
}