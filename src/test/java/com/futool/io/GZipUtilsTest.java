package com.futool.io;

import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class GZipUtilsTest {
    @SneakyThrows
    @Test
    void gzip(){
        Map<String, byte[]> fileBytesMap = null;

        fileBytesMap = new HashMap<String, byte[]>();
        // 设置文件列表.
        File dirFile = new File("C:/Users/Administrator/Downloads/个人文件/2020-07-13/files");
        for (File file : dirFile.listFiles()) {
            fileBytesMap.put(file.getName(), FileUtils.readFileToByteArray(file));
        }
        byte[] ramBytes = GZipUtils.compressByTar(fileBytesMap);
        ramBytes = GZipUtils.compressByGZip(ramBytes);
        FileUtils.writeByteArrayToFile(new File("C:/Users/Administrator/Downloads/个人文件/2020-07-13/ram.tar.gz"), ramBytes);

        ramBytes = GZipUtils.uncompressByGZip(ramBytes);
        fileBytesMap = GZipUtils.uncompressByTar(ramBytes);
        System.out.println(fileBytesMap.size());
    }
}
