package com.futool.concurrent;

import com.futool.concurrent.constants.BusinessProcessConstants;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;

public class ConcurrentUtilTest {
    @Test
    void batchDealTest() throws InterruptedException {
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < 1000000; i++) {
            list.add("第" + i + "个");
        }
        ConcurrentUtil.batchDeal(list,10000, BusinessProcessConstants.BATCH_UPDATE_CATALOG,BusinessProcessConstants.BATCH_UPDATE_CATALOG);
        ConcurrentUtil.batchDeal(list,10000, BusinessProcessConstants.BATCH_CREATE_CATALOG,BusinessProcessConstants.BATCH_CREATE_CATALOG);
    }
}
