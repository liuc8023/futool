package com.futool.pdf;

import com.itextpdf.text.PageSize;
import org.junit.jupiter.api.Test;
import java.io.File;
import java.util.*;

/**
 * 测试pdf生成
 */
class PdfUtilsTest {

    @Test
    void createPdf() {
        Map<String, Object> mp = new HashMap<String, Object>(16);
        try {
            // 生成后的路径
            String outputFile = "temp/4.pdf";
            Map<String, Object> dataMap = new HashMap<String, Object>(16);

            List<String> titleList = Arrays.asList("属性1", "属性2", "属性3", "属性4", "属性5", "属性6", "属性7");
            dataMap.put("titleList", titleList);

            List<List<String>> dataList = new ArrayList<List<String>>();
            for (int i = 0; i < 100; i++) {
                dataList
                        .add(Arrays.asList("数据1_" + i, "数据2_" + i, "数据3_数据3_数据3_数据3_数据3_数据3_数据3_数据3_数据3_" + i,
                                "数据4_" + i, "数据5_" + i, "数据6_" + i, "数据7_" + i));
            }
            dataMap.put("dataList", dataList);
            dataMap.put("contractName", null);
            dataMap.put("applyType", "3");
            dataMap.put("applyCategory", "2");
            dataMap.put("applyReasonAnaly", "鬼画符鬼画符\n给哈哈哈哈");
            dataMap.put("surveyTraceItemOnCheck", "鬼画符鬼画符\n就将计就计");
            File water = ResourceUtils.getFileFromRelativePath("templates/watermark/water.png");
//            PdfUtils.createPdf("templates/creidt_review_report_word7.html", dataMap, outputFile, PageSize.A4, "", true, water);
            PdfUtils.createPdf("templates/OtherApplyReport.html", dataMap, outputFile, PageSize.A4, "", true, water);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}