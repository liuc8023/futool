package com.futool.regex;

import org.junit.jupiter.api.Test;

class RegexUtilTest {
    @Test
    void checkEmail(){
        System.out.println("验证邮箱正则表达式...");
        System.out.println(RegexUtil.checkEmail("112@qq.com"));
        System.out.println(RegexUtil.checkEmail("112@163.com"));
        System.out.println(RegexUtil.checkEmail("112@126.com"));
        System.out.println(RegexUtil.checkEmail("112@1261.com"));
        System.out.println(RegexUtil.checkEmail("liu_chao@chery.comc"));
        System.out.println(RegexUtil.checkEmail("123456123@qq.com"));
        System.out.println(RegexUtil.checkEmail("test@163.com"));
        System.out.println(RegexUtil.checkEmail("test99@163.com.cin"));
        System.out.println(RegexUtil.checkEmail("test.b"));
    }

    @Test
    void isDigit() {
        System.out.println(RegexUtil.isDigit("123"));
        System.out.println(RegexUtil.isDigit("123.0"));
        System.out.println(RegexUtil.isDigit("123,23120"));
        System.out.println(RegexUtil.isDigit("a123"));
        System.out.println(RegexUtil.isDigit(" 123"));
        System.out.println(RegexUtil.isDigit("_123"));
        System.out.println(RegexUtil.isDigit(""));
        System.out.println(RegexUtil.isDigit(null));
        System.out.println(RegexUtil.isDigit("null"));
        System.out.println(RegexUtil.isDigit(" "));
        System.out.println(RegexUtil.isDigit("-123"));
        System.out.println(RegexUtil.isDigit("+123"));
        System.out.println(RegexUtil.isDigit("123456789012"));
    }

    @Test
    void isNumeric() {
        System.out.println(RegexUtil.isNumeric("123"));
        System.out.println(RegexUtil.isNumeric("123.0"));
        System.out.println(RegexUtil.isNumeric("123,23120"));
        System.out.println(RegexUtil.isNumeric("a123"));
        System.out.println(RegexUtil.isNumeric(" 123"));
        System.out.println(RegexUtil.isNumeric("_123"));
        System.out.println(RegexUtil.isNumeric(""));
        System.out.println(RegexUtil.isNumeric(null));
        System.out.println(RegexUtil.isNumeric("null"));
        System.out.println(RegexUtil.isNumeric(" "));
        System.out.println(RegexUtil.isNumeric("-123"));
        System.out.println(RegexUtil.isNumeric("+123"));
    }

    @Test
    void isNonnegativeDecimal(){
        System.out.println(RegexUtil.isNonnegativeDecimal("-1.22"));
        System.out.println(RegexUtil.isNonnegativeDecimal("+1.22"));
        System.out.println(RegexUtil.isNonnegativeDecimal("1.22"));
        System.out.println(RegexUtil.isNonnegativeDecimal("1.232"));
        System.out.println(RegexUtil.isNonnegativeDecimal("1."));
        System.out.println(RegexUtil.isNonnegativeDecimal("1.0"));
    }

    @Test
    void isPositiveNumIncludeZero(){
        System.out.println(RegexUtil.isPositiveIntIncludesZero("a"));
        System.out.println(RegexUtil.isPositiveIntIncludesZero("-1"));
        System.out.println(RegexUtil.isPositiveIntIncludesZero("-0.2"));
        System.out.println(RegexUtil.isPositiveIntIncludesZero("0.2"));
        System.out.println(RegexUtil.isPositiveIntIncludesZero("0"));
        System.out.println(RegexUtil.isPositiveIntIncludesZero("1"));
    }
}