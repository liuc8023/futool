package com.futool.collection;

import com.futool.JsonUtil;
import com.google.common.collect.Lists;
import org.junit.jupiter.api.Test;
import java.util.*;

class ListUtilTest {

    @Test
    void compareListHitData() {
        List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
        Map<String,Object> map1 = new HashMap<String,Object>(8);
        map1.put("id","1");
        map1.put("name","张三");
        map1.put("age","23");
        Map<String,Object> map2 = new HashMap<String,Object>(8);
        map2.put("id","2");
        map2.put("name","李四");
        map2.put("age","24");
        List<Map<String,Object>> list1 = new ArrayList<Map<String,Object>>();
        Map<String,Object> map3 = new HashMap<String,Object>(8);
        map3.put("id","1");
        map3.put("name","王五");
        map3.put("age","24");
        map3.put("sex","男");
        Map<String,Object> map4 = new HashMap<String,Object>(8);
        map4.put("id","3");
        map4.put("name","赵六");
        map4.put("age","24");
        list.add(map1);
        list.add(map2);
        list1.add(map3);
        list1.add(map4);
        System.out.println(JsonUtil.toJson(ListUtil.compareListHitData(list,list1,"id")));
    }

    @Test
    void mergeList() {
        List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
        Map<String,Object> map1 = new HashMap<String,Object>(8);
        map1.put("id","1");
        map1.put("name","张三");
        map1.put("age",23d);
        Map<String,Object> map2 = new HashMap<String,Object>(8);
        map2.put("id","2");
        map2.put("name","李四");
        map2.put("age",26);
        List<Map<String,Object>> list1 = new ArrayList<Map<String,Object>>();
        Map<String,Object> map3 = new HashMap<String,Object>(8);
        map3.put("id","1");
        map3.put("name","王五");
        map3.put("age",30);
        map3.put("sex","男");
        Map<String,Object> map4 = new HashMap<String,Object>(8);
        map4.put("id","3");
        map4.put("name","赵六");
        map4.put("age",24);
        list.add(map2);
        list.add(map1);

        list1.add(map3);
        list1.add(map4);
        //[{"sex":"男","name":"王五","id":"1","age":"24"},{"name":"李四","id":"2","age":"24"},{"name":"赵六","id":"3","age":"24"}]
//        System.out.println(JsonUtil.toJson(ListUtil.mergeList(list,list1,"id")));
        System.out.println(JsonUtil.toJson(ListUtil.sortListMap(ListUtil.mergeList(list,list1,"id"), "age", "desc")));
        //[{"sex":"男","name":"张三","id":"1","age":"23"},{"name":"李四","id":"2","age":"24"},{"name":"赵六","id":"3","age":"24"}]
//        System.out.println(JsonUtil.toJson(ListUtil.mergeList(list1,list,"id")));
    }

    @Test
    void getValueListFromKey() {
        List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
        Map<String,Object> map1 = new HashMap<String,Object>(8);
        map1.put("name","张三");
        map1.put("age","23");
        Map<String,Object> map2 = new HashMap<String,Object>(8);
        map2.put("name","李四");
        map2.put("age","24");
        Map<String,Object> map3 = new HashMap<String,Object>(8);
        map3.put("name","王五");
        map3.put("age","25");
        Map<String,Object> map4 = new HashMap<String,Object>(8);
        map4.put("name","赵六");
        map4.put("age","");
        list.add(map1);
        list.add(map2);
        list.add(map3);
        list.add(map4);
        System.out.println(JsonUtil.toJson(ListUtil.getValueListFromKey(list,"age")));
    }

    @Test
    void getListGroupByValue() {
        List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
        Map<String,Object> map1 = new HashMap<String,Object>(8);
        map1.put("name","张三");
        map1.put("age","23");
        Map<String,Object> map2 = new HashMap<String,Object>(8);
        map2.put("name","李四");
        map2.put("age","24");
        Map<String,Object> map3 = new HashMap<String,Object>(8);
        map3.put("name","王五");
        map3.put("age","23");
        Map<String,Object> map4 = new HashMap<String,Object>(8);
        map4.put("name","赵六");
        map4.put("age","24");
        list.add(map1);
        list.add(map2);
        list.add(map3);
        list.add(map4);
        Map<String,List<Map<String,Object>>> map = ListUtil.getListGroupByKey(list,"age");
        System.out.println(JsonUtil.toJson(map));
    }

    @Test
    void getListNoNull() {
        List<String> list = Lists.newArrayList("", "1", "2", null, "3", null);
        //["1","2","3"]
        System.out.println(JsonUtil.toJson(ListUtil.getListNoNull(list)));
    }

    @Test
    void pageList() {
        List<Object> list = new ArrayList<>();
        for (int i = 1; i <= 50; i++) {
            list.add(String.valueOf(i));
        }
        List<Object> curList = ListUtil.pageList(list,5,10);
        System.out.println("page："+curList);
    }

    @Test
    void deleteDuplicatedMapFromListByKeys(){
        List<Map<String, Object>> list = new ArrayList<>();
        Map<String, Object> map1 = new HashMap<String, Object>();
        map1.put("id", "11");
        map1.put("name", "张翰");
        map1.put("age", "33");
        Map<String, Object> map2 = new HashMap<String, Object>();
        map2.put("id", "11");
        map2.put("name", "霍尊");
        map2.put("age", "33");
        Map<String, Object> map3 = new HashMap<String, Object>();
        map3.put("id", "22");
        map3.put("name", "吴签");
        map3.put("age", "34");
        Map<String, Object> map4 = new HashMap<String, Object>();
        map4.put("id", "22");
        map4.put("name", "吴签");
        map4.put("age", "35");
        list.add(map1);
        list.add(map2);
        list.add(map3);
        list.add(map4);
        //创建去重的key的集合
        List<Object> deleteDuplicatedKeys = new ArrayList<>();
        deleteDuplicatedKeys.add("id");
        deleteDuplicatedKeys.add("name");
        System.out.println("未去重的集合："+list);
        List list1 = ListUtil.deleteDuplicatedMapFromListByKeys(list, deleteDuplicatedKeys);
        System.out.println("去重后的集合："+list1);
    }

    @Test
    void differenceMapByList(){
        List<Map<String,Object>> listMap = new ArrayList<>();
        List<Map<String,Object>> list = new ArrayList<>();
        Map<String, Object> map1 = new HashMap<String, Object>();
        map1.put("id", "11");
        map1.put("name", "张翰");
        Map<String, Object> map2 = new HashMap<String, Object>();
        map2.put("id", "11");
        map2.put("name", "霍尊");
        Map<String, Object> map3 = new HashMap<String, Object>();
        map3.put("id", "22");
        map3.put("name", "吴签");
        list.add(map1);
        list.add(map2);
        list.add(map3);
        System.out.println("集合1："+list);
        Map<String, Object> map4= new HashMap<String, Object>();
        map4.put("id", "22");
        map4.put("name", "吴签");
        listMap.add(map1);
        listMap.add(map4);
        System.out.println("集合2："+listMap);
        //查找差集
        List list2 = ListUtil.differenceMapByList(list, listMap);
        System.out.println("集合的差集："+list2);
    }
}