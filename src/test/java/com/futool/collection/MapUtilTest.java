package com.futool.collection;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

public class MapUtilTest {
    @Test
    void keyInitialToLowCaseTest(){
        Map<String,Object> map = new HashMap<>();
        map.put("Key","value");
        map.put("Zhong","Guo");
        System.out.println(MapUtil.keyInitialToLowCase(map));
    }
}
