package com.futool;

import org.junit.jupiter.api.Test;

class SftpUtilTest {

    private SftpUtil sftp = null;

    @Test
    void downloadFile() {
        try {
            sftp = new SftpUtil("192.168.11.159", 22,"log", "log",60);
            sftp.downloadFile("/gfs/joint-debug/cgm", "黄石东瑞汽车销售服务有限公司贷款车辆盘点表20211120.pdf",
                    "E://abc//黄石东瑞汽车销售服务有限公司贷款车辆盘点表20211120.pdf", false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void uploadFile() {
        try {
            sftp = new SftpUtil("192.168.11.159", 22,"log", "log",120);
            sftp.uploadFile("/gfs/joint-debug/cgm", "黄石东瑞汽车销售服务有限公司贷款车辆盘点表20211120.pdf",
                    "E://abc//liuc.txt", true);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}