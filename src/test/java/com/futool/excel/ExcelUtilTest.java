package com.futool.excel;

import com.futool.JsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Slf4j
public class ExcelUtilTest {
    @Test
    public void simpleRead() throws Exception {
        String title = "名称,key,value";
        List<List<Object>> content = new ArrayList<>();
        // 10行3列
        for (int i = 0; i < 10; i++) {
            List<Object> dataList = new ArrayList<>();
            for (int j = 0; j < 3; j++) {
                dataList.add(j);
            }
            content.add(dataList);
        }
        ExcelUtil.writeExcel("temp/测试.xlsx",title,"导出数据",content);
    }

    @Test
    public void readWithoutObj() {
        // 被读取的文件绝对路径
        String fileName = "temp/测试.xlsx";
        List<Map<String,Object>> list = ExcelUtil.readExcel(fileName);
        log.info(JsonUtil.toJson(list));
    }
}