package com.futool.string;

import com.futool.StringUtils;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class StringUtilTest {
    @Test
    public void testAddOneForStr(){
        String str = "01";
        String s = StringUtils.addOneForStr(str);
        //02
        System.out.println(s);
    }

    @Test
    public void testStringToList(){
        String str = "0,1,23";
        List<String> list = StringUtils.stringToList(str);
        //[0, 1, 23]
        System.out.println(list);
    }

    @Test
    public void testListToString(){
        List<String> list1 = new ArrayList<String>();
        list1.add("a");
        list1.add("b");
        list1.add("c");
        List<Integer> list2 = new ArrayList<Integer>();
        list2.add(1);
        list2.add(2);
        list2.add(3);
        //a,b,c
        System.out.println(StringUtils.listToString(list1));
        //1,2,3
        System.out.println(StringUtils.listToString(list2));
    }

    @Test
    public void testConvertToCamelCase() {
        String str = "HELLO_WORLD";
        String s = StringUtils.convertToCamelCase(str);
        //HelloWorld
        System.out.println(s);
    }

    @Test
    public void testUnderlineToHump() {
        String str1 = "hello_world";
        String str2 = "HELLO_WORLD";
        //helloWorld
        System.out.println(StringUtils.underlineToHump(str1));
        //helloWorld
        System.out.println(StringUtils.underlineToHump(str2));
    }

    @Test
    public void testHumpToUnderline() {
        String str1 = "helloWorld";
        //HELLO_WORLD
        System.out.println(StringUtils.humpToUnderline(str1));
    }

    @Test
    public void testStrLeftFillZero(){
        System.out.println(StringUtils.strLeftFillZero("10000000000","1"));
    }

    @Test
    public void testStrLeftRemoveZero(){
        System.out.println(StringUtils.strLeftRemoveZero("000000145345"));
    }

}